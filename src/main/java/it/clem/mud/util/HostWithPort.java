package it.clem.mud.util;

import java.io.Serializable;

public class HostWithPort implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -7560579127946822666L;
  private final String host;
  private final int port;

  public HostWithPort(String host, int port) {
    super();
    this.host = host;
    this.port = port;
  }
  
  public HostWithPort(String host) {
    this(host,23);
  }

  public String getHost() {
    return host;
  }

  public int getPort() {
    return port;
  }

  @Override
  public String toString() {
    return host + ":" + port;
  }

}
