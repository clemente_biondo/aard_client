package it.clem.mud.util.colors.ansi;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public enum AnsiColor {
  Black, Red, Green, Yellow, Blue, Magenta, Cyan, White, BrightBlack, BrightRed, BrightGreen, BrightYellow, BrightBlue, BrightMagenta, BrightCyan, BrightWhite;

  private static final Color[] ANSI_COLORS = { 
      Color.BLACK, 
      new Color(128, 0, 0), 
      new Color(0, 128, 0),
      new Color(128, 128, 0), 
      new Color(0, 0, 255), //altrimenti è illegibile
      new Color(128, 0, 128), 
      new Color(0, 128, 128), 
      new Color(192, 192, 192),
      
      new Color(128, 128, 128), 
      Color.RED, 
      Color.GREEN, 
      Color.YELLOW, 
      Color.BLUE, 
      Color.MAGENTA, 
      Color.CYAN,
      Color.WHITE 
      };

  private static final AttributeSet[] ATTRIBUTE_SETS;
  static {
    List<AttributeSet> tmp= new ArrayList<>();
    for( Color c: ANSI_COLORS) {
      SimpleAttributeSet s=new SimpleAttributeSet();
      StyleConstants.setForeground(s, c);
      tmp.add(s);
    }
    ATTRIBUTE_SETS=tmp.toArray(new AttributeSet[tmp.size()] );
  }

  public Color toAwtColor() {
    return ANSI_COLORS[ordinal()];
  }
  
  public AttributeSet toSwingAttributeSet() {
    return ATTRIBUTE_SETS[ordinal()];
  }

  public static AnsiColor defaultColor() {
    return White;
  }

  public static AnsiColor getColor(boolean bright, int colorIdx) {
    int order = colorIdx + (bright ? 8 : 0);
    return AnsiColor.values()[order];
  }

}
