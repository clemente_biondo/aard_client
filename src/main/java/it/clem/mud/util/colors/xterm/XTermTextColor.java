package it.clem.mud.util.colors.xterm;

import java.util.Map;
import java.util.TreeMap;
import javax.annotation.concurrent.NotThreadSafe;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

@NotThreadSafe
public class XTermTextColor /*implements Serializable*/ {
  
  private final XTermColor foregroundColor;
  private final XTermColor backgroundColor;
  private final SimpleAttributeSet attr;
  
  private final static Map<Integer,Map<Integer,XTermTextColor>> CACHE = new TreeMap<>();

  private XTermTextColor(XTermColor foregroundColor, XTermColor backgroundColor) {
    super();
    this.foregroundColor = foregroundColor;
    this.backgroundColor = backgroundColor;
    this.attr = new SimpleAttributeSet();
    StyleConstants.setForeground(attr, this.foregroundColor.toAWTColor());
    if(backgroundColor !=null) {
      StyleConstants.setBackground(attr, this.backgroundColor.toAWTColor());
    }
  }
  
  public static XTermTextColor valueOf(int foregroundColorIndex,int backgroundColorIndex) {
    if (foregroundColorIndex < 0 || foregroundColorIndex > 255) {
      throw new IllegalArgumentException("" + foregroundColorIndex);
    }
    if (backgroundColorIndex < -1 || backgroundColorIndex > 255) {
      throw new IllegalArgumentException("" + backgroundColorIndex);
    }
 
    return CACHE
    .computeIfAbsent(foregroundColorIndex,kf -> new TreeMap<>())
    .computeIfAbsent(backgroundColorIndex, kb ->
      new XTermTextColor(XTermColor.valueOf(foregroundColorIndex), backgroundColorIndex!=-1 ? XTermColor.valueOf(backgroundColorIndex):null));
  }
  
  public static XTermTextColor defaultColor() {
    return valueOf(7,-1);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((backgroundColor == null) ? 0 : backgroundColor.hashCode());
    result = prime * result + ((foregroundColor == null) ? 0 : foregroundColor.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    XTermTextColor other = (XTermTextColor) obj;
    if (backgroundColor == null) {
      if (other.backgroundColor != null)
        return false;
    } else if (!backgroundColor.equals(other.backgroundColor))
      return false;
    if (foregroundColor == null) {
      if (other.foregroundColor != null)
        return false;
    } else if (!foregroundColor.equals(other.foregroundColor))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return  "("+foregroundColor + ","+ backgroundColor + ")";
  }

  public AttributeSet toSwingAttributeSet() {
    return attr;
  }

}
