package it.clem.mud.util.colors.ansi;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class AnsiColoredText implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -8677364391909291965L;
  private final String text;
  private final AnsiColor color;

  public AnsiColoredText(String text, AnsiColor color) {
    super();
    this.text = text;
    this.color = color == null ? AnsiColor.defaultColor() : color;
  }

  public String getText() {
    return text;
  }

  public AnsiColor getColor() {
    return color;
  }

  @Override
  public String toString() {
    return "[" + color + "]" + text;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((color == null) ? 0 : color.hashCode());
    result = prime * result + ((text == null) ? 0 : text.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AnsiColoredText other = (AnsiColoredText) obj;
    if (color != other.color)
      return false;
    if (text == null) {
      if (other.text != null)
        return false;
    } else if (!text.equals(other.text))
      return false;
    return true;
  }

  public boolean equals(String text, AnsiColor color) {
    return (this.color == color) && StringUtils.equals(this.text, text);
  }

  public static AnsiColoredText valueOf(String text, AnsiColor color) {
    return new AnsiColoredText(text, color);
  }

  public static AnsiColoredText valueOf(String text) {
    return new AnsiColoredText(text, null);
  }

}
