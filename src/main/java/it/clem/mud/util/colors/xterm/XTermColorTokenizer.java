package it.clem.mud.util.colors.xterm;

import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XTermColorTokenizer {
  private final Consumer<String> textConsumer;
  private final Consumer<String> sgrConsumer;
  private final static Pattern SGR_PATTERN = Pattern.compile("\033\\[(\\d+(;\\d+)*)m");
  
  public XTermColorTokenizer(Consumer<String> textConsumer, Consumer<String> sgrConsumer) {
    super();
    this.textConsumer = textConsumer;
    this.sgrConsumer = sgrConsumer;
  }
  
  public void tokenize(String text) {
    if (text== null)return;
    Matcher m = SGR_PATTERN.matcher(text);
    int textStartPos=0;
    while(m.find()){
      int regexpStart=m.start();
      if (regexpStart > textStartPos){
        textConsumer.accept(text.substring(textStartPos,regexpStart));
      }
      textStartPos=m.end();
      sgrConsumer.accept(m.group(1));
    }
    if (text.length() > textStartPos){
      textConsumer.accept(text.substring(textStartPos));
    }
  }
  
}
