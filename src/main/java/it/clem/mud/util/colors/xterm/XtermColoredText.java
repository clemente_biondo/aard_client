package it.clem.mud.util.colors.xterm;

public class XtermColoredText /* implements Serializable */ {
  private final XTermTextColor color;
  private final String         text;

  public XtermColoredText(XTermTextColor color, String text) {
    super();
    this.color = color;
    this.text = text;
  }

  public XTermTextColor getColor() {
    return color;
  }

  public String getText() {
    return text;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((color == null) ? 0 : color.hashCode());
    result = prime * result + ((text == null) ? 0 : text.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    XtermColoredText other = (XtermColoredText) obj;
    if (color == null) {
      if (other.color != null)
        return false;
    } else if (!color.equals(other.color))
      return false;
    if (text == null) {
      if (other.text != null)
        return false;
    } else if (!text.equals(other.text))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return  color + ",>"+text+"<";
  }

  public static XtermColoredText valueOf(String text, int foregroundColorIndex, int backgroundColorIndex) {
    return new XtermColoredText(XTermTextColor.valueOf(foregroundColorIndex, backgroundColorIndex), text);
  }

  public static XtermColoredText valueOf(String text) {
    return valueOf(text, 7, -1);
  }

  public static XtermColoredText valueOf(String text, int foregroundColorIndex) {
    return valueOf(text, foregroundColorIndex, -1);
  }
}
