package it.clem.mud.util.colors.ansi;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

/**
 * Implementa CSI n m SGR – Select Graphic Rendition.Sets SGR parameters, including text color. After CSI can be zero or
 * more parameters separated with ;. With no parameters, CSI m is treated as CSI 0 m (reset / normal), which is typical
 * of most of the ANSI escape sequences.
 * https://en.wikipedia.org/wiki/ANSI_escape_code#graphics
 */
public class AnsiColorParser {
  private final static Pattern COLOR_PATTERN = Pattern.compile("\\x1B\\[(0|(([0-1]);3([0-7])))m");
  private AnsiColor currColor = AnsiColor.defaultColor();
  public List<AnsiColoredText> parseText(final String string) {
    Builder<AnsiColoredText> res = ImmutableList.builder();
    if (string != null) {
      Matcher m = COLOR_PATTERN.matcher(string);
      int lastEndPos = 0;
      
      while (m.find()) {
        if (m.start() > lastEndPos) {
          res.add(AnsiColoredText.valueOf(string.substring(lastEndPos, m.start()), currColor));
        }
        lastEndPos = m.end();
        currColor = getColor(m);
      }
      if (lastEndPos < string.length()) {
        res.add(AnsiColoredText.valueOf(string.substring(lastEndPos), currColor));
      }
    }

    return res.build();
  }

  private AnsiColor getColor(Matcher m) {
    String brightFlag = m.group(3);
    if (brightFlag == null) {// ho incontrato ESC[0m
      return AnsiColor.defaultColor();
    } else {// ho incontrato ad es. ESC[0;33m
      boolean bright = "1".equals(brightFlag);
      int colorIdx = Integer.parseInt(m.group(4));
      return AnsiColor.getColor(bright, colorIdx);
    }
  }

  public static void main(String... s) {

  }
}
