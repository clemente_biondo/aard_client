package it.clem.mud.util.colors.xterm;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SGRStatus {
  private boolean increasedIntensity;
  private int fgColor;
  private int bgColor;

  public SGRStatus() {
    super();
    reset();
  }

  private void reset() {
    increasedIntensity = false;
    fgColor = 7;
    bgColor = -1;
  }
  
  public XTermTextColor getXTermTextColor() {
    return XTermTextColor.valueOf(fgColor, bgColor);
  }
  
  public boolean update(String sgr) {
    List<Integer> codes = Arrays.stream(sgr.split(";")).filter(s -> s.length() > 0).map(Integer::parseInt)
        .collect(Collectors.toList());
    for (int j = 0; j < codes.size(); j++) {
      int currCode=codes.get(j);
      boolean beforeSecondLast = codes.size() - j >= 3;
      if (currCode == 0) {
        reset();
      } else if (currCode == 1) {
        increasedIntensity = true;
        //cambio anche il colore corrente
        if (fgColor <=7){
          fgColor +=8;
        }
        if (bgColor >=0 && bgColor <=7){
          bgColor +=8;
        }
      } else if (currCode >= 30 && currCode <= 37) {
        fgColor = currCode - 30 + (increasedIntensity ? 8 : 0);
      } else if (currCode == 38) {
        if (beforeSecondLast && codes.get(j + 1) == 5 && codes.get(j + 2) >= 0 && codes.get(j + 2) <= 255) {
          j += 2;
          fgColor = codes.get(j);
        } else {
          return false;
        }
      } else if (currCode >= 40 && currCode <= 47) {
        bgColor = currCode - 40 + (increasedIntensity ? 8 : 0);
      } else if (currCode == 48) {
        if (beforeSecondLast && codes.get(j + 1) == 5 && codes.get(j + 2) >= 0 && codes.get(j + 2) <= 255) {
          j += 2;
          bgColor = codes.get(j);
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
    return true;
  }
}
