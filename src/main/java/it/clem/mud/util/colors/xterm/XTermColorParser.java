package it.clem.mud.util.colors.xterm;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public class XTermColorParser {
  private Builder<XtermColoredText> builder;
  private final SGRStatus status = new SGRStatus();
  private final XTermColorTokenizer tokenizer = new XTermColorTokenizer(this::addText, this::updateColor);
  
  public List<XtermColoredText> parseText(String string) {
    builder = ImmutableList.builder();
    tokenizer.tokenize(string);
    return builder.build();
  }
  
  private void addText(String text){
    builder.add(new XtermColoredText(status.getXTermTextColor(),text));
  }
  
  private void updateColor(String sgr){
   if(! status.update(sgr)){
     addText(sgr);
   }
  }
  
}
