package it.clem.mud.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LangUtils {
  @SuppressWarnings("unused")
  private final static Logger log = LoggerFactory.getLogger(LangUtils.class);

  public static byte[] arrOfIntToArrOfByte(int... is) {
    byte[] res = new byte[is.length];
    for (int i = 0; i < res.length; i++) {
      res[i] = (byte) (is[i] & 0xff);
    }
    return res;
  }

  public static int[] arrOfByteToArrOfInt(byte... is) {
    int[] res = new int[is.length];
    for (int i = 0; i < res.length; i++) {
      res[i] = is[i] & 0xFF;
    }
    return res;
  }

  public static int[] stringToArrOfInt(String s) {
    if (s == null)
      return new int[0];
    return arrOfByteToArrOfInt(s.getBytes());
  }

  public static byte[] concat(byte[] first, byte[] second) {
    if (second == null)
      return first;
    if (first == null)
      return second;
    byte[] result = Arrays.copyOf(first, first.length + second.length);
    System.arraycopy(second, 0, result, first.length, second.length);
    return result;
  }

  public static byte[] concat(byte[] first, byte[]... others) {
    byte[] result = first;
    for (byte[] other : others) {
      result = concat(result, other);
    }
    return result;
  }

  public static <E> String joinAsString(Collection<E> collection, Function<E, String> stringExtractor,
      String delimiter) {
    //@formatter:off
    return collection.stream()
        .map(stringExtractor)
        .filter(s-> s!= null && s.length()>0)
        .collect( Collectors.joining( delimiter) );
    //@formatter:on
  }

  private final static DateTimeFormatter YYYYMMddHHmmss_FORMATTER = DateTimeFormatter.ofPattern("YYYYMMddHHmmss");

  public static String formatYMDHmSS(LocalDateTime dt) {
    return dt.format(YYYYMMddHHmmss_FORMATTER);
  }
  
  public static String nowAsYMDHmSS() {
    return formatYMDHmSS(LocalDateTime.now());
  }

}
