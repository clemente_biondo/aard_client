package it.clem.mud.util;

import java.util.concurrent.atomic.AtomicInteger;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class ThreadProgressiveConverter extends ClassicConverter {
  private static final AtomicInteger threadProg = new AtomicInteger();
  private static final ThreadLocal<String> threadProgr = new ThreadLocal<String>() {    
    @Override
    protected String initialValue() {
      return String.format("%05d", threadProg.incrementAndGet());
    }
  };
  
  public String convert(ILoggingEvent event) {
      return threadProgr.get();
  }


}
