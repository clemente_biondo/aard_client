package it.clem.mud.util.proxy_inspector;

import java.io.Serializable;
import java.time.LocalDateTime;

public class TelnetBuffer implements Serializable {
  private static final long serialVersionUID = 2598476572733703569L;
  private final LocalDateTime creationDate = LocalDateTime.now();
  private final boolean       fromServer;
  private final boolean       compressed;
  private final byte[]        sentBytes;
  private final byte[]        uncompressedBytes;
  private final int seqId;
  public TelnetBuffer(boolean fromServer, boolean compressed, byte[] sentBytes, byte[] uncompressedBytes,int seqId) {
    super();
    this.fromServer = fromServer;
    this.compressed = compressed;
    this.sentBytes = sentBytes;
    this.uncompressedBytes = uncompressedBytes;
    this.seqId=seqId;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public boolean isFromServer() {
    return fromServer;
  }

  public boolean isCompressed() {
    return compressed;
  }

  public byte[] getSentBytes() {
    return sentBytes;
  }

  public byte[] getUncompressedBytes() {
    return uncompressedBytes;
  }

  public int getSeqId() {
    return seqId;
  }

}
