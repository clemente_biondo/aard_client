package it.clem.mud.util.proxy_inspector;

import static it.clem.mud.protocol.TelnetProtocolResources.DONT;
import static it.clem.mud.protocol.TelnetProtocolResources.WILL;
import static it.clem.mud.protocol.TelnetProtocolResources.SB;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

import static it.clem.mud.protocol.TelnetProtocolResources.*;

public class TelnetBufferPrinter {
  @SuppressWarnings("unused")
  private final static Logger log = LoggerFactory.getLogger(TelnetBufferPrinter.class);

  public AnalysisResult analyzeAard(InputStream is) {
    return analyze(is, true);
  }

  public AnalysisResult analyze(InputStream is, boolean aardNewLine) {
    int b;
    int prevB = -1;
    int textBytesCount = 0;
    int protocolBytesCount = 0;
    final AnalysisResult.Builder builder = AnalysisResult.builder();
    TelnetState state = TelnetState.DFT;
    boolean unclosedText = false;
    try {
      while ((b = is.read()) != -1) {
        state = state.next(b);
        if (state.isCommandOption()) {
          builder.addCommandWithOption(prevB, b);
        }
        if (state == TelnetState.DFT) {
          unclosedText = true;
          textBytesCount++;
          if ((aardNewLine && (prevB == LF && b == CR)) || (prevB == CR && b == LF)) {
            builder.addTextLine();
            unclosedText = false;
            prevB = -1;// non voglio ricontare come newline LF dopo LFCR e simili
          } else {
            prevB = b;
          }
        } else {
          protocolBytesCount++;
          if (unclosedText) {// Conto anche le righe di testo non terminate
            builder.addTextLine();
            unclosedText = false;
          }
          prevB = b;
        }

      }
      return builder.setTextBytesCount(textBytesCount).setProtocolBytesCount(protocolBytesCount).build();
    } catch (IOException io) {
      throw new UncheckedIOException(io);
    }
  }

  public static class AnalysisResult {
    private final Map<Integer, Map<Integer, Integer>> options;
    private final int                                 textBytesRead;
    private final int                                 protocolBytesRead;
    private final int                                 textlines;

    public AnalysisResult(Map<Integer, Map<Integer, Integer>> options, int textBytesRead, int protocolBytesRead,
        int textlines) {
      super();
      this.textBytesRead = textBytesRead;
      this.protocolBytesRead = protocolBytesRead;
      this.textlines = textlines;
      this.options = options.entrySet().stream()
          .collect(ImmutableMap.toImmutableMap(Map.Entry::getKey, e -> ImmutableMap.copyOf(e.getValue())));
    }

    public int getTextlines() {
      return textlines;
    }

    public int getTextBytesRead() {
      return textBytesRead;
    }

    public int getProtocolBytesRead() {
      return protocolBytesRead;
    }

    public int getCodeCount(int code) {
      if ((code >= WILL && code <= DONT) || code == SB) {
        return options.getOrDefault(code, ImmutableMap.of()).values().stream().mapToInt(i -> i).sum();
      }
      throw new IllegalArgumentException();

    }

    public int getOptionCount(int code, int option) {
      if ((code >= WILL && code <= DONT) || code == SB) {
        return options.getOrDefault(code, ImmutableMap.of()).getOrDefault(option, 0);
      }
      throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
      String res = "Text Bytes:" + textBytesRead + "\nProtocol Bytes:" + protocolBytesRead + "\nLines:" + textlines
          + "\n";
      for (Map.Entry<Integer, Map<Integer, Integer>> codes : options.entrySet()) {
        for (Map.Entry<Integer, Integer> opt : codes.getValue().entrySet()) {
          res += CODES[codes.getKey()] + " " + OPTIONS[opt.getKey()] + ":" + opt.getValue() + "\n";
        }
      }
      return res;
    }

    public static Builder builder() {
      return new Builder();
    }

    public static class Builder {

      private int textBytesCount = 0;
      private int protocolBytesCount;
      private int textLines      = 0;
      // WILL/WONT/DO/DONT/SB seguito da opzione e cardinalita'
      private final Map<Integer, Map<Integer, Integer>> options = new TreeMap<>();

      public Builder addCommandWithOption(int code, int option) {
        options.computeIfAbsent(code, k -> new TreeMap<Integer, Integer>()).merge(option, 1, (v1, v2) -> v1 + v2);
        return this;
      }

      public Builder setTextBytesCount(int textBytesCount) {
        this.textBytesCount = textBytesCount;
        return this;
      }

      public Builder setProtocolBytesCount(int protocolBytesCount) {
        this.protocolBytesCount = protocolBytesCount;
        return this;
      }

      public Builder addTextLine() {
        this.textLines++;
        return this;
      }

      public AnalysisResult build() {
        return new AnalysisResult(options, textBytesCount, protocolBytesCount, textLines);
      }
    }

    public int countDistinctCodes() {
      return options.size();
    }

    public int count() {
      return options.values().stream().map(Map::values).flatMapToInt((c -> c.stream().mapToInt(i -> i))).sum();
    }

    public int countCodes() {
      return options.values().stream().mapToInt(Map::size).sum();
    }
  }

  public void copyInputUncompressed(File f, OutputStream out)
      throws FileNotFoundException, IOException, ClassNotFoundException {
    try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(f))) {
      for (;;) {
        TelnetBuffer tb;
        try {
          tb = (TelnetBuffer) in.readObject();
        } catch (EOFException ex) {
          break;
        }
        if (tb.isFromServer()) {
          out.write(tb.getUncompressedBytes());
        }
      }
      out.flush();
    }
  }

  public void print(File f, PrintWriter w, boolean printText) throws IOException, ClassNotFoundException {
    try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(f))) {
      Pair<TelnetPrinter, TelnetPrinter> printerPair = ImmutablePair.of(new TelnetPrinter(w, printText),
          new TelnetPrinter(w, printText));

      for (;;) {
        TelnetBuffer tb;
        try {
          tb = (TelnetBuffer) in.readObject();
        } catch (EOFException ex) {
          break;
        }
        TelnetPrinter p;
        if (tb.isFromServer()) {
          w.write("\n<<<<<<<<<<<< " + tb.getSeqId() + " " + tb.getCreationDate().format(DateTimeFormatter.ISO_DATE_TIME)
              + " <<<<<<<<<<<<\n");
          p = printerPair.getLeft();
        } else {
          w.write("\n>>>>>>>>>>>> " + tb.getSeqId() + " " + tb.getCreationDate().format(DateTimeFormatter.ISO_DATE_TIME)
              + " >>>>>>>>>>>>\n");
          p = printerPair.getRight();
        }
        p.print(tb.getUncompressedBytes());
      }
    }
  }

  public void print(File f, PrintWriter w) throws IOException, ClassNotFoundException {
    print(f, w, true);
  }

  public static void main(String... s) throws ClassNotFoundException, IOException {
    // PrintWriter pw = new PrintWriter(new FileOutputStream("c:/temp/commands.bin"));
    // new TelnetBufferPrinter().print(new File("c:/temp/log_1502218612.bin"), pw, false);
    // pw.flush();
    // pw.close();
    // pw = new PrintWriter(new FileOutputStream("c:/temp/all_decoded.bin"));
    // new TelnetBufferPrinter().print(new File("c:/temp/log_1502218612.bin"), pw, true);
    // pw.flush();
    // pw.close();
    // new TelnetBufferPrinter().copyInputUncompressed(new File("c:/temp/log.bin"),new
    // FileOutputStream("c:/temp/in_uncompressed.bin"));
    // System.out.println(new TelnetBufferPrinter().analyzeAard(new
    // FileInputStream("src/test/resources/sess1/in_uncompressed.bin")));
    //showCommands("D:\\work\\workspace_20160608\\telnet_study\\src\\test\\resources\\sess1\\log.bin");
    showAll("D:/git_aard_client/src/test/resources/aard_xterm_due/log.bin");
    showCommands("D:/git_aard_client/src/test/resources/aard_xterm_due/log.bin");
    copyInputUncompressed("D:/git_aard_client/src/test/resources/aard_xterm_due/log.bin");
  }
  public static void copyInputUncompressed(String inFileFilename) throws IOException, ClassNotFoundException {
    FileOutputStream fout =  new FileOutputStream("c:/temp/uncompressed.bin") ;
    new TelnetBufferPrinter().copyInputUncompressed(new File(inFileFilename), fout);
    fout.flush();
    fout.close();
  }
  
  public static void showCommands(String logFilename) throws IOException, ClassNotFoundException {
    PrintWriter pw = new PrintWriter(new FileOutputStream("c:/temp/commands.bin"));
    new TelnetBufferPrinter().print(new File(logFilename), pw, false);
    pw.flush();
    pw.close();
  }
  public static void showAll(String logFilename) throws IOException, ClassNotFoundException {
    PrintWriter pw = new PrintWriter(new FileOutputStream("c:/temp/all_decoded.bin"));
    new TelnetBufferPrinter().print(new File(logFilename), pw, true);
    pw.flush();
    pw.close();
  }

}
