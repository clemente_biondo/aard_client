package it.clem.mud.util.proxy_inspector;

import static it.clem.mud.protocol.TelnetProtocolResources.SUBNEG_START_COMPRESSION;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.DataFormatException;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;

import com.google.common.primitives.Bytes;

import it.clem.mud.util.ByteInflater;

public class TelnetLogger implements Closeable {
  @SuppressWarnings("unused")
  private final static Logger log=org.slf4j.LoggerFactory.getLogger( TelnetLogger.class);

  private final ObjectOutputStream lout;
  private final OutputStream       sout;
  private final OutputStream       cout;
  private boolean                  compressed    = false;
  private boolean                  alreadyclosed = false;
  private final ByteInflater       bd            = new ByteInflater();
  private int                      bufPos        = 0;
  private byte[]                   buf           = new byte[8192];
  private final boolean            logOnSysout;
  private final AtomicInteger      counter       = new AtomicInteger();

  public TelnetLogger(File flog, File fin, File fout, boolean logOnSysout) throws IOException {
    if (flog.exists()) {
      throw new FileAlreadyExistsException(flog.getAbsolutePath());
    }
    if (fin.exists()) {
      throw new FileAlreadyExistsException(fin.getAbsolutePath());
    }
    if (fout.exists()) {
      throw new FileAlreadyExistsException(fout.getAbsolutePath());
    }
    lout = new ObjectOutputStream(new FileOutputStream(flog));
    sout = new FileOutputStream(fin);
    cout = new FileOutputStream(fout);

    this.logOnSysout = logOnSysout;
  }

  public InputStream wrapInputStream(InputStream in) {
    return new InputStream() {

      @Override
      public long skip(long n) throws IOException {
        return in.skip(n);
      }

      @Override
      public int available() throws IOException {
        return in.available();
      }

      @Override
      public void close() throws IOException {
        in.close();
        lout.flush();
        sout.flush();
      }

      @Override
      public synchronized void mark(int readlimit) {
        in.mark(readlimit);
      }

      @Override
      public synchronized void reset() throws IOException {
        in.reset();
      }

      @Override
      public boolean markSupported() {
        return in.markSupported();
      }

      @Override
      public int read() throws IOException {
        int read = in.read();
        if (read > 0) {
          sout.write(read);
          TelnetLogger.this.logSingleByteFromServer((byte) (read & 0xff));
        }
        return read;
      }

      @Override
      public int read(byte[] b) throws IOException {
        int read = in.read(b);
        if (read > 0) {
          sout.write(b, 0, read);
          TelnetLogger.this.log(true, b, 0, read);
        }
        return read;
      }

      @Override
      public int read(byte[] b, int off, int len) throws IOException {
        int read = in.read(b, off, len);
        if (read > 0) {
          sout.write(b, off, read);
          TelnetLogger.this.log(true, b, off, read);
        }
        return read;
      }

    };
  }

  protected void logSingleByteFromServer(byte b) throws IOException {
    buf[bufPos++] = b;
    if (buf.length == bufPos)
      flushBuffer();
  }

  private void flushBuffer() throws IOException {
    log(true, buf, 0, bufPos, false);
    bufPos = 0;
  }

  public OutputStream wrapOutputStream(final OutputStream out) {
    return new BufferedOutputStream(new OutputStream() {

      @Override
      public void close() throws IOException {
        out.close();
        lout.flush();
        cout.flush();
      }

      @Override
      public void write(byte[] b) throws IOException {
        throw new IllegalStateException();
      }

      @Override
      public void write(byte[] b, int off, int len) throws IOException {
        cout.write(b, off, len);
        TelnetLogger.this.log(false, b, off, len);
        out.write(b, off, len);
      }

      @Override
      public void flush() throws IOException {
        out.flush();
        lout.flush();
        cout.flush();
      }

      @Override
      public void write(int b) throws IOException {
        throw new IllegalStateException();
      }
    });
  }

  public synchronized void log(boolean fromServer, byte[] data) throws IOException {
    log(fromServer, data, 0, data.length);
  }

  public synchronized void log(boolean fromServer, byte[] data, int offset, int len) throws IOException {
    log(fromServer, data, offset, len, fromServer);
  }

  private synchronized void log(boolean fromServer, byte[] data, int offset, int len, boolean flushBuffer) throws IOException {
    if(len<=0)return;
    if (logOnSysout) {
      System.out.write(data, offset, len);
    }
    if (flushBuffer) {
      flushBuffer();
    }

    int cpos;
    byte[] sentBytes = len == data.length ? data : ArrayUtils.subarray(data, offset, len), uncompressedBytes;
    if (compressed && fromServer) {
      try {
        uncompressedBytes = bd.decompress(sentBytes);
      } catch (DataFormatException dex) {
        throw new IOException(dex.getMessage(), dex);
      }
    } else if (fromServer && (cpos = Bytes.indexOf(sentBytes, SUBNEG_START_COMPRESSION)) != -1) {
      byte[] uncompressedPart= ArrayUtils.subarray(sentBytes, 0, cpos + SUBNEG_START_COMPRESSION.length);
      writeObject(fromServer, uncompressedPart, uncompressedPart);
      compressed = true;
      log(fromServer, sentBytes, cpos + SUBNEG_START_COMPRESSION.length, sentBytes.length - (cpos + SUBNEG_START_COMPRESSION.length));
      return;
    } else {
      uncompressedBytes = sentBytes;
    }
    writeObject(fromServer, sentBytes, uncompressedBytes);
  }

  private void writeObject(boolean fromServer,byte[] sentBytes,byte[] uncompressedBytes) throws IOException{
    lout.writeObject(new TelnetBuffer(fromServer, compressed, sentBytes, uncompressedBytes, counter.incrementAndGet()));
  }
  
  @Override
  public synchronized void close() throws IOException {
    if (alreadyclosed)
      return;
    try {
      lout.close();
      sout.close();
      cout.close();
    } finally {
      bd.close();
    }
    alreadyclosed = true;
  }

  public static TelnetLogger createLogger(boolean logOnSysout) throws IOException {
    long id = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    return new TelnetLogger(new File("c:/temp/log_" + id + ".bin"), new File("c:/temp/in_" + id + ".bin"), new File("c:/temp/out_" + id + ".bin"), logOnSysout);
  }
}
