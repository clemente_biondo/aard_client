package it.clem.mud.util.proxy_inspector;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import it.clem.mud.protocol.AardProtocolResources;
import it.clem.mud.util.HostWithPort;


public class TelnetProxy {
  private byte[] buffer = new byte[8192];

  public static void main(String[] args) throws IOException {
    new TelnetProxy().startProxy(AardProtocolResources.AARD_SERVER);
    //new TelnetProxy().startProxy(AardProtocolResources.STAR_TREK_SERVER);
    // new TelnetProxy().startProxy("forgottenkingdoms.org",4000);
  }

  private void startProxy(HostWithPort hp) throws IOException {
    TelnetLogger log = TelnetLogger.createLogger(true);

    try (ServerSocket server = new ServerSocket(23); Socket proxyAard = new Socket(hp.getHost(), hp.getPort());) {
      Socket userProxy = server.accept();
      userProxy.setSoTimeout(300);
      proxyAard.setSoTimeout(300);
      InputStream fromAard = log.wrapInputStream(proxyAard.getInputStream());
      OutputStream toAard = log.wrapOutputStream(proxyAard.getOutputStream());
      InputStream fromUser = userProxy.getInputStream();
      OutputStream toUser = userProxy.getOutputStream();

      for (;;) {
        if (processInput(fromAard, toUser) || processInput(fromUser, toAard)) {
          break;
        }
      }
    }
  }

  boolean processInput(InputStream is, OutputStream out) throws IOException {
    boolean closed = false;
    int read;
    try {
      read = is.read(buffer);
      closed = read == -1;
      if (read > 0) {
        out.write(buffer, 0, read);
        out.flush();
      }
    } catch (SocketTimeoutException ex) {
    }
    return closed;
  }

}
