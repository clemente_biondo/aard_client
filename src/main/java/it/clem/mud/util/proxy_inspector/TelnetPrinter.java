package it.clem.mud.util.proxy_inspector;

import java.io.PrintWriter;

public class TelnetPrinter {
  private final PrintWriter  pw;
  private TelnetState state = TelnetState.DFT;
  private final boolean printText;
  public TelnetPrinter(PrintWriter pw,boolean printText) {
    super();
    this.pw = pw;
    this.printText=printText;
  }

  public void print(byte[] data) {
    StringBuilder sb = new StringBuilder(); 
    for (int i = 0; i < data.length; i++) {
      int b = data[i] & 0xFF;
      state = state.next(b);
      state.print(sb, b,printText);
    }
    pw.write(sb.toString());

  }
}
