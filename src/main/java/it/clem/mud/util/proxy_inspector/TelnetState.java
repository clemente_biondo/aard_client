package it.clem.mud.util.proxy_inspector;

import static it.clem.mud.protocol.TelnetProtocolResources.*;

public enum TelnetState {
  LAST_WAS_IAC(false, TYPE.IS_COMMAND_CODE), // IAC*
  LAST_WAS_OPTION_CMD(false, TYPE.IS_COMMAND_CODE), // IAC (WILL|WONT|DONT|DO)*
  DFT(false, TYPE.NONE), LAST_WAS_START_SUBNEG(false, TYPE.IS_COMMAND_CODE), // IAC SB*
  LAST_WAS_OPTION_OF_SUBNEG(false, TYPE.IS_COMMAND_OPTION), // IAC SB <OPTION>*
  LAST_WAS_OPTION_OF_SUBNEG_GMCP(false, TYPE.IS_COMMAND_OPTION), // IAC SB GMCP*
  INSIDE_SUBNEG(false, TYPE.NONE), // IAC SB <OPTION> .*
  INSIDE_SUBNEG_GMCP(false, TYPE.NONE), // IAC SB GMCP .*
  LAST_WAS_CMD(true, TYPE.IS_COMMAND_CODE), // IAC (SE|NOP|DM|BRK|IP|AO|AYT|EC|EL)*
  LAST_WAS_OPTION_OF_OPTION_CMD(true, TYPE.IS_COMMAND_OPTION); // IAC WILL|WONT|DONT|DO <OPTION>*

  private enum TYPE {
    IS_COMMAND_CODE, IS_COMMAND_OPTION, NONE
  };

  private final boolean lastOfASequence;
  private final TYPE    type;

  private TelnetState(boolean lastOfASequence, TYPE type) {
    this.lastOfASequence = lastOfASequence;
    this.type = type;
  }

  private boolean isCommandCode() {
    return type == TYPE.IS_COMMAND_CODE;
  }

  public boolean isCommandOption() {
    return type == TYPE.IS_COMMAND_OPTION;
  }

  private boolean isClearText() {
    return this == DFT || this == INSIDE_SUBNEG_GMCP;
  }

  public TelnetState next(int b) {
    if (this == LAST_WAS_IAC) {
      return nextAfterIAC(b);
    } else if (b == IAC) {
      return LAST_WAS_IAC;
    } else if (lastOfASequence) {
      return DFT;
    } else if (this == LAST_WAS_OPTION_CMD) {// IAC WILL|WONT|DONT|DO <OPTION>*
      return LAST_WAS_OPTION_OF_OPTION_CMD;
    } else if (this == LAST_WAS_START_SUBNEG) {// IAC SB <OPTION>*
      return b == GMCP ? LAST_WAS_OPTION_OF_SUBNEG_GMCP : LAST_WAS_OPTION_OF_SUBNEG;
    } else if (this == LAST_WAS_OPTION_OF_SUBNEG) {
      return INSIDE_SUBNEG;
    } else if (this == LAST_WAS_OPTION_OF_SUBNEG_GMCP) {
      return TelnetState.INSIDE_SUBNEG_GMCP;
    } else {
      return this;
    }
  }

  private TelnetState nextAfterIAC(int b) {
    if (b == IAC) {// IAC IAC*
      return DFT;
    } else if (b >= WILL && b <= DONT) {// IAC (WILL|WONT|DONT|DO)*
      return LAST_WAS_OPTION_CMD;
    } else if (b >= SE && b < SB) {// IAC CMD*
      return LAST_WAS_CMD;
    } else if (b == SB) {// IAC SB*
      return LAST_WAS_START_SUBNEG;
    } else {
      throw new IllegalStateException();
    }
  }

  public void print(StringBuilder sb, int b,boolean printText) {
    if (isCommandCode()) {
      sb.append(CODES[b]);
      sb.append(lastOfASequence ?"\n":" ");
    } else if (isCommandOption()) {
      sb.append(OPTIONS[b]);
      sb.append(lastOfASequence ?"\n":" ");
    }else if(!printText && this == DFT){
      
    } else if (b==10 && isClearText()){
      sb.append("\r");
    } else if (b==13 && isClearText()){
      sb.append("\n");
    } else if ((b < 20 || b > 126) || !isClearText()) {
      sb.append("[" + b + " "+ (char)b +"]");
    }else{
      sb.append((char)b);
    }
  }
}
