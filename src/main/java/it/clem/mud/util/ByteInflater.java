package it.clem.mud.util;

import java.io.ByteArrayOutputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public class ByteInflater implements AutoCloseable {
  private final Inflater inflater = new Inflater();

  public byte[] decompress(byte[] bytesToDecompress) throws DataFormatException {
    return decompress(bytesToDecompress, 0, bytesToDecompress.length);
  }

  public byte[] decompress(byte[] data, int pos, int len) throws DataFormatException {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(len * 10);
    inflater.setInput(data, pos, len);
    byte[] buffer = new byte[1024];
    while (!inflater.needsInput()) {
      int count = inflater.inflate(buffer);
      outputStream.write(buffer, 0, count);
    }
    return outputStream.toByteArray();
  }

  @Override
  public void close() {
    inflater.end();
  }
}
