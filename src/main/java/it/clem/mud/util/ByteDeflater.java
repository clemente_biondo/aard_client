package it.clem.mud.util;

import java.io.ByteArrayOutputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;

public class ByteDeflater implements AutoCloseable {
  private final Deflater deflater = new Deflater();

  public byte[] compress(byte[] bytesToDecompress) throws DataFormatException {
    return compress(bytesToDecompress, 0, bytesToDecompress.length);
  }

  public byte[] compress(byte[] data, int pos, int len) throws DataFormatException {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(len * 10);
    deflater.setInput(data, pos, len);
    deflater.finish();
    byte[] buffer = new byte[1024];
    while (!deflater.finished()) {
      int count = deflater.deflate(buffer);
      outputStream.write(buffer, 0, count);
    }
    return outputStream.toByteArray();
  }

  @Override
  public void close() {
    deflater.end();
  }
}
