package it.clem.mud.recorder.analyzer;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

import it.clem.mud.protocol.data.TelnetText;
import it.clem.mud.recorder.Record;
import it.clem.mud.recorder.RecordType;
import it.clem.mud.shared.game.event.client.TextSentFromClientEvent;
import it.clem.mud.shared.game.event.server.TextSentFromServerEvent;

public class RecordFilterBuilder {
  private Set<RecordType> typesAllowed = new TreeSet<>(Arrays.asList(RecordType.values()));
  private Predicate<Optional<Serializable>> payloadPredicate = s -> true;

  public static final Predicate<Record> ONLY_EVENTS = new RecordFilterBuilder().onlyGameEvents().build();
  public static final Predicate<Record> ONLY_TELNET_DATA = new RecordFilterBuilder().onlyTelnetData().build();
  public static final Predicate<Record> ONLY_TELNET_COMMANDS = new RecordFilterBuilder().onlyTelnetData().excludeText()
      .build();
  public static final Predicate<Record> ALL_WITHOUT_TEXT = new RecordFilterBuilder().excludeText().build();

  public RecordFilterBuilder whitelist(RecordType... types) {
    typesAllowed.retainAll(Arrays.asList(types));
    return this;
  }

  public RecordFilterBuilder exclude(RecordType... types) {
    typesAllowed.removeAll(Arrays.asList(types));
    return this;
  }

  public RecordFilterBuilder onlyGameEvents() {
    return whitelist(RecordType.GameEvent);
  }

  public RecordFilterBuilder excludePayloadClass(Class<? extends Serializable> playloadClass) {
    payloadPredicate = payloadPredicate.and(o -> {
      return !o.isPresent() || !playloadClass.isInstance(o.get());
    });
    return this;
  }

  public RecordFilterBuilder excludeText() {
    return excludePayloadClass(TelnetText.class).excludePayloadClass(TextSentFromServerEvent.class)
        .excludePayloadClass(TextSentFromClientEvent.class);
  }

  public RecordFilterBuilder onlyTelnetData() {
    return whitelist(RecordType.TelnetDataIgnored, RecordType.TelnetDataSent, RecordType.TelnetDataReceived,
        RecordType.TelnetDataNotSupported,

        RecordType.CompressionStarted, RecordType.CompressionStopped, RecordType.ReplyWithRefusalOf,
        RecordType.ReplyWithConfirmationOf, RecordType.TerminalTypeSent);
  }

  public Predicate<Record> build() {
    return r -> typesAllowed.contains(r.getRecordType()) && payloadPredicate.test(r.getPayload());

  }

}
