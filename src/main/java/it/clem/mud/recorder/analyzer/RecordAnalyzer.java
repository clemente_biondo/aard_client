package it.clem.mud.recorder.analyzer;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.protocol.telnet.reader.impl.TelnetReaderImpl;
import it.clem.mud.recorder.Record;
import it.clem.mud.recorder.RecorderThread;
import it.clem.mud.shared.streams.impl.SourceStreamImpl;
import it.clem.mud.shared.telnet.TelnetDataFactory;

public class RecordAnalyzer {
  private final File baseDirectory;
  private final RecordFormatter recordFormatter = new TelnetRecordFormatter();
  private final static Logger log = LoggerFactory.getLogger(RecordAnalyzer.class);
  private final TelnetDataFactory tFactory = new TelnetDataFactory();
  
  public RecordAnalyzer(String baseDirectoryStr) {
    super();
    this.baseDirectory = new File(baseDirectoryStr);
    if (!this.baseDirectory.exists() || !this.baseDirectory.isDirectory()) {
      throw new IllegalArgumentException(baseDirectoryStr);
    }
  }

  public void decodeInputStream() throws IOException {
    File fin = new File(baseDirectory, RecorderThread.INPUT_FILE);
    if (!fin.exists()) {
      return;
    }
    try (TelnetReader tr = new TelnetReaderImpl(new SourceStreamImpl(new FileInputStream(fin)),tFactory);
        OutputStream out = new FileOutputStream(new File(baseDirectory, RecorderThread.INPUT_FILE + ".decoded"));) {
      TelnetData data;
      while (!(data = tr.read()).itDenotesConnectionClosedByServer()) {
        out.write((data.toString() + "\n").getBytes());
      }
    }
  }

  public void decodeOutputStream() throws IOException {
    File fout = new File(baseDirectory, RecorderThread.OUTPUT_FILE);
    if (!fout.exists()) {
      return;
    }
    try (TelnetReader tr = new TelnetReaderImpl(new SourceStreamImpl(new FileInputStream(fout)),tFactory);
        OutputStream out = new FileOutputStream(new File(baseDirectory, RecorderThread.OUTPUT_FILE + ".decoded"));) {
      TelnetData data;
      while (!(data = tr.read()).itDenotesConnectionClosedByServer()) {
        out.write((data.toString() + "\n").getBytes());
      }
    }
  }

  public void decodeSession() throws IOException {
    File fin = new File(baseDirectory, RecorderThread.SESSION_FILE);
    if (!fin.exists()) {
      return;
    }
    try (
        ObjectInputStream is = new ObjectInputStream(
            new GZIPInputStream(new FileInputStream(new File(baseDirectory, RecorderThread.SESSION_FILE))));
        OutputStream all = new FileOutputStream(new File(baseDirectory, RecorderThread.SESSION_FILE + ".all.decoded"));
        OutputStream telnet = new FileOutputStream(
            new File(baseDirectory, RecorderThread.SESSION_FILE + ".telnet.decoded"));
        OutputStream events = new FileOutputStream(
            new File(baseDirectory, RecorderThread.SESSION_FILE + ".events.decoded"));
        OutputStream notext = new FileOutputStream(
            new File(baseDirectory, RecorderThread.SESSION_FILE + ".notext.decoded"));) {
      try {
        while (true) {
          Record rec = (Record) is.readObject();
          if(rec==null) {
            continue;
          }
          all.write(recordFormatter.format(rec).getBytes());
          if (RecordFilterBuilder.ONLY_TELNET_COMMANDS.test(rec)) {
            telnet.write(recordFormatter.format(rec).getBytes());
          }
          if (RecordFilterBuilder.ONLY_EVENTS.test(rec)) {
            events.write(recordFormatter.format(rec).getBytes());
          }
          if (RecordFilterBuilder.ALL_WITHOUT_TEXT.test(rec)) {
            notext.write(recordFormatter.format(rec).getBytes());
          }

        }
      } catch (EOFException exc) {

      } catch (ClassNotFoundException cce) {
        log.error(cce.getMessage(), cce);
      }

    }

  }

  public void decodeAll() throws IOException {
    this.decodeInputStream();
    this.decodeOutputStream();
    this.decodeSession();
  }
}
