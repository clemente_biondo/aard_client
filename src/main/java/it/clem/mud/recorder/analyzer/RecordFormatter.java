package it.clem.mud.recorder.analyzer;

import it.clem.mud.recorder.Record;

public interface RecordFormatter {
  String format(Record r);
}
