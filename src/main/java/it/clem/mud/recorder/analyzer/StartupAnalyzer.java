package it.clem.mud.recorder.analyzer;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.gedt.RunnableGameLoop;

public class StartupAnalyzer implements Runnable {
  private final static Logger log = LoggerFactory.getLogger(StartupAnalyzer.class);

  public static void main(String[] args) throws Exception {
    new StartupAnalyzer().run();
  }

  @Override
  public void run() {

    String[] dirsSorted = new File(RunnableGameLoop.BASE_DIRECTORY)
        .list((f, n) -> f.isDirectory() && n.startsWith("2"));

    Arrays.sort(dirsSorted, Collections.reverseOrder());
    if (dirsSorted.length > 0) {
      String directoryToAnalyze = RunnableGameLoop.BASE_DIRECTORY + dirsSorted[0];
      try {

        new RecordAnalyzer(directoryToAnalyze).decodeAll();
        Desktop.getDesktop().open(new File(directoryToAnalyze));
      } catch (IOException ioe) {
        log.error(ioe.getMessage(), ioe);
      }
    }

  }

}
