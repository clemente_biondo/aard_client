package it.clem.mud.recorder.analyzer;

import it.clem.mud.recorder.Record;
import it.clem.mud.recorder.RecordType;

public class TelnetRecordFormatter implements RecordFormatter {

  @Override
  public String format(Record r) {
    StringBuilder sb = new StringBuilder();
    
    if(r.getRecordType() == RecordType.TelnetDataSent) {
      sb.append(">>");  
    } else if(r.getRecordType() ==RecordType.TelnetDataReceived) {
      sb.append("<<");  
    }else{
      sb.append(r.getRecordType());
      sb.append(":");
    }
    sb.append(r.getPayload().orElse(""));
    sb.append("\n");
    return sb.toString();
  }

}
