package it.clem.mud.recorder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.shared.recording.RecordConsumer;

public class RecorderThread extends Thread {
  private final File           recordingDirectory;
  private final RecordConsumer recordConsumer;
  private final static Logger  log         = LoggerFactory.getLogger(RecorderThread.class);
  public final static String   THREAD_NAME = "recorder";
  public final static String INPUT_FILE="in.rec";
  public final static String OUTPUT_FILE="out.rec";
  public final static String SESSION_FILE="session.rec";

  public RecorderThread(File recordingDirectory, RecordConsumer recordConsumer) {
    super(THREAD_NAME);
    this.recordingDirectory = recordingDirectory;
    this.recordConsumer = recordConsumer;
  }

  @Override
  public void run() {
    log.debug("thread started.");
    try (ObjectOutputStream oos = new ObjectOutputStream(
        new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory,SESSION_FILE )),true))) {
      //non ho motivo di interrompere dato che posso inviare una poison pill
      //lo lascio giusto per non mettere un while true e per facilitare i test
      while (!Thread.currentThread().isInterrupted()) {
        Record rec = recordConsumer.take();
        oos.writeObject(rec);
        oos.flush();
        if (rec.getRecordType() == RecordType.StopRecordingSession) {
          log.debug("thread terminato da evento..");
          break;
        }
      }
    } catch (Exception ex) {
      log.error("thread terminato a seguito di eccezione:" + ex.getMessage(), ex);
    }
    if (Thread.currentThread().isInterrupted()) {
      log.debug("thread terminato a seguito di interruzione ");
    }
    log.debug("thread terminato.");
  }

}
