package it.clem.mud.recorder;

public enum RecordType {
  GameEvent, 
  TelnetDataIgnored, 
  TelnetDataSent, 
  TelnetDataReceived, 
  CompressionStarted, 
  CompressionStopped, 
  TelnetDataNotSupported,
  StopRecordingSession, 
  ReplyWithRefusalOf, 
  ReplyWithConfirmationOf, 
  TerminalTypeSent, 
  GMCPHandshakeSent
}
