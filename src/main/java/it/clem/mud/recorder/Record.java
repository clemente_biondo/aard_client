package it.clem.mud.recorder;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

import javax.annotation.concurrent.Immutable;

@Immutable
public class Record implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 3434337297058420565L;

  private final LocalDateTime timestamp = LocalDateTime.now();

  private final RecordType recordType;

  private final Serializable payload;

  public Record(RecordType recordType, Serializable payload) {
    super();
    this.recordType = recordType;
    this.payload = payload;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public RecordType getRecordType() {
    return recordType;
  }

  public Optional<Serializable> getPayload() {
    return Optional.ofNullable(payload);
  }
  
  @Override
  public String toString() {
    return getRecordType()+":"+payload;
  }
}
