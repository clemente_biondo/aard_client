package it.clem.mud.client.console;

import java.util.concurrent.LinkedBlockingQueue;

import it.clem.mud.gedt.RunnableGameLoop;
import it.clem.mud.protocol.AardProtocolResources;
import it.clem.mud.protocol.data.handlers.NVTSupport;
import it.clem.mud.shared.game.GameStrategy;
import it.clem.mud.shared.game.event.GameEventFactoryImpl;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.game.event.queue.GameEventQueueImpl;
import it.clem.mud.shared.recording.RecordQueue;
import it.clem.mud.shared.recording.impl.RecordQueueImpl;
import it.clem.mud.shared.view.console.ConsoleClientCommunicationStrategy;

public class ConsoleClient {
  //java -cp telnet_study-0.0.2.jar it.clem.mud.game.stdout.StdoutGame
  public static void main(String[] args) {
    
    GameEventQueue eventQueue = new GameEventQueueImpl(new LinkedBlockingQueue<>(),new GameEventFactoryImpl());
    RecordQueue recordQueue = new RecordQueueImpl(new LinkedBlockingQueue<>());
    RunnableGameLoop gameLoop = new RunnableGameLoop(AardProtocolResources.FURRYMUCK_SERVER,eventQueue, NVTSupport.Minimal,  GameStrategy.Minimal, new ConsoleClientCommunicationStrategy(),recordQueue);
    new Thread(gameLoop, RunnableGameLoop.THREAD_NAME).start();
    try {
      while (true) {
        String line = System.console().readLine();
        if (line== null ||line.equals(".termina")) {
          break;
        }
        eventQueue.fireTextSentFromClientEvent(line);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    eventQueue.fireDisconnectByClientEvent();
  }

}
