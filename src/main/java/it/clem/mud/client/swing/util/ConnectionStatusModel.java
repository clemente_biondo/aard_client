package it.clem.mud.client.swing.util;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JMenuItem;

import it.clem.mud.client.swing.actions.DisconnectAction;
import it.clem.mud.client.swing.actions.WaitFortConnectionAction;
import it.clem.mud.shared.game.event.queue.GameEventQueue;

public class ConnectionStatusModel {
  private final JButton toggleConnectionToolbarButton = new JButton();
  private final JMenuItem connettiMenuItem = new JMenuItem();
  private final JMenuItem disConnettiMenuItem;
  private final DisconnectAction disconnectAction;
  private final WaitFortConnectionAction waitingForConnectionAction = new WaitFortConnectionAction();

  public ConnectionStatusModel(GameEventQueue eventQueue) {
    super();
    this.disConnettiMenuItem = new JMenuItem(disconnectAction=new DisconnectAction(eventQueue));
  }

  public void closeConnection(Action connectAction) {
    connettiMenuItem.setAction(connectAction);
    connettiMenuItem.setEnabled(true);
    disConnettiMenuItem.setEnabled(false);
    toggleConnectionToolbarButton.setAction(connectAction);
    toggleConnectionToolbarButton.setText(null);
  }

  public void waitForConnection() {
    connettiMenuItem.setAction(waitingForConnectionAction);
    connettiMenuItem.setEnabled(false);
    disConnettiMenuItem.setEnabled(false);
    toggleConnectionToolbarButton.setAction(waitingForConnectionAction);
    toggleConnectionToolbarButton.setText(null);
  }

  public void startConnection(Action connectAction) {
    connettiMenuItem.setAction(connectAction);
    connettiMenuItem.setEnabled(false);
    disConnettiMenuItem.setEnabled(true);
    toggleConnectionToolbarButton.setAction(disconnectAction);
    toggleConnectionToolbarButton.setText(null);
  }

  public JButton getToggleConnectionToolbarButton() {
    return toggleConnectionToolbarButton;
  }

  public JMenuItem getConnettiMenuItem() {
    return connettiMenuItem;
  }

  public JMenuItem getDisConnettiMenuItem() {
    return disConnettiMenuItem;
  }


}