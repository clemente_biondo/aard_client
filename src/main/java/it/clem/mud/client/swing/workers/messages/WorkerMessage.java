package it.clem.mud.client.swing.workers.messages;

import javax.annotation.concurrent.Immutable;


//oggetti creati in game-loop thread
@Immutable
public interface WorkerMessage {
  
  //metodo eseguito in edt
  void handleMessage();
}
