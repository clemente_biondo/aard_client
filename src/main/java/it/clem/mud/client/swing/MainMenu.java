package it.clem.mud.client.swing;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import it.clem.mud.client.swing.actions.ExitAppAction;
import it.clem.mud.client.swing.actions.RefreshGMCPData;
import it.clem.mud.client.swing.actions.ShowLogsAction;
import it.clem.mud.client.swing.util.ConnectionStatusController;

public class MainMenu extends JMenuBar {

  /**
   * 
   */
  private static final long serialVersionUID = -7988307263357195727L;

  public MainMenu(ExitAppAction exitAction,RefreshGMCPData refreshGMCPData, ConnectionStatusController connCtrl) {
    JMenu menuFile = new JMenu("File");
    menuFile.add(refreshGMCPData);
    menuFile.add(new ShowLogsAction());
    menuFile.addSeparator();
    menuFile.add(connCtrl.getConnettiMenuItem() );
    menuFile.add(connCtrl.getDisConnettiMenuItem());
    menuFile.addSeparator();
    menuFile.add(exitAction);
    add(menuFile);
  }

}
