package it.clem.mud.client.swing;

import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledEditorKit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.util.colors.xterm.XTermColorParser;
import it.clem.mud.util.colors.xterm.XtermColoredText;

public class OutputPanel extends JEditorPane {
  /**
   * 
   */
  private static final long serialVersionUID = -1980672282932852192L;
  private final static Logger log = LoggerFactory.getLogger(OutputPanel.class);
  private final XTermColorParser colorParser = new XTermColorParser();

  public OutputPanel() {
    this(false);
  }

  public OutputPanel(boolean smallFont) {
    super();
    putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
    setBackground(Color.BLACK);
    setForeground(Color.WHITE);
    setEditable(false);
    setFont(smallFont ? SwingResources.MONO_FONT_SMALL : SwingResources.MONO_FONT);
    setEditorKit(new StyledEditorKit());
  }

  public void appendText(String text) {
    // SmallAttributeSet
    // http://javatechniques.com/blog/faster-jtextpane-text-insertion-part-i/
    Document doc = getDocument();
    try {
      for (XtermColoredText ct : colorParser.parseText(text)) {
        doc.insertString(doc.getLength(), ct.getText(), ct.getColor().toSwingAttributeSet());
      }
    } catch (BadLocationException e) {
      log.error(e.getMessage(), e);
    }
    setCaretPosition(doc.getLength());
  }

  public void clearText() {
    setText("");

  }
}
