package it.clem.mud.client.swing.workers.messages;

import javax.swing.Action;

import it.clem.mud.client.swing.util.ConnectionStatusModel;

public class ServerConnectionStarted implements WorkerMessage {
  private final ConnectionStatusModel model;
  private final Action connectAction;
  
  public ServerConnectionStarted(ConnectionStatusModel model,Action connectAction) {
    super();
    this.model=model;
    this.connectAction=connectAction;
  }

  @Override
  public void handleMessage() {
    model.startConnection(connectAction);
  }

}
