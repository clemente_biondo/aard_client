package it.clem.mud.client.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JFrame;
import it.clem.mud.client.swing.actions.ExitAppAction;
import it.clem.mud.client.swing.actions.RefreshGMCPData;
import it.clem.mud.client.swing.util.ConnectionStatusController;
import it.clem.mud.client.swing.util.MonoComponentFocusTraversalPolicy;
import it.clem.mud.client.swing.util.RedirectFocusListener;
import it.clem.mud.shared.game.event.GameEventFactoryImpl;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.game.event.queue.GameEventQueueImpl;

public class MainWindow extends JFrame {
  private static final long serialVersionUID = 1L;
  private final static Dimension DFT_SIZE = new Dimension(1024, 768);



  public MainWindow() throws HeadlessException {
    this(new GameEventQueueImpl(new LinkedBlockingQueue<>(), new GameEventFactoryImpl()),"Aardwolf Client");
  }

  public MainWindow(GameEventQueue eventQueue,String title) throws HeadlessException {
    super(title);

    //creazione subcomponenti
    final OutputPanel outputPane = new OutputPanel();
    final OutputPanel mapPanel = new OutputPanel(true);
    final UserInputBar inputBar= new UserInputBar(eventQueue);
    final MainPanel mainPanel = new MainPanel(inputBar, outputPane, mapPanel);
    final ConnectionStatusController connectionStatusController = getConnectionStatusController(eventQueue, outputPane,
        mapPanel);
    final RefreshGMCPData refreshGMCPData = new RefreshGMCPData(eventQueue);
    
    //layout frame
    setPreferredSize(DFT_SIZE);
    setSize(DFT_SIZE);
    setLocationRelativeTo(null);

    // cosa fare in caso di chiusura
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    final ExitAppAction exitAction = new ExitAppAction(this);
    addWindowListener(exitAction);

    // layout dei sottosistemi
    setJMenuBar(new MainMenu(exitAction,refreshGMCPData, connectionStatusController));
    add(new MainToolbar(exitAction, connectionStatusController), BorderLayout.PAGE_START);
    add(mainPanel, BorderLayout.CENTER);
    add(new MainStatusBar(), BorderLayout.PAGE_END);

    // indirizzare sempre il focus sull'input bar
    setFocusTraversalPolicy(new MonoComponentFocusTraversalPolicy(inputBar));
    outputPane.addKeyListener(new RedirectFocusListener(inputBar));
  }
  
  // E' protected per consentire i test
  protected ConnectionStatusController getConnectionStatusController(GameEventQueue eventQueue,
      final OutputPanel outputPane, final OutputPanel mapPanel) {
    return new ConnectionStatusController(eventQueue, this, outputPane,mapPanel);
    
  }

}
