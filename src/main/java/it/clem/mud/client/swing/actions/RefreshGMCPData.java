package it.clem.mud.client.swing.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import it.clem.mud.client.swing.Resources;
import it.clem.mud.shared.game.event.queue.ClientEventProducer;

public class RefreshGMCPData extends AbstractAction {
  /**
   * 
   */
  private static final long serialVersionUID = -7577177241555192747L;
  private final ClientEventProducer producer;

  public RefreshGMCPData(ClientEventProducer producer) {
    super("Refresh GMCP Data", Resources.REFRESH_GMCP_DATA_ICON);
    this.producer = producer;
    putValue(SHORT_DESCRIPTION, "Richiede tutte le info disponibili da GMCP");
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    producer.GMCPRefreshAll();
  }

}
