package it.clem.mud.client.swing;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JToolBar;

import it.clem.mud.client.swing.actions.ExitAppAction;
import it.clem.mud.client.swing.util.ConnectionStatusController;

public class MainToolbar extends JToolBar {

  /**
   * 
   */
  private static final long serialVersionUID = -3440397521609615598L;

  public MainToolbar(ExitAppAction exitAction,ConnectionStatusController connCtrl) {
    append(exitAction);
    add(connCtrl.getToggleConnectionToolbarButton());
  }

  private void append(Action a) {
    JButton btn = new JButton(a);
    btn.setText(null);
    add(btn);
  }

}
