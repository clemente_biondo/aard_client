package it.clem.mud.client.swing;

import java.awt.Font;

public class SwingResources {
  public final static Font MONO_FONT = new Font("Courier New", Font.PLAIN, 18);
  public final static Font MONO_FONT_SMALL = new Font("Courier New", Font.PLAIN, 16);
}
