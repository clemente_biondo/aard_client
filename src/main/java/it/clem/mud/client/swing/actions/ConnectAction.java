package it.clem.mud.client.swing.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;

import it.clem.mud.client.swing.Resources;
import it.clem.mud.client.swing.util.ConnectionStatusModel;

public class ConnectAction extends AbstractAction {
  /**
   * 
   */
  private static final long serialVersionUID = -9105251779728378457L;
  private final SwingWorker<?,?> connectWorker;
  private final ConnectionStatusModel model;

  public ConnectAction(ConnectionStatusModel model, SwingWorker<?,?> connectWorker) {
    super("Connetti", Resources.CONNECT_ICON);
    this.connectWorker = connectWorker;
    this.model = model;
    putValue(SHORT_DESCRIPTION, "Apri una connessione verso il server");
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    model.waitForConnection();
    connectWorker.execute();
    // new GameEventLoopWorker(eventQueue,factory).execute();
  }

}
