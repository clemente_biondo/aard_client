package it.clem.mud.client.swing.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CommandHistory {
  private final List<String> history = new LinkedList<>();
  private int                currPos;                     // va da 0 a size

  public CommandHistory() {
    super();
    resetHistoryPosition();
  }

  public void appendCommand(String cmd) {
    if (cmd.length() > 3) {
      int delPos = history.indexOf(cmd);
      if (delPos != -1) {
        history.remove(delPos);
      }
      history.add(cmd);
    }
    resetHistoryPosition();
  }

  public Optional<String> getNext() {
    if (currPos < history.size() - 1) {
      currPos++;
      return Optional.of(history.get(currPos));
    }
    return Optional.empty();
  }

  public Optional<String> getPrev() {
    if (currPos > 0) {
      currPos--;
      return Optional.of(history.get(currPos));
    }
    return Optional.empty();
  }

  private void resetHistoryPosition() {
    currPos = history.size();
  }

  @Override
  public String toString() {
    return "CommandHistory [history=" + history + "]";
  }

}
