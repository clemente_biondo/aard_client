package it.clem.mud.client.swing.workers.messages;

public interface WorkerMessageFactory {

  WorkerMessage getTextForClientMessage(String text);

  WorkerMessage getMapTagUpdateMessage(String map);

  WorkerMessage getServerErrorMessage(String text);

  WorkerMessage getServerConnectionStarted();

  WorkerMessage getServerConnectionClosed();

}