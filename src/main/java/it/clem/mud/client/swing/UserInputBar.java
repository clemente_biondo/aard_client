package it.clem.mud.client.swing;

import javax.swing.JTextField;

import it.clem.mud.client.swing.actions.InputBarKeyListener;
import it.clem.mud.shared.game.event.queue.GameEventQueue;

public class UserInputBar extends JTextField {
  /**
   * 
   */
  private static final long serialVersionUID = -6136229625461535241L;

  public UserInputBar(GameEventQueue eventQueue) {
    setFont(SwingResources.MONO_FONT);
    addKeyListener(new InputBarKeyListener(eventQueue));
  }
}
