package it.clem.mud.client.swing.workers.messages;

import java.awt.Window;

import javax.swing.JOptionPane;

public class ServerErrorToShow implements WorkerMessage {
  private final String text;
  private final Window window;

  public ServerErrorToShow(String text, Window window) {
    super();
    this.text = text;
    this.window=window;
  }

  @Override
  public void handleMessage() {
    JOptionPane.showMessageDialog(window, text, "Server error", JOptionPane.ERROR_MESSAGE);
  }

}
