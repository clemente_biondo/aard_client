package it.clem.mud.client.swing.workers;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.SwingWorker;

import it.clem.mud.client.swing.workers.messages.WorkerMessage;
import it.clem.mud.client.swing.workers.messages.WorkerMessageFactory;
import it.clem.mud.gedt.RunnableGameLoop;
import it.clem.mud.protocol.AardProtocolResources;
import it.clem.mud.protocol.data.handlers.NVTSupport;
import it.clem.mud.shared.game.GameStrategy;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.recording.RecordQueue;
import it.clem.mud.shared.recording.impl.RecordQueueImpl;
import it.clem.mud.shared.view.ClientCommunicationStrategy;
import it.clem.mud.util.HostWithPort;

public class GameEventLoopWorker extends SwingWorker<Void, WorkerMessage> implements ClientCommunicationStrategy {
  private final WorkerMessageFactory factory;
  private final GameEventQueue eventQueue;

  public GameEventLoopWorker(GameEventQueue eventQueue, WorkerMessageFactory factory) {
    super();
    this.factory = factory;
    this.eventQueue = eventQueue;
  }

  @Override
  protected Void doInBackground() throws Exception {
    // GameEventQueue eventQueue = new GameEventQueueImpl(new LinkedBlockingQueue<>(), new GameEventFactoryImpl());
    RecordQueue recordQueue = new RecordQueueImpl(new LinkedBlockingQueue<>());
    RunnableGameLoop gameLoop = geRunnableGameLoop(AardProtocolResources.AARD_SERVER, eventQueue, NVTSupport.Aard,
        GameStrategy.Passive, this, recordQueue);
    Thread.currentThread().setName(RunnableGameLoop.THREAD_NAME);
    gameLoop.run();
    return null;
  }

  // E' protected per consentire i test
  protected RunnableGameLoop geRunnableGameLoop(HostWithPort server, GameEventQueue eventBus, NVTSupport nvtSupport,
      GameStrategy gameStrategy, ClientCommunicationStrategy clientStrategy, RecordQueue recordQueue) {
    return new RunnableGameLoop(server, eventBus, nvtSupport, gameStrategy,
        clientStrategy, recordQueue);
  }
  
 

  @Override
  protected void process(List<WorkerMessage> messages) {
    for (WorkerMessage m : messages) {
      m.handleMessage();
    }
  }

  // invocato in game-loop thread
  @Override
  public void sendTextToClient(String text) {
    publish(factory.getTextForClientMessage(text));
  }

  // invocato in game-loop thread
  @Override
  public void notifyServerError(String error) {
    publish(factory.getServerErrorMessage(error));
  }

  // invocato in game-loop thread
  @Override
  public void notifyServerStarted() {
    publish(factory.getServerConnectionStarted());
  }

  // invocato in game-loop thread
  @Override
  public void notifyServerClosed() {
    publish(factory.getServerConnectionClosed());
  }

  // invocato in game-loop thread
  @Override
  public void updateMapTag(String map) {
    publish(factory.getMapTagUpdateMessage(map));
  }
}
