package it.clem.mud.client.swing.workers.messages;

import java.awt.Window;

import javax.swing.Action;

import it.clem.mud.client.swing.OutputPanel;
import it.clem.mud.client.swing.util.ConnectionStatusModel;

public class WorkerMessageFactoryImpl implements WorkerMessageFactory {
  private final OutputPanel out;
  private final OutputPanel mapPanel;
  private final Window window;
  private final ConnectionStatusModel model;
  private Action connectAction;// escluso dal costruttore per un problema di referenza circolare

  public WorkerMessageFactoryImpl(OutputPanel mapPanel, OutputPanel out, Window window, ConnectionStatusModel model) {
    super();
    this.mapPanel = mapPanel;
    this.out = out;
    this.window = window;
    this.model = model;
  }

  public void setConnectAction(Action connectAction) {
    this.connectAction = connectAction;
  }

  /*
   * (non-Javadoc)
   * @see it.clem.mud.client.swing.workers.messages.WorkerMessageFactory#getTextForClientMessage(java.lang.String)
   */
  @Override
  public WorkerMessage getTextForClientMessage(String text) {
    return new OutputTextToShow(text, out);
  }

  /*
   * (non-Javadoc)
   * @see it.clem.mud.client.swing.workers.messages.WorkerMessageFactory#getMapTagUpdateMessage(java.lang.String)
   */
  @Override
  public WorkerMessage getMapTagUpdateMessage(String map) {
    return new UpdateMapTag(map, mapPanel);
  }

  /*
   * (non-Javadoc)
   * @see it.clem.mud.client.swing.workers.messages.WorkerMessageFactory#getServerErrorMessage(java.lang.String)
   */
  @Override
  public WorkerMessage getServerErrorMessage(String text) {
    return new ServerErrorToShow(text, window);
  }

  /*
   * (non-Javadoc)
   * @see it.clem.mud.client.swing.workers.messages.WorkerMessageFactory#getServerConnectionStarted()
   */
  @Override
  public WorkerMessage getServerConnectionStarted() {
    return new ServerConnectionStarted(model, connectAction);
  }

  /*
   * (non-Javadoc)
   * @see it.clem.mud.client.swing.workers.messages.WorkerMessageFactory#getServerConnectionClosed()
   */
  @Override
  public WorkerMessage getServerConnectionClosed() {
    return new ServerConnectionClosed(model, connectAction);
  }

}
