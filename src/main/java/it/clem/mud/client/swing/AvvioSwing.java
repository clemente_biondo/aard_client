package it.clem.mud.client.swing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AvvioSwing {
  @SuppressWarnings("unused")
  private final static Logger log=LoggerFactory.getLogger(AvvioSwing.class);
  public static void main(String[] args) {
    // Schedule a job for the event-dispatching thread: creating and showing this application's GUI.Create the GUI and
    // show it. For thread safety,this method should be invoked from thee vent-dispatching thread.
    javax.swing.SwingUtilities.invokeLater(() -> {
//      UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
//      javax.swing.plaf.metal.MetalLookAndFeel
//      javax.swing.plaf.nimbus.NimbusLookAndFeel
//      com.sun.java.swing.plaf.motif.MotifLookAndFeel
//      com.sun.java.swing.plaf.windows.WindowsLookAndFeel
//      com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel
//
//      
//      try {
//        UIManager.setLookAndFeel ( "javax.swing.plaf.nimbus.NimbusLookAndFeel" );
//      } catch (Exception e) {
//        log.error(e.getMessage(),e);
//        return;
//      }
//      
      MainWindow main = new MainWindow();
      main.pack();
      main.setVisible(true);
      
      //mostraOutputSalvato(main);
    });
  }

  
//  private static void mostraOutputSalvato(MainWindow main) {
//    try {
//      main.sendTextToClient(IOUtils.toString(new FileInputStream("D:\\eclipse_oxygen\\bitbucket_aard_client_repository\\src\\test\\resources\\sess1\\in_only_text_uncompressed.bin")/*,AardProtocolResources.ENCODING_CHARSET*/));  
//    } catch(IOException ioe) {
//      ioe.printStackTrace();
//    }
//    
//  }
}
