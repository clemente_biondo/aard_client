package it.clem.mud.client.swing.util;

import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JMenuItem;

import it.clem.mud.client.swing.OutputPanel;
import it.clem.mud.client.swing.actions.ConnectAction;
import it.clem.mud.client.swing.workers.GameEventLoopWorker;
import it.clem.mud.client.swing.workers.messages.WorkerMessageFactoryImpl;
import it.clem.mud.shared.game.event.queue.GameEventQueue;

public class ConnectionStatusController {
  private final ConnectionStatusModel model;

  public ConnectionStatusController(GameEventQueue eventQueue, Window window, OutputPanel out, OutputPanel mapPanel) {
    super();
    this.model = new ConnectionStatusModel(eventQueue);
    WorkerMessageFactoryImpl factory = new WorkerMessageFactoryImpl(mapPanel, out, window, model);
    GameEventLoopWorker worker = getGameEventLoopWorker(eventQueue, factory);
    ConnectAction connectAction=new ConnectAction(model, worker);
    factory.setConnectAction(new ConnectAction(model, worker));
    this.model.closeConnection(connectAction);
  }
  // E' protected per consentire i test
  protected GameEventLoopWorker getGameEventLoopWorker(GameEventQueue eventQueue, WorkerMessageFactoryImpl factory) {
    GameEventLoopWorker worker = new GameEventLoopWorker(eventQueue, factory);
    return worker;
  }

  public JButton getToggleConnectionToolbarButton() {
    return model.getToggleConnectionToolbarButton();
  }

  public JMenuItem getConnettiMenuItem() {
    return model.getConnettiMenuItem();
  }

  public JMenuItem getDisConnettiMenuItem() {
    return model.getDisConnettiMenuItem();
  }
  // E' protected per consentire i test

}
