package it.clem.mud.client.swing.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import it.clem.mud.client.swing.Resources;
import it.clem.mud.recorder.analyzer.StartupAnalyzer;

public class ShowLogsAction extends AbstractAction {
  /**
   * 
   */
  private static final long serialVersionUID = -1556597825907953771L;

  public ShowLogsAction() {
    super("Logs", Resources.LOGS_ICON);
    putValue(SHORT_DESCRIPTION, "Apri la directory contenente i log files.");
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    new Thread(new StartupAnalyzer()).start();

  }

}
