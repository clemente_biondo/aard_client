package it.clem.mud.client.swing.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import it.clem.mud.client.swing.Resources;

public class WaitFortConnectionAction extends AbstractAction {
  /**
   * 
   */
  private static final long serialVersionUID = 3623897735436918425L;

  public WaitFortConnectionAction() {
    super("Connessione...", Resources.WAIT_ICON);
    putValue(SHORT_DESCRIPTION, "In attesa di connessione...");
  }

  @Override
  public void actionPerformed(ActionEvent e) {
  }
  
}
