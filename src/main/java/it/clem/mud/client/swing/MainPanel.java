package it.clem.mud.client.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class MainPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = -1855095475649807583L;

  public MainPanel(UserInputBar inputBar, OutputPanel outputPane, OutputPanel mapPanel) {
    super();
    setLayout(new BorderLayout());
    applyLayeredLayout(outputPane, mapPanel);
    // add(new JScrollPane(outputPane), BorderLayout.CENTER);
    add(inputBar, BorderLayout.PAGE_END);
  }

  private void applyLayeredLayout(OutputPanel outputPane, OutputPanel mapPanel) {
    JLayeredPane layeredPane = new JLayeredPane();
    JScrollPane sp = new JScrollPane(outputPane);
    
    mapPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
    layeredPane.add(sp, JLayeredPane.DEFAULT_LAYER);
    layeredPane.add(mapPanel, JLayeredPane.PALETTE_LAYER);
    // outputPane.setBounds(0,0,100,100);
    layeredPane.addComponentListener(new ComponentAdapter() {
      
      @Override
      public void componentResized(ComponentEvent e) {
        Rectangle r = e.getComponent().getBounds();
        sp.setBounds(r);
        mapPanel.setBounds(r.width-315,0,295,380);
      }

    });
    add(layeredPane, BorderLayout.CENTER);
  }
  
}
