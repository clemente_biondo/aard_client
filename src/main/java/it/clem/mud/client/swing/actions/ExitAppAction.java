package it.clem.mud.client.swing.actions;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import it.clem.mud.client.swing.Resources;

public class ExitAppAction extends AbstractAction implements WindowListener {
  /**
   * 
   */
  private static final long serialVersionUID = 7464183696050741527L;
  private final Window window;

  public ExitAppAction(Window window) {
    super("Termina", Resources.EXIT_ICON);
    putValue(SHORT_DESCRIPTION, "Chiudi l'applicazione");
    this.window = window;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (JOptionPane.showConfirmDialog(window, "Terminare l'applicazione?") == JOptionPane.OK_OPTION) {
      window.setVisible(false);
      window.dispose();
    }
  }

  @Override
  public void windowOpened(WindowEvent e) {

  }

  @Override
  public void windowClosing(WindowEvent e) {
    actionPerformed(new ActionEvent(e, ActionEvent.ACTION_PERFORMED, null));
  }

  @Override
  public void windowClosed(WindowEvent e) {

  }

  @Override
  public void windowIconified(WindowEvent e) {

  }

  @Override
  public void windowDeiconified(WindowEvent e) {

  }

  @Override
  public void windowActivated(WindowEvent e) {

  }

  @Override
  public void windowDeactivated(WindowEvent e) {

  }

}
