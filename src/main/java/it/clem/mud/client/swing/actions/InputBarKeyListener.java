package it.clem.mud.client.swing.actions;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;

import it.clem.mud.client.swing.util.CommandHistory;
import it.clem.mud.shared.game.event.queue.ClientEventProducer;

public class InputBarKeyListener extends KeyAdapter {
  private final ClientEventProducer eventQueue;
  private final CommandHistory history = new CommandHistory();

  public InputBarKeyListener(ClientEventProducer eventQueue) {
    super();
    this.eventQueue = eventQueue;
  }

  public void keyPressed(KeyEvent e) {
    JTextField textField = (JTextField) e.getSource();
    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
      String text = StringUtils.trimToEmpty(textField.getText());
      eventQueue.fireTextSentFromClientEvent(text);
      textField.setText(null);
      history.appendCommand(text);
    } else if (e.getKeyCode() == KeyEvent.VK_UP) {
      history.getPrev().ifPresent(textField::setText);
    } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
      history.getNext().ifPresent(textField::setText);
    } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD8) {
      eventQueue.fireTextSentFromClientEvent("n");
    } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD2) {
      eventQueue.fireTextSentFromClientEvent("s");
    } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD4) {
      eventQueue.fireTextSentFromClientEvent("w");
    } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD6) {
      eventQueue.fireTextSentFromClientEvent("e");
    } else if (e.getKeyCode() == KeyEvent.VK_ADD) {
      eventQueue.fireTextSentFromClientEvent("d");
    } else if (e.getKeyCode() == KeyEvent.VK_SUBTRACT) {
      eventQueue.fireTextSentFromClientEvent("u");
    } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD5) {
      eventQueue.fireTextSentFromClientEvent("l");
    }
    //System.out.println(e);
  }
}
