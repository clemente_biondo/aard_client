package it.clem.mud.client.swing.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import it.clem.mud.client.swing.Resources;
import it.clem.mud.shared.game.event.queue.GameEventQueue;

public class DisconnectAction extends AbstractAction {
  /**
   * 
   */
  private static final long serialVersionUID = -90525592459765770L;
  private final GameEventQueue eventQueue;
  
  public DisconnectAction(GameEventQueue eventQueue) {
    super("Disconnetti", Resources.DISCONNECT_ICON);
    this.eventQueue=eventQueue;
    putValue(SHORT_DESCRIPTION, "Chiudi la connessione al server");
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    eventQueue.fireDisconnectByClientEvent();
  }
  
}
