package it.clem.mud.client.swing.workers.messages;

import it.clem.mud.client.swing.OutputPanel;

public class OutputTextToShow implements WorkerMessage {
  private final String text;
  private final OutputPanel out;

  public OutputTextToShow(String text, OutputPanel out) {
    super();
    this.text = text;
    this.out = out;
  }

  @Override
  public void handleMessage() {
    out.appendText(text);
  }

}
