package it.clem.mud.client.swing.util;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.text.JTextComponent;

public class RedirectFocusListener extends KeyAdapter{
  private final JTextComponent component;
  
  public RedirectFocusListener(JTextComponent component) {
    super();
    this.component = component;
  }

  @Override
  public void keyPressed(KeyEvent e) {
    if (!component.hasFocus()) {
      if (!e.isControlDown() /*&& e.getKeyCode() >= KeyEvent.VK_A && e.getKeyCode() <= KeyEvent.VK_Z*/) {
        component.setText(component.getText()+e.getKeyChar());
        component.requestFocusInWindow();
      }
    }
  }


}
