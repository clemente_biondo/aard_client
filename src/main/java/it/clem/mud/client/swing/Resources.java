package it.clem.mud.client.swing;

import javax.swing.ImageIcon;

public class Resources {
  public final static ImageIcon EXIT_ICON = new ImageIcon(Resources.class.getResource("/icons/exit_24.png"));
  public final static ImageIcon CONNECT_ICON = new ImageIcon(Resources.class.getResource("/icons/connect_24.png"));
  public final static ImageIcon DISCONNECT_ICON = new ImageIcon(Resources.class.getResource("/icons/disconnect_24.png"));
  public final static ImageIcon WAIT_ICON = new ImageIcon(Resources.class.getResource("/icons/timer_sand_24.png"));
  public final static ImageIcon LOGS_ICON = new ImageIcon(Resources.class.getResource("/icons/logs_24.png"));
  public final static ImageIcon REFRESH_GMCP_DATA_ICON = new ImageIcon(Resources.class.getResource("/icons/refresh_gmcp_data_24.png"));
  
}
