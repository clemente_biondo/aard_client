package it.clem.mud.client.swing;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

public class MainStatusBar extends JPanel{

  /**
   * 
   */
  private static final long serialVersionUID = 5205481632960144022L;

  public MainStatusBar() {
    super();
    setBorder(new BevelBorder(BevelBorder.LOWERED));
    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
  }

}
