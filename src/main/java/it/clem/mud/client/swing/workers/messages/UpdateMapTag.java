package it.clem.mud.client.swing.workers.messages;

import it.clem.mud.client.swing.OutputPanel;

public class UpdateMapTag implements WorkerMessage {
  private final String map;
  private final OutputPanel out;

  public UpdateMapTag(String map, OutputPanel out) {
    super();
    this.map = map;
    this.out = out;
  }

  @Override
  public void handleMessage() {
    out.clearText();
    out.appendText(map);
  }

}