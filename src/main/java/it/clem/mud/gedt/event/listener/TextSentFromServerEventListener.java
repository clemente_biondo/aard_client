package it.clem.mud.gedt.event.listener;


import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.server.TextSentFromServerEvent;

@FunctionalInterface
public interface TextSentFromServerEventListener extends GameEventListener{
  boolean onTextSentFromServer(TextSentFromServerEvent ev,GameContext gc);
}
