package it.clem.mud.gedt.event.listener;


import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.client.RequestGMCPDataEvent;

@FunctionalInterface
public interface RequestGMCPDataEventListener extends GameEventListener{
  boolean onGMCPDataRequestFromClient(RequestGMCPDataEvent  ev,GameContext gc);
}
