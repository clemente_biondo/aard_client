package it.clem.mud.gedt.event.bus.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import com.google.common.collect.ImmutableList;

import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.GameEvent;
import it.clem.mud.shared.game.event.GameEventType;

final class GameEventTypeListeners<T extends GameEvent> {

	private final List<BiFunction<T, GameContext,Boolean>> listeners;
	private final Class<T> clazz;
  private final GameEventType type;

	GameEventTypeListeners(GameEventType type, List<BiFunction<T, GameContext,Boolean>> listeners,Class<T> clazz) {
		this.type=type;
		this.listeners = ImmutableList.copyOf(listeners);
		this.clazz=clazz;
	}

	
	GameEventType getType() {
    return type;
  }

  void notifyEvent(GameEvent gameEvent, GameContext gc) {
    T event = clazz.cast(gameEvent);
		for (BiFunction<T, GameContext,Boolean> b : listeners) {
			if (b.apply(event, gc)){
			  return;
			}
		}
	}

	
	static <R extends GameEvent> Builder<R> builder(GameEventType type,Class<R> clazz) {
		return new Builder<R>(type,clazz);
	}


	static class Builder<R extends GameEvent> {
	  private final GameEventType type;
	  private final Class<R> clazz;
		private final List<BiFunction<R, GameContext,Boolean>> listeners = new ArrayList<>();

		Builder(GameEventType type,Class<R> clazz) {
      super();
      this.type = type;
      this.clazz=clazz;
    }

    Builder<R> register(BiFunction<R, GameContext,Boolean> listener) {
			listeners.add(listener);
			return this;
		}

		Builder<R> registerGenericListener(BiFunction<GameEvent, GameContext,Boolean> listener) {
		  listeners.add(listener::apply);
			return this;
		}

		GameEventTypeListeners<R> build() {
			return new GameEventTypeListeners<R>(type,listeners,clazz);
		}

	}
}
