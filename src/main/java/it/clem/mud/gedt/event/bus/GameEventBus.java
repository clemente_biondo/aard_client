package it.clem.mud.gedt.event.bus;

import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.GameEvent;

public interface GameEventBus {
    void dispatch(GameEvent ge,GameContext gc);
}
