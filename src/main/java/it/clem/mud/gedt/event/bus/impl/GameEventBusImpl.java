package it.clem.mud.gedt.event.bus.impl;

import java.util.Arrays;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

import it.clem.mud.gedt.event.bus.GameEventBus;
import it.clem.mud.gedt.event.bus.GameEventSubscriber;
import it.clem.mud.gedt.event.listener.ConnectionClosedByServerEventListener;
import it.clem.mud.gedt.event.listener.DisconnectByClientEventListener;
import it.clem.mud.gedt.event.listener.GameEventListener;
import it.clem.mud.gedt.event.listener.GenericGameEventListener;
import it.clem.mud.gedt.event.listener.MapTagUpdatedEventListener;
import it.clem.mud.gedt.event.listener.RequestGMCPDataEventListener;
import it.clem.mud.gedt.event.listener.ServerErrorEventListener;
import it.clem.mud.gedt.event.listener.TextSentFromClientEventListener;
import it.clem.mud.gedt.event.listener.TextSentFromServerEventListener;
import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.GameStrategy;
import it.clem.mud.shared.game.event.GameEvent;
import it.clem.mud.shared.game.event.GameEventType;
import it.clem.mud.shared.game.event.MinimalGameStrategy;
import it.clem.mud.shared.game.event.PassiveGameStrategy;
import it.clem.mud.shared.game.event.client.DisconnectByClientEvent;
import it.clem.mud.shared.game.event.client.RequestGMCPDataEvent;
import it.clem.mud.shared.game.event.client.TextSentFromClientEvent;
import it.clem.mud.shared.game.event.server.ConnectionClosedByServerEvent;
import it.clem.mud.shared.game.event.server.MapTagUpdatedEvent;
import it.clem.mud.shared.game.event.server.ServerErrorEvent;
import it.clem.mud.shared.game.event.server.TextSentFromServerEvent;

public class GameEventBusImpl implements GameEventBus {
  private final static Logger log = LoggerFactory.getLogger(GameEventBusImpl.class);
  private final Map<GameEventType,GameEventTypeListeners<? extends GameEvent>> map;
  // Nuovo evento:agigungere parametro qua
  @SafeVarargs
  private GameEventBusImpl(GameEventTypeListeners<? extends GameEvent>... listeners) {
    super();
    map=Arrays.stream(listeners).collect(ImmutableMap.toImmutableMap(GameEventTypeListeners::getType, Function.identity()));
  }


  @Override
  public void dispatch(GameEvent event, GameContext gc) {
    GameEventTypeListeners<? extends GameEvent> listener = map.get(event.getType());
    if(listener!= null){
      listener.notifyEvent(event, gc);
    } else {
      log.warn("Nessun listener per "+event);
    }
    
  }
  
  public static GameEventBus getBusByStrategy(GameStrategy strategy){
    switch(strategy){
      case None:return builder().build();
      case Minimal:return MinimalGameStrategy.getMinimalGameStrategy();
      case Passive:return PassiveGameStrategy.getPassiveGameStrategy();
      default:
        throw new UnsupportedOperationException();
    }
  }
  
  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    //BUILDER DEI TIPI EVENTO - IMPORTANTE -
    //@formatter:off
    private final GameEventTypeListeners.Builder<ConnectionClosedByServerEvent> connectionClosedByServerEventTypeBuilder = 
        GameEventTypeListeners.<ConnectionClosedByServerEvent>builder(GameEventType.ConnectionClosedByServer,ConnectionClosedByServerEvent.class);
    
    private final GameEventTypeListeners.Builder<DisconnectByClientEvent> disconnectByClientEventTypeBuilder = 
        GameEventTypeListeners.<DisconnectByClientEvent>builder(GameEventType.DisconnectByClient,DisconnectByClientEvent.class);
        
    private final GameEventTypeListeners.Builder<ServerErrorEvent> serverErrorEventTypeBuilder = 
        GameEventTypeListeners.<ServerErrorEvent>builder(GameEventType.ServerError,ServerErrorEvent.class);
    
    private final GameEventTypeListeners.Builder<TextSentFromServerEvent> textSentFromServerEventTypeBuilder = 
        GameEventTypeListeners.<TextSentFromServerEvent>builder(GameEventType.TextSentFromServer,TextSentFromServerEvent.class);

    private final GameEventTypeListeners.Builder<MapTagUpdatedEvent> mapTagUpdatedEventTypeBuilder = 
        GameEventTypeListeners.<MapTagUpdatedEvent>builder(GameEventType.MapTagUpdatedFromServer,MapTagUpdatedEvent.class);

    private final GameEventTypeListeners.Builder<TextSentFromClientEvent> textSentFromClientEventTypeBuilder = 
        GameEventTypeListeners.<TextSentFromClientEvent>builder(GameEventType.TextSentFromClient,TextSentFromClientEvent.class);
    
    private final GameEventTypeListeners.Builder<RequestGMCPDataEvent> requestGMCPDataEventTypeBuilder = 
        GameEventTypeListeners.<RequestGMCPDataEvent>builder(GameEventType.GMCPRequestFromClient,RequestGMCPDataEvent.class);
    
    // Nuovo evento:agigungere riga qua
    //@formatter:on
    
    public GameEventBusImpl build() {
      // Nuovo evento:agigungere parametro qua
      return new GameEventBusImpl(disconnectByClientEventTypeBuilder.build(),connectionClosedByServerEventTypeBuilder.build(), serverErrorEventTypeBuilder.build(),
          textSentFromServerEventTypeBuilder.build(), textSentFromClientEventTypeBuilder.build(),mapTagUpdatedEventTypeBuilder.build(),requestGMCPDataEventTypeBuilder.build());
    }

    public Builder addDisconnectByClientEventListener(DisconnectByClientEventListener listener) {
      disconnectByClientEventTypeBuilder.register(listener::onDisconnectByClient);
      return this;
    }

    public Builder addDisconnectByClientEventListener(BiConsumer<DisconnectByClientEvent,GameContext> listener) {
      disconnectByClientEventTypeBuilder.register((e,g)-> {listener.accept(e, g);return false;});
      return this;
    }
    public Builder addConnectionClosedByServerEventListener(ConnectionClosedByServerEventListener listener) {
      connectionClosedByServerEventTypeBuilder.register(listener::onConnectionClosedByServer);
      return this;
    }

    public Builder addNonConsumableConnectionClosedByServerEventListener(BiConsumer<ConnectionClosedByServerEvent,GameContext> listener) {
      connectionClosedByServerEventTypeBuilder.register((e,g)-> {listener.accept(e, g);return false;});
      return this;
    }

    public Builder addMapTagUpdatedEventListener(MapTagUpdatedEventListener listener) {
      mapTagUpdatedEventTypeBuilder.register(listener::onMapTagUpdated);
      return this;
    }

    public Builder addNonConsumableMapTagUpdatedEventListener(BiConsumer<MapTagUpdatedEvent,GameContext> listener) {
      mapTagUpdatedEventTypeBuilder.register((e,g)-> {listener.accept(e, g);return false;});
      return this;
    }
    
    public Builder addTextSentFromServerEventListener(TextSentFromServerEventListener listener) {
      textSentFromServerEventTypeBuilder.register(listener::onTextSentFromServer);
      return this;
    }
    public Builder addNonConsumableTextSentFromServerEventListener(BiConsumer<TextSentFromServerEvent,GameContext> listener) {
      textSentFromServerEventTypeBuilder.register((e,g)-> {listener.accept(e, g);return false;});
      return this;
    }

    public Builder addTextSentFromClientEventListener(TextSentFromClientEventListener listener) {
      textSentFromClientEventTypeBuilder.register(listener::onTextSentFromClient);
      return this;
    }
    public Builder addNonConsumableTextSentFromClientEventListener(BiConsumer<TextSentFromClientEvent,GameContext> listener) {
      textSentFromClientEventTypeBuilder.register((e,g)-> {listener.accept(e, g);return false;});
      return this;
    }
    
    public Builder addRequestGMCPDataEventListener(RequestGMCPDataEventListener listener) {
      requestGMCPDataEventTypeBuilder.register(listener::onGMCPDataRequestFromClient);
      return this;
    }    

    public Builder addNonConsumableRequestGMCPDataEventListener(BiConsumer<RequestGMCPDataEvent,GameContext> listener) {
      requestGMCPDataEventTypeBuilder.register((e,g)-> {listener.accept(e, g);return false;});
      return this;
    }

    
    // Nuovo evento:agigungere metodo come questo
    public Builder addServerErrorEventListener(ServerErrorEventListener listener) {
      serverErrorEventTypeBuilder.register(listener::onServerError);
      return this;
    }

    public Builder addNonConsumableServerErrorEventListener(BiConsumer<ServerErrorEvent,GameContext> listener) {
      serverErrorEventTypeBuilder.register((e,g)-> {listener.accept(e, g);return false;});
      return this;
    }
    
    public Builder subscribe(GameEventSubscriber subscriber) {
      for (GameEventListener l : subscriber) {
        add(l);
      }
      return this;
    }

    // Nuovo evento:agigungere riga qua
    public Builder addGenericEventListener(GenericGameEventListener listener) {
      requestGMCPDataEventTypeBuilder.register(listener::onGameEvent);
      textSentFromClientEventTypeBuilder.registerGenericListener(listener::onGameEvent);
      serverErrorEventTypeBuilder.registerGenericListener(listener::onGameEvent);
      textSentFromServerEventTypeBuilder.registerGenericListener(listener::onGameEvent);
      mapTagUpdatedEventTypeBuilder.register(listener::onGameEvent);
      connectionClosedByServerEventTypeBuilder.registerGenericListener(listener::onGameEvent);
      return this;
    }
    
    public Builder add(GameEventListener... listeners) {
      for(GameEventListener listener:listeners){
        if (listener instanceof ServerErrorEventListener) {
          addServerErrorEventListener((ServerErrorEventListener) listener);
        }
        if (listener instanceof TextSentFromServerEventListener) {
          addTextSentFromServerEventListener((TextSentFromServerEventListener) listener);
        }
        if (listener instanceof MapTagUpdatedEventListener) {
          addMapTagUpdatedEventListener((MapTagUpdatedEventListener) listener);
        }
        if (listener instanceof TextSentFromClientEventListener) {
          addTextSentFromClientEventListener((TextSentFromClientEventListener) listener);
        }
        if (listener instanceof RequestGMCPDataEventListener) {
          addRequestGMCPDataEventListener((RequestGMCPDataEventListener) listener);
        }
        // Nuovo evento:agigungere if come questa
        if (listener instanceof ConnectionClosedByServerEventListener) {
          addConnectionClosedByServerEventListener((ConnectionClosedByServerEventListener) listener);
        }
        if (listener instanceof GenericGameEventListener) {
          addGenericEventListener((GenericGameEventListener) listener);
        }
      }
      return this;
    }

  }


}
