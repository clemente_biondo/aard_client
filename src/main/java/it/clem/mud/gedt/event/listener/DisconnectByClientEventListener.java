package it.clem.mud.gedt.event.listener;

import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.client.DisconnectByClientEvent;

@FunctionalInterface
public interface DisconnectByClientEventListener extends GameEventListener{
    boolean onDisconnectByClient(DisconnectByClientEvent e,GameContext gc);
  }

