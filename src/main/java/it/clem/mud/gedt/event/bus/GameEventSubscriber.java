package it.clem.mud.gedt.event.bus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import it.clem.mud.gedt.event.listener.GameEventListener;

public class GameEventSubscriber implements Iterable<GameEventListener>{
  private final List<GameEventListener> listeners = new ArrayList<>();

  public void register(GameEventListener listener) {
    listeners.add(listener);
  }

  @Override
  public Iterator<GameEventListener> iterator() {
    return listeners.iterator();
  }
}
