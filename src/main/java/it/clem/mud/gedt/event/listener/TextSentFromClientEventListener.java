package it.clem.mud.gedt.event.listener;


import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.client.TextSentFromClientEvent;

@FunctionalInterface
public interface TextSentFromClientEventListener extends GameEventListener{
  boolean onTextSentFromClient(TextSentFromClientEvent ev,GameContext gc);
}
