package it.clem.mud.gedt.event.listener;

import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.server.ConnectionClosedByServerEvent;

@FunctionalInterface
public interface ConnectionClosedByServerEventListener extends GameEventListener{
  boolean onConnectionClosedByServer(ConnectionClosedByServerEvent e,GameContext gc);
}
