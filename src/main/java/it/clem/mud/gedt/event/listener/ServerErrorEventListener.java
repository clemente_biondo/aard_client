package it.clem.mud.gedt.event.listener;


import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.server.ServerErrorEvent;

@FunctionalInterface
public interface ServerErrorEventListener extends GameEventListener{
  boolean onServerError(ServerErrorEvent ev,GameContext gc);
}
