package it.clem.mud.gedt.event.listener;

import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.GameEvent;

@FunctionalInterface
public interface GenericGameEventListener extends GameEventListener{
  boolean onGameEvent(GameEvent ev,GameContext gc);

}
