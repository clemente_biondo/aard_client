package it.clem.mud.gedt.event.listener;


import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.server.MapTagUpdatedEvent;

@FunctionalInterface
public interface MapTagUpdatedEventListener extends GameEventListener{
  boolean onMapTagUpdated(MapTagUpdatedEvent ev,GameContext gc);
}
