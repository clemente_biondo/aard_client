package it.clem.mud.gedt;

import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.gedt.event.bus.GameEventBus;
import it.clem.mud.gedt.event.bus.impl.GameEventBusImpl;
import it.clem.mud.gedt.socket.AbstractSocket;
import it.clem.mud.gedt.socket.impl.AbstractSocketSocketBasedImpl;
import it.clem.mud.gedt.socket.impl.RecordableAbstractSocket;
import it.clem.mud.protocol.AardServerThread;
import it.clem.mud.protocol.data.handlers.NVTSupport;
import it.clem.mud.recorder.RecorderThread;
import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.GameContextImpl;
import it.clem.mud.shared.game.GameStrategy;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.recording.GameEventThreadDispatcherProducer;
import it.clem.mud.shared.recording.RecordQueue;
import it.clem.mud.shared.streams.TargetStream;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;
import it.clem.mud.shared.telnet.writer.impl.RecordableTelnetWriter;
import it.clem.mud.shared.telnet.writer.impl.TelnetWriterImpl;
import it.clem.mud.shared.view.ClientCommunicationStrategy;
import it.clem.mud.util.HostWithPort;
import it.clem.mud.util.LangUtils;

/**
 * Concorrenza. Sono presenti tre thread: client ui e server input che producono eventi e li accodano e questo che li
 * processa. Un evento è immutabile a meno dei listener collegati al relativo tipo. Ma dato che: If an object is
 * constructed in one thread and used in another, could another thread sees uninitialized or partially initialized
 * states of the object? Answer: No. Allora fin tanto che questi listener sono usati solo in questo thread non ci sono
 * problemi.
 */
public class RunnableGameLoop implements Runnable {
  private final Logger log = LoggerFactory.getLogger(RunnableGameLoop.class);

  public final static String THREAD_NAME = "game-loop";
  public final static String BASE_DIRECTORY = System.getProperty("user.home") + File.separator + "aard_client"
      + File.separator;
  private final RecordQueue recordQueue;
  private final GameEventQueue eventBus;
  private final ClientCommunicationStrategy clientStrategy;
  private final GameStrategy gameStrategy;
  private final NVTSupport nvtSupport;
  private final HostWithPort server;

  public RunnableGameLoop(HostWithPort server, GameEventQueue eventBus, NVTSupport nvtSupport,
      GameStrategy gameStrategy, ClientCommunicationStrategy clientStrategy, RecordQueue recordQueue) {
    // super(THREAD_NAME);
    this.eventBus = eventBus;
    this.server = server;
    this.gameStrategy = gameStrategy;
    this.clientStrategy = clientStrategy;
    this.nvtSupport = nvtSupport;
    this.recordQueue = recordQueue;
  }

  @Override
  public void run() {
    final File recordingDirectory = new File(BASE_DIRECTORY + LangUtils.nowAsYMDHmSS());
    recordingDirectory.mkdirs();
    new RecorderThread(recordingDirectory, recordQueue).start();
    Thread aardServerThread = null;
    AbstractSocket socket = null;
    try  {
      socket = getSocket(server, recordingDirectory);
      final TelnetDataFactory telnetDataFactory = new TelnetDataFactory();
      final TelnetWriter sender = getTelnetSerder(socket.getTargetStream(), recordQueue);
      aardServerThread = new AardServerThread(recordQueue, eventBus, socket.getSourceStream(), sender, nvtSupport,telnetDataFactory);
      aardServerThread.start();
      final GameContext gameContext = new GameContextImpl(telnetDataFactory,sender, clientStrategy);
      clientStrategy.notifyServerStarted();
      new GameLoop(eventBus, gameContext, getGameEventListenerBus(gameStrategy),recordQueue).doLoop();
      
    } catch (Exception e) {
      log.warn("thread game loop terminato da eccezione:" + e.getMessage(), e);
      clientStrategy.notifyServerError(e.getMessage());
    } finally {
      if (aardServerThread != null) {
        aardServerThread.interrupt();
      }
      if (socket != null) {
        try {
          socket.close();
        } catch (IOException e) {
          log.error(e.getMessage(),e);
        }
      }
      clientStrategy.notifyServerClosed();
      recordQueue.stopRecording();
    }

  }

  // E' protected per consentire i test
  protected TelnetWriter getTelnetSerder(TargetStream targetStream, GameEventThreadDispatcherProducer recordQueue) {
    return new RecordableTelnetWriter(new TelnetWriterImpl(targetStream), recordQueue);
  }

  // E' protected per consentire i test
  protected GameEventBus getGameEventListenerBus(GameStrategy gameStrategy) {
    return GameEventBusImpl.getBusByStrategy(gameStrategy);
  }

  // E' protected per consentire i test
  protected AbstractSocket getSocket(HostWithPort server, File recordingDirectory) throws IOException {
    return new RecordableAbstractSocket(new AbstractSocketSocketBasedImpl(server), recordingDirectory) ;
  }

  // public static RunnableGameLoop forTest(GameEventQueue eventBus, NVTSupport nvtSupport, GameStrategy gameStrategy,
  // ClientCommunicationStrategy clientStrategy, SourceStream sourceStream, TargetStream targetStream) {
  // return new RunnableGameLoop(null, -1, eventBus, nvtSupport, gameStrategy, clientStrategy) {
  //
  // @Override
  // AbstractSocket getSocket(String host, int port) throws IOException {
  // return new AbstractSocketImpl(sourceStream, targetStream);
  // }
  //
  // };
  //
  // }
}
