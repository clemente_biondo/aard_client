package it.clem.mud.gedt.socket;

import java.io.IOException;

import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.streams.TargetStream;

public interface AbstractSocket extends AutoCloseable{
  SourceStream getSourceStream()  throws IOException;
  TargetStream getTargetStream()  throws IOException;
  void close() throws IOException;
}
