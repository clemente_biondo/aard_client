package it.clem.mud.gedt.socket.impl;

import java.io.IOException;
import java.net.Socket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.gedt.socket.AbstractSocket;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.streams.TargetStream;
import it.clem.mud.shared.streams.impl.SourceStreamImpl;
import it.clem.mud.shared.streams.impl.TargetStreamImpl;
import it.clem.mud.util.HostWithPort;

public class AbstractSocketSocketBasedImpl implements AbstractSocket {
  private final static Logger log = LoggerFactory.getLogger(AbstractSocketSocketBasedImpl.class);
  private final Socket socket;
  private final static int READ_TIMEOUT = 750;

  public AbstractSocketSocketBasedImpl(HostWithPort server) throws IOException {
    super();
    this.socket = new Socket(server.getHost(), server.getPort());
    this.socket.setSoTimeout(READ_TIMEOUT);
  }
  
  @Override
  public TargetStream getTargetStream() throws IOException {
    return new TargetStreamImpl(socket.getOutputStream());
  }
  
  @Override
  public SourceStream getSourceStream()  throws IOException{
    return new SourceStreamImpl(socket.getInputStream());
  }

  @Override
  public void close() throws IOException {
   try{
     socket.shutdownInput();
     socket.shutdownOutput();
     try {
       //do il tempo al socket di chiudere input es output
      Thread.sleep(500);
    } catch (InterruptedException e) {
      log.error(e.getMessage(),e);
    }
     socket.close();
   } catch(IOException ioe){
     log.warn(ioe.getMessage(),ioe);
   }
  }
  
 }
