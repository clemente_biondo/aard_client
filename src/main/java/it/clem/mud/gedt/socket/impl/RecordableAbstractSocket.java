package it.clem.mud.gedt.socket.impl;

import java.io.File;
import java.io.IOException;

import it.clem.mud.gedt.socket.AbstractSocket;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.streams.TargetStream;
import it.clem.mud.shared.streams.impl.RecordableSourceStream;
import it.clem.mud.shared.streams.impl.RecordableTargetStream;

public class RecordableAbstractSocket implements AbstractSocket {
  private final AbstractSocket as;
  private final File recordingDirectory;

  public RecordableAbstractSocket(AbstractSocket as, File recordingDirectory) {
    super();
    this.as = as;
    this.recordingDirectory = recordingDirectory;
  }

  @Override
  public SourceStream getSourceStream() throws IOException {
    return new RecordableSourceStream(as.getSourceStream(),recordingDirectory);
  }

  @Override
  public TargetStream getTargetStream() throws IOException {
    return new RecordableTargetStream(as.getTargetStream(),recordingDirectory);
  }

  @Override
  public void close() throws IOException {
    as.close();
  }

}
