package it.clem.mud.gedt.socket.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.gedt.socket.AbstractSocket;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.streams.TargetStream;

public class AbstractSocketImpl implements AbstractSocket {
  private final static Logger log = LoggerFactory.getLogger(AbstractSocketImpl.class);
  private final TargetStream targetStream;
  private final SourceStream sourceStream;

  public AbstractSocketImpl(SourceStream sourceStream,TargetStream targetStream) {
    super();
    this.targetStream = targetStream;
    this.sourceStream = sourceStream;
  }

  public TargetStream getTargetStream() {
    return targetStream;
  }

  public SourceStream getSourceStream() {
    return sourceStream;
  }

  @Override
  public void close() throws IOException {
   try{
     targetStream.close();
   } catch(IOException ioe){
     log.warn(ioe.getMessage(),ioe);
   }
   try{
     sourceStream.close();
   } catch(IOException ioe){
     log.warn(ioe.getMessage(),ioe);
   }
    
  }

}
