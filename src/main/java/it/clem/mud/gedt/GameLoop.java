package it.clem.mud.gedt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.gedt.event.bus.GameEventBus;
import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.GameEvent;
import it.clem.mud.shared.game.event.queue.GameEventConsumer;
import it.clem.mud.shared.recording.GameEventThreadDispatcherProducer;

public class GameLoop {
  private final GameEventConsumer eventQueue;
  private final GameContext    gameContext;
  private final GameEventBus geBus;
  private final GameEventThreadDispatcherProducer recordProducer;
  private final Logger         log = LoggerFactory.getLogger(GameLoop.class);

  public GameLoop(GameEventConsumer eventQueue, GameContext gameContext,GameEventBus geBus,GameEventThreadDispatcherProducer recordProducer) {
    super();
    this.eventQueue = eventQueue;
    this.gameContext = gameContext;
    this.geBus=geBus;
    this.recordProducer=recordProducer;
  }

  public void doLoop() {
    try {
      //non ho motivo di interrompere dato che posso inviare una poison pill
      //lo lascio giusto per non mettere un while true e per facilitare i test
      while (!Thread.currentThread().isInterrupted()) {
        final GameEvent evt = eventQueue.take();
        recordProducer.recordGameEvent(evt);
        if(log.isTraceEnabled()){
          log.trace("Evento ricevuto:"+evt);
        }
        geBus.dispatch(evt,gameContext);
        if (evt.endsGameLoop()) {//poison pill
          log.trace("game loop terminato da evento");
          break;
        }
      }
    } catch (Exception e) {
      log.warn("game loop terminato da eccezione:" + e.getMessage(), e);
    }
    if (Thread.currentThread().isInterrupted()) {
      log.trace("game loop terminato a seguito di interruzione ");
    }

  }
}
