package it.clem.mud;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.clem.mud.client.console.ConsoleClient;
// per i colori ansicon
//d: && cd D:\eclipse_oxygen\bitbucket_aard_client_repository && mvn spring-boot:run -Dmaven.test.skip=true
//d: && cd D:\eclipse_oxygen\bitbucket_aard_client_repository && mvn clean package -Dmaven.test.skip=true && cd target && java -jar aardwolf_client-0.0.2.jar 
@SpringBootApplication
public class Startup {
  public static void main(String[] args) {
    //SpringApplication.run(Startup.class, args);
    ConsoleClient.main(args);
    
  }
}
