package it.clem.mud.protocol;

import java.nio.charset.Charset;

import it.clem.mud.util.HostWithPort;

public class AardProtocolResources {
  public final static String SERVER_HOSTNAME = "aardmud.org";
  public final static String SERVER_IP = "23.111.136.202";
  public final static int SERVER_PORT = 23;
  public final static HostWithPort AARD_SERVER = new HostWithPort(SERVER_IP, SERVER_PORT);
  public final static HostWithPort CUBAN_BAR_SERVER = new HostWithPort("52.88.68.92", 1234);
  public final static HostWithPort STAR_TREK_SERVER = new HostWithPort("xmltrek.com", 1701);
  public final static HostWithPort FURRYMUCK_SERVER = new HostWithPort("furrymuck.com", 8888);
  public final static HostWithPort ARMAGEDDON_SERVER = new HostWithPort("armageddon.org", 4050);
  public final static HostWithPort DIMENSIONS_SERVER = new HostWithPort("4dimensions.org", 6000);
  public final static HostWithPort ACHAEA = new HostWithPort("achaea.com");
  public final static String ENCODING = "ISO-8859-1";
  public final static Charset ENCODING_CHARSET = Charset.forName("ISO-8859-1");

  public static byte[] string2Bytes(String s) {
    return s.getBytes(AardProtocolResources.ENCODING_CHARSET);
  }
}
