package it.clem.mud.protocol.data;

import java.io.Serializable;

public interface TelnetData extends Serializable {

  byte[] getBytes();

  boolean sameBytes(byte[] bytes);

  boolean sameOf(TelnetData data);

  TelnetDataType getType();

  boolean isUnknown();

  boolean itDenotesConnectionClosedByServer();

}
