package it.clem.mud.protocol.data.handlers.impl;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetText;

public class NotifyTextFromAardwolf extends TelnetDataHandlerByTypeBaseImpl<TelnetText> {
  private final StringBuilder sb        = new StringBuilder();
  private boolean             insideMap = false;

  public NotifyTextFromAardwolf() {
    super(TelnetDataType.TEXT);
  }

  @Override
  public void handle(TelnetText data, AardTelnetContext context) {
    if (data.isMapStart()) {
      insideMap = true;
    } else if (data.isMapEnd()) {
      context.fireMapTagUpdated(sb.toString());
      sb.setLength(0);
      insideMap = false;
    } else if (insideMap) {
      sb.append(data.getContent());
    } else {
      context.fireTextSentFromServerEvent(data.getContent());
    }

  }

}
