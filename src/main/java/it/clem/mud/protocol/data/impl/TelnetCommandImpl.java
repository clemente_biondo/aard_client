package it.clem.mud.protocol.data.impl;

import it.clem.mud.protocol.TelnetProtocolResources;
import it.clem.mud.protocol.data.TelnetCommand;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.util.LangUtils;

public class TelnetCommandImpl extends TelnetSequencedDataImpl implements TelnetCommand {
  /**
   * 
   */
  private static final long serialVersionUID = -4680008501761917344L;
  private final int code;
  private final int sequenceId;
  private final byte[] byteArr;

  public TelnetCommandImpl(int sequenceId, TelnetDataType type, int code) {
    super(sequenceId,type);
    this.code = code;
    this.sequenceId = sequenceId;
    this.byteArr=LangUtils.arrOfIntToArrOfByte(TelnetProtocolResources.IAC, code);
  }

  @Override
  public int getCode() {
    return code;
  }

  @Override
  public byte[] getBytes() {
    return this.byteArr;
  }

  @Override
  public String toString() {
    return getSeqId() + ":<IAC " + TelnetProtocolResources.CODES[code] + ">";
  }

  @Override
  public int getSeqId() {
    return sequenceId;
  }
}
