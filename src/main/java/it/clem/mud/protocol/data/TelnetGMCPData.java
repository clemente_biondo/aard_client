package it.clem.mud.protocol.data;

import java.util.List;


import com.google.common.collect.ImmutableList;

import it.clem.mud.protocol.TelnetProtocolResources;

public class TelnetGMCPData extends TelnetSubnegotiation{
  public final static String SERVER_TICK_MESSAGE="comm.tick {}";
  
  public final static String SERVER_CHAR_prefix="char.";
  public final static String SERVER_CHAR_BASE_START="base";
  public final static String SERVER_CHAR_MAXSTATS_START="maxstats" ;
  public final static String SERVER_CHAR_STATS_START   ="stats"    ;
  public final static String SERVER_CHAR_STATUS_START  ="status"   ;
  public final static String SERVER_CHAR_VITALS_START  ="vitals"   ;
  public final static String SERVER_CHAR_WORTH_START   ="worth"    ;

  public final static String SERVER_COMM_PREFIX ="comm."  ;
  public final static String SERVER_COMM_CHANNEL_START ="channel"  ;
  public final static String SERVER_COMM_QUEST_START   ="quest"    ;
  public final static String SERVER_COMM_REPOP_START   ="repop"    ;
  
  public final static String SERVER_ROOM_PREFIX ="room."  ;
  public final static String SERVER_ROOM_AREA_START    ="area"     ;
  public final static String SERVER_ROOM_INFO_START    ="info"     ;
  public final static String SERVER_ROOM_SECTORS_START ="sectors"  ;
  public final static String SERVER_ROOM_WRONGDIR_START="wrongdir" ;
  public final static int MAX_START_LEN=13 ;
  public final static int MIN_START_LEN=9 ;
  
  private static final long serialVersionUID = -8722571307142988262L;
  public final static String CLIENT_HANDSHAKE="Core.Hello { \"client\": \"MUSHclient\", \"version\": \"4.98 r1825\" }";
  public final static String CLIENT_SUPPORT="Core.Supports.Set [ \"Char 1\", \"Comm 1\", \"Room 1\", \"Group 1\" ]";
  //public final static String CLIENT_LOGIN="Char.Login { \"name\": \"heliar\", \"password\": \"arancia1975\" }";
  public final static String RAWCOLOR_ON="rawcolor on";
  public final static String GROUP_ON="group on";
  public final static String MAPTYPE_UTF8_ON="maptype 6";
  
  public final static String CLIENT_REQUEST_AREA="request area";
  public final static String CLIENT_REQUEST_CHAR="request char";
  public final static String CLIENT_REQUEST_ROOM="request room";
  public final static String CLIENT_REQUEST_QUEST="request quest";
  public final static String CLIENT_REQUEST_SECTORS="request sectors";
  
  public final static List<String> HANDSHAKE_SEQUENCE = ImmutableList.of(
      CLIENT_HANDSHAKE        
      ,CLIENT_SUPPORT          
    //  ,CLIENT_LOGIN            
      ,RAWCOLOR_ON             
      ,GROUP_ON                
      ,MAPTYPE_UTF8_ON         
      ,CLIENT_REQUEST_AREA     
      ,CLIENT_REQUEST_CHAR     
      ,CLIENT_REQUEST_ROOM     
      ,CLIENT_REQUEST_QUEST    
      ,CLIENT_REQUEST_SECTORS  
      );
  public final static List<String> REQUEST_ALL = ImmutableList.of(
       CLIENT_REQUEST_AREA     
      ,CLIENT_REQUEST_CHAR     
      ,CLIENT_REQUEST_ROOM     
      ,CLIENT_REQUEST_QUEST    
      ,CLIENT_REQUEST_SECTORS        
      );
      
  public TelnetGMCPData(int sequenceId,  String content) {
    this(sequenceId,  content.getBytes());
  }

  public TelnetGMCPData(int sequenceId,  byte... content) {
    super(sequenceId, TelnetProtocolResources.GMCP, content);
  }

  @Override
  public String toString() {
    return  "<GMCP " +getContentAsString()+ " >";
  }
  
  
}

