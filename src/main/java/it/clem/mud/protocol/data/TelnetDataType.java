package it.clem.mud.protocol.data;

public enum TelnetDataType {
   //@formatter:off
   CONNECTION_CLOSED_BY_SERVER (false,true),
   TEXT                        (false,false),
   SE_CMD                      (false,false),
   NOP_CMD                     (false,false),
   DM_CMD                      (false,false),
   BRK_CMD                     (false,false),
   IP_CMD                      (false,false),
   AO_CMD                      (false,false),
   AYT_CMD                     (false,false),
   EC_CMD                      (false,false),
   EL_CMD                      (false,false),
   GA_CMD                      (false,false),
   SB_CMD                      (false,false),
   WILL_CMP2                   (false,false),
   WILL_CMP1                   (false,false),
   WILL_AARD                   (false,false),
   WILL_ATCP                   (false,false),
   WILL_GMCP                   (false,false),
   WILL_ECHO                   (false,false),
   DO_ECHO                     (false,false),
   DO_CMP2                     (false,false),
   DO_TERMINAL_TYPE            (false,false),
   DO_NAWS                     (false,false),
   WONT_CMP2                   (false,false),
   WONT_TTY                    (false,false),
   WONT_NAWS                   (false,false),
   WILL_TERMINAL_TYPE          (false,false),
   WILL_NAWS                   (false,false),
   DONT_CMP2                   (false,false),
   DONT_CMP1                   (false,false),
   DONT_AARD                   (false,false),
   DONT_ATCP                   (false,false),
   DONT_GMCP                   (false,false),
   DO_CMP1                     (false,false),
   DO_AARD                     (false,false),
   DO_ATCP                     (false,false),
   DO_GMCP                     (false,false),
   START_COMPRESSION           (false,false),
   TERMINAL_TYPE_SEND          (false,false),
   TERMINAL_TYPE_IS            (false,false),
//Aggiungere aard e gmcp   
   AARD_SUBNEG                 (false,false),
   GMCP_SUBNEG                 (false,false),
   UNKNOWN_SUBNEGOTIATION      (true,false),
   UNKNOWN_WILL                (true,false),
   UNKNOWN_DO                  (true,false),
   UNKNOWN_DONT                (true,false),
   UNKNOWN_WONT                (true,false);
  //@formatter:on  
  private final boolean unknown;
  private final boolean connectionClosed;

  private TelnetDataType(boolean unknown,boolean connectionClosed) {
    this.unknown = unknown;
    this.connectionClosed=connectionClosed;
  }

  public boolean isUnknown() {
    return unknown;
  }

  public boolean itDenotesConnectionClosedByServer() {
    return connectionClosed;
  }
  
}
