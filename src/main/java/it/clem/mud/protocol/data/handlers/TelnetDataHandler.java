package it.clem.mud.protocol.data.handlers;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetData;

/**
 * 
 * Evade la richiesta telnet ricevuta, agendo sul TelnetContext. 
 * E' un Consumer, quindi unlike most other functional interfaces, {@code Consumer} is expected
 * to operate via side-effects. TelnetData
 * @param <T>
 */
@FunctionalInterface
public interface TelnetDataHandler<T extends TelnetData> {
  void handle(T data,AardTelnetContext context);
}
