package it.clem.mud.protocol.data;

public interface TelnetCommand extends TelnetSequencedData{
  int getCode();
  

}
