package it.clem.mud.protocol.data;

import java.io.UncheckedIOException;

import it.clem.mud.shared.game.event.queue.ServerEventProducer;

public interface AardTelnetContext extends ServerEventProducer, AbleToIgnoreTelnetData{
  boolean isServerOptionActive(int option);
  boolean isClientOptionActive(int option);
  void sendGMCPCommand(String todo) throws UncheckedIOException;
  void sendTerminalType() throws UncheckedIOException;
  void replyWithConfirmationOf(TelnetCommandWithOption data) throws UncheckedIOException;
  void replyWithRefusalOf(TelnetPositiveCommandWithOption data) throws UncheckedIOException;
  void startCompression();
  void stopCompression();
  void signalAsNotSupported(TelnetData data);
  void sendGMCPHandshake();
  

}
