package it.clem.mud.protocol.data;

import it.clem.mud.protocol.data.impl.TelnetDataImpl;

public final class TelnetConnectionClosedByServer extends TelnetDataImpl {
 
  /**
   * 
   */
  private static final long serialVersionUID = 4289879306272307179L;

  private TelnetConnectionClosedByServer() {
    super(TelnetDataType.CONNECTION_CLOSED_BY_SERVER);
  }

  private static final TelnetConnectionClosedByServer INSTANCE = new TelnetConnectionClosedByServer();

  public static TelnetConnectionClosedByServer getInstance() {
    return INSTANCE;
  }

  @Override
  public byte[] getBytes() {
    return null;
  }

  @Override
  public boolean sameBytes(byte[] bytes) {
    return bytes == null || bytes.length == 0;
  }

  @Override
  public TelnetDataType getType() {
    return TelnetDataType.CONNECTION_CLOSED_BY_SERVER;
  }

  @Override
  public String toString() {
    return "<Connection Closed By Server>";
  }

}
