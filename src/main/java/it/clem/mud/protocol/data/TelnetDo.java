package it.clem.mud.protocol.data;

import it.clem.mud.protocol.TelnetProtocolResources;
import it.clem.mud.protocol.data.impl.TelnetPositiveCommandWithOptionImpl;

public class TelnetDo extends TelnetPositiveCommandWithOptionImpl {

	/**
   * 
   */
  private static final long serialVersionUID = 4500774851596517616L;

  public TelnetDo(Integer sequenceIdOfSource,int sequenceId,TelnetDataType type,int option) { 
		super(sequenceIdOfSource,sequenceId,type,TelnetProtocolResources.DO, option);
	}

}
