package it.clem.mud.protocol.data.handlers;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import it.clem.mud.protocol.data.TelnetCommandWithOption;
import it.clem.mud.protocol.AardServerThread;
import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetPositiveCommandWithOption;
import it.clem.mud.protocol.data.impl.TelnetPositiveCommandWithOptionImpl;

public class TelnetDataHandlerBus implements TelnetDataHandler<TelnetData> {
  private final Map<TelnetDataType, TelnetDataHandler<? extends TelnetData>> handlers;
  private final static Logger log = LoggerFactory.getLogger(TelnetDataHandlerBus.class); 
  public TelnetDataHandlerBus(Map<TelnetDataType, TelnetDataHandler<? extends TelnetData>> handlers) {
    if (handlers.size() != TelnetDataType.values().length) {
      Set<TelnetDataType> s = Sets.newHashSet(TelnetDataType.values());
      s.removeAll(handlers.keySet());
      String missing="Mancano:" + s.stream().map(TelnetDataType::name).collect(Collectors.joining(","));
      log.error(missing);
      throw new IllegalArgumentException(missing);
    }

    this.handlers = ImmutableMap.copyOf(handlers);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void handle(TelnetData data, AardTelnetContext context) {
    assert Thread.currentThread().getName().equals(AardServerThread.THREAD_NAME) : Thread.currentThread().getName();
    TelnetDataType tdt=data.getType();
    if(tdt.isUnknown()){
      log.warn("Comando sconosciuto:"+data);
    }
    TelnetDataHandler<? extends TelnetData> td = handlers.get(tdt);
    ((TelnetDataHandler<TelnetData>) td).handle(data, context);
  }

  public static class Builder {
    private final Map<TelnetDataType, TelnetDataHandler<? extends TelnetData>> handlers = new TreeMap<>();

    @SafeVarargs
    public final Builder register(TelnetDataHandlerByType<? extends TelnetData>... arrTd) {
      for (TelnetDataHandlerByType<?> td : arrTd) {
        register(td);
      }
      return this;
    }

    public <T extends TelnetData> Builder register(TelnetDataHandlerByType<T> td) {
      for (TelnetDataType tdt : td.handles()) {
        register(tdt, td);
      }
      return this;
    }

    public <T extends TelnetData> Builder register(TelnetDataType tdt, TelnetDataHandler<T> tdh) {
      if (handlers.containsKey(tdt)) {
        throw new IllegalArgumentException(tdt.name());
      }
      handlers.put(tdt, tdh);
      return this;
    }

    public TelnetDataHandlerBus build() {
      return new TelnetDataHandlerBus(handlers);
    }

    public Builder confirm(TelnetCommandWithOption... cmds) {
      for (TelnetCommandWithOption cmd : cmds) {
        register(cmd.getType(), (td, tc) -> {
          tc.replyWithConfirmationOf(cmd);
        });
      }
      return this;
    }

    public Builder confirm(TelnetDataType... types) {
      for (TelnetDataType tdt : types) {
        register(tdt, (td, tc) -> {
          tc.replyWithConfirmationOf((TelnetCommandWithOption) td);
        });
      }
      return this;
    }

    public Builder refuse(TelnetDataType... types) {
      for (TelnetDataType tdt : types) {
        register(tdt, (td, tc) -> {
          tc.replyWithRefusalOf((TelnetPositiveCommandWithOption) td);
        });
      }
      return this;
    }

    public Builder refuse(TelnetPositiveCommandWithOptionImpl... cmds) {
      for (TelnetPositiveCommandWithOptionImpl cmd : cmds) {
        register(cmd.getType(), (td, tc) -> {
          tc.replyWithRefusalOf(cmd);
        });
      }
      return this;
    }

    public Builder registerAsNotSupported(TelnetDataType... types) {
      for (TelnetDataType tdt : types) {
        register(tdt, (td, tc) -> {
          tc.signalAsNotSupported(td);
        });
      }
      return this;
    }

    public Builder ignore(TelnetDataType... types) {
      for (TelnetDataType tdt : types) {
        register(tdt, (td, tc) -> {
          tc.ignore(td);
        });
      }
      return this;
    }

  }

  public static Builder builder() {
    return new Builder();
  }

  public static TelnetDataHandlerBus getClient(NVTSupport client) {
    switch (client) {
    case Minimal:
      return MinimalTelnetClient.getMinimalClient();
    case Aard:
      return AardTelnetClient.getAardClient();
    default:
      throw new IllegalArgumentException();
    }
  }

  /**
   * Restituisce un client telnet stateless minimale.
   * @return
   */
  public static TelnetDataHandlerBus getMinimalClient() {
    return getClient(NVTSupport.Minimal);
  }
  
  public static TelnetDataHandlerBus getAardClient() {
    return getClient(NVTSupport.Aard);
  }

}
