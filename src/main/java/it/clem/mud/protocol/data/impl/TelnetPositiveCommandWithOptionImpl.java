package it.clem.mud.protocol.data.impl;

import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetPositiveCommandWithOption;

public class TelnetPositiveCommandWithOptionImpl extends TelnetCommandWithOptionImpl implements TelnetPositiveCommandWithOption{

	/**
   * 
   */
  private static final long serialVersionUID = 1552906395971677837L;

  public TelnetPositiveCommandWithOptionImpl(Integer sequenceIdOfSource,int sequenceId,TelnetDataType type,int code, int option) {
		super(sequenceIdOfSource,sequenceId,type,code, option);
	}

  @Override
  public boolean isANegativeReply() {
    return false;
  }
	
}