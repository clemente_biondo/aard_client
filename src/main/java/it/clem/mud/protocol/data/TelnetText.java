package it.clem.mud.protocol.data;

import it.clem.mud.protocol.AardProtocolResources;
import it.clem.mud.protocol.data.impl.TelnetSequencedDataImpl;

public class TelnetText extends TelnetSequencedDataImpl {
  /**
   * 
   */
  private static final long  serialVersionUID = -7691384346378512254L;
  private final String       text;
  public final static String MAP_START_TAG    = "<MAPSTART>\n\r";
  public final static String MAP_END_TAG      = "<MAPEND>\n\r";

  public TelnetText(int sequenceId, String text) {
    super(sequenceId, TelnetDataType.TEXT);
    this.text = text;
  }

  @Override
  public String toString() {
    return text;
  }

  @Override
  public byte[] getBytes() {
    return AardProtocolResources.string2Bytes(text);
  }

  public String getContent() {
    return text;
  }

  public boolean isMapStart() {
    return MAP_START_TAG.equals(text);
  }

  public boolean isMapEnd() {
    return MAP_END_TAG.equals(text);
  }

}
