package it.clem.mud.protocol.data.handlers.impl;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetSubnegotiation;

public class HandleCompression extends TelnetDataHandlerByTypeBaseImpl<TelnetSubnegotiation> {

  public HandleCompression() {
    super(TelnetDataType.START_COMPRESSION);
  }

  @Override
  public void handle(TelnetSubnegotiation data, AardTelnetContext context) {
      context.startCompression();
  }

}
