package it.clem.mud.protocol.data;

public interface TelnetSequencedData extends TelnetData {
  int getSeqId();
}
