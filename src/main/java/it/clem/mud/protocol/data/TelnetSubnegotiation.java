package it.clem.mud.protocol.data;

import org.apache.commons.codec.binary.Hex;

import it.clem.mud.protocol.AardProtocolResources;
import it.clem.mud.protocol.TelnetProtocolResources;

import static it.clem.mud.protocol.TelnetProtocolResources.*;

import it.clem.mud.protocol.data.impl.TelnetSequencedDataImpl;
import it.clem.mud.util.LangUtils;

//E' un macrocomando...
public class TelnetSubnegotiation extends TelnetSequencedDataImpl implements TelnetSequencedData {
  /**
   * 
   */
  private static final long serialVersionUID = -8785420075015215545L;
  private final int    option;
  private final byte[] content;
  private final byte[] byteArr;

  public TelnetSubnegotiation(int sequenceId, int option, int... content) {
    this(sequenceId,option, LangUtils.arrOfIntToArrOfByte(content));
  }

  public TelnetSubnegotiation(int sequenceId, int option, byte... content) {
    super(sequenceId,determineType(option, content));
    this.content = content;
    this.option = option;
    this.byteArr = LangUtils.concat(TelnetProtocolResources.IAC_SB, new byte[] { (byte) option }, content,
        TelnetProtocolResources.IAC_SE);
  }

  private static TelnetDataType determineType(int option, byte... content) {
    if (option == CMP2 && content.length == 0) {
      return TelnetDataType.START_COMPRESSION;
    }
    if (option == TERMINAL_TYPE) {
      if (content.length == 1 && content[0] == TERMINAL_TYPE_SEND) {
        return TelnetDataType.TERMINAL_TYPE_SEND;
      }
      if (content.length > 1 && content[0] == TERMINAL_TYPE_IS) {
        return TelnetDataType.TERMINAL_TYPE_IS;
      }
    }
    if (option==AARD){
      return TelnetDataType.AARD_SUBNEG;
    }
    if (option==GMCP){
      return TelnetDataType.GMCP_SUBNEG;
    }

    return TelnetDataType.UNKNOWN_SUBNEGOTIATION;
  }

  public byte[] getContent() {
    return content;
  }

  public String getContentAsString() {
    return new String(content, AardProtocolResources.ENCODING_CHARSET);
  }

  public int getOption() {
    return option;
  }

  @Override
  public byte[] getBytes() {
    return byteArr;
  }

  @Override
  public String toString() {
    return getSeqId() + ":<IAC SB " + OPTIONS[option] + " " + String.valueOf(Hex.encodeHex(content))
        + " IAC SE>";
  }

}
