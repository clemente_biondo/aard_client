package it.clem.mud.protocol.data;

import it.clem.mud.protocol.TelnetProtocolResources;
import it.clem.mud.protocol.data.impl.TelnetCommandWithOptionImpl;

public class TelnetDont extends TelnetCommandWithOptionImpl {

	/**
   * 
   */
  private static final long serialVersionUID = 348214550881584462L;

  public TelnetDont(Integer sequenceIdOfSource,int sequenceId,TelnetDataType type,int option) {
		super(sequenceIdOfSource,sequenceId,type,TelnetProtocolResources.DONT, option);
	}
}