package it.clem.mud.protocol.data;

import it.clem.mud.protocol.TelnetProtocolResources;
import it.clem.mud.protocol.data.impl.TelnetCommandImpl;

public class TelnetGA extends TelnetCommandImpl{

  /**
   * 
   */
  private static final long serialVersionUID = -7447135439326976279L;

  public TelnetGA(int sequenceId) {
    super(sequenceId, TelnetDataType.GA_CMD, TelnetProtocolResources.GA);
  }

}
