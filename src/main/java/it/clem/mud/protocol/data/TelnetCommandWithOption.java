package it.clem.mud.protocol.data;

import java.util.Optional;

public interface TelnetCommandWithOption extends TelnetCommand{
  int getOption();
  Optional<Integer> getSeqIdSource();
  boolean isAReply();
  boolean isANegativeReply();
}
