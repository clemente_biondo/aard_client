package it.clem.mud.protocol.data.handlers.impl;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetGMCPData;
import it.clem.mud.protocol.data.TelnetSubnegotiation;

public class HandleGMCPSentFromServer extends TelnetDataHandlerByTypeBaseImpl<TelnetGMCPData> {

  public HandleGMCPSentFromServer() {
    super(TelnetDataType.GMCP_SUBNEG);
  }

  @Override
  public void handle(TelnetGMCPData data, AardTelnetContext context) {
    String content = data.getContentAsString();
    if (TelnetGMCPData.SERVER_TICK_MESSAGE.equals(content)) {
      context.fireGMCPTick();
    } else if (parseCommand(content)) {
      context.ignore(data);
    }
  }

  private boolean parseCommand(String content) {
    int spacePos = content.indexOf("");
    if (spacePos < TelnetGMCPData.MIN_START_LEN || spacePos > TelnetGMCPData.MAX_START_LEN
        || spacePos == content.length()) {
      return false;
    }
    String prefix = content.substring(0, 5);
    String command = content.substring(5, spacePos);
    String payload = content.substring(spacePos);
    switch (prefix) {
    case TelnetGMCPData.SERVER_CHAR_prefix:
      switch (payload) {
      case TelnetGMCPData.SERVER_CHAR_BASE_START:
        return parseCharBase(payload);
      case TelnetGMCPData.SERVER_CHAR_MAXSTATS_START:
        return parseMaxStats(payload);
      case TelnetGMCPData.SERVER_CHAR_STATS_START:
        return parseStats(payload);
      case TelnetGMCPData.SERVER_CHAR_STATUS_START:
        return parseMaxStatus(payload);
      case TelnetGMCPData.SERVER_CHAR_VITALS_START:
        return parseVitals(payload);
      case TelnetGMCPData.SERVER_CHAR_WORTH_START:
        return parseWorth(payload);
      default:
        return false;

      }
    case TelnetGMCPData.SERVER_COMM_PREFIX:
      switch (payload) {
      case TelnetGMCPData.SERVER_COMM_CHANNEL_START:
        return parseChannel(payload);
      case TelnetGMCPData.SERVER_COMM_QUEST_START:
        return parseQuest(payload);
      case TelnetGMCPData.SERVER_COMM_REPOP_START:
        return parseRepop(payload);
      default:
        return false;

      }
    case TelnetGMCPData.SERVER_ROOM_PREFIX:
      switch (payload) {
      case TelnetGMCPData.SERVER_ROOM_AREA_START:
        return parseArea(payload);
      case TelnetGMCPData.SERVER_ROOM_INFO_START:
        return parseInfo(payload);
      case TelnetGMCPData.SERVER_ROOM_SECTORS_START:
        return parseSectors(payload);
      case TelnetGMCPData.SERVER_ROOM_WRONGDIR_START:
        return parseWrongdir(payload);
      default:
        return false;

      }
    default:
      return false;
    }

  }

  private boolean parseWrongdir(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseSectors(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseInfo(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseArea(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseRepop(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseQuest(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseChannel(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseCharBase(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseMaxStats(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseStats(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseMaxStatus(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseVitals(String payload) {
    // TODO Auto-generated method stub
    return false;
  }

  private boolean parseWorth(String payload) {
    // TODO Auto-generated method stub
    return false;
  }
}
