package it.clem.mud.protocol.data;

import it.clem.mud.protocol.TelnetProtocolResources;
import it.clem.mud.protocol.data.impl.TelnetPositiveCommandWithOptionImpl;

public class TelnetWill extends TelnetPositiveCommandWithOptionImpl {

	/**
   * 
   */
  private static final long serialVersionUID = 6456741003546110552L;

  public TelnetWill(Integer sequenceIdOfSource,int sequenceId,TelnetDataType type,int option) { 
		super(sequenceIdOfSource,sequenceId,type,TelnetProtocolResources.WILL, option);
	}
	


}