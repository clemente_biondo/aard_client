package it.clem.mud.protocol.data.handlers;

import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.handlers.impl.HandleConnectionClosedByServer;
import it.clem.mud.protocol.data.handlers.impl.NotifyTextFromServer;

public class MinimalTelnetClient {
  
  private final static TelnetDataHandlerBus INSTANCE =
    //@formatter:off
    TelnetDataHandlerBus.builder()
    .register(
       new HandleConnectionClosedByServer(),
       new NotifyTextFromServer()
     ).confirm(
       TelnetDataType.WONT_CMP2                   ,
       TelnetDataType.WONT_TTY                    ,
       TelnetDataType.WONT_NAWS                   ,
       TelnetDataType.DONT_CMP2                   ,
       TelnetDataType.DONT_CMP1                   ,
       TelnetDataType.DONT_AARD                   ,
       TelnetDataType.DONT_ATCP                   ,
       TelnetDataType.DONT_GMCP                   ,        
       TelnetDataType.UNKNOWN_WONT                ,
       TelnetDataType.WILL_ECHO                   ,
       TelnetDataType.UNKNOWN_DONT
     ).refuse(
         TelnetDataType.WILL_TERMINAL_TYPE                    ,
         TelnetDataType.WILL_NAWS                   ,
         TelnetDataType.WILL_CMP2                   ,
         TelnetDataType.WILL_CMP1                   ,
         TelnetDataType.WILL_AARD                   ,
         TelnetDataType.WILL_ATCP                   ,
         TelnetDataType.WILL_GMCP                   ,
         TelnetDataType.DO_CMP2                     ,
         TelnetDataType.DO_TERMINAL_TYPE                      ,
         TelnetDataType.DO_NAWS                     ,
         TelnetDataType.DO_CMP1                     ,
         TelnetDataType.DO_AARD                     ,
         TelnetDataType.DO_ATCP                     ,
         TelnetDataType.DO_GMCP                     ,
         TelnetDataType.DO_ECHO                     ,
         TelnetDataType.UNKNOWN_DO                  ,
         TelnetDataType.UNKNOWN_WILL
     ).ignore(
         TelnetDataType.START_COMPRESSION           ,
         TelnetDataType.TERMINAL_TYPE_SEND          ,
         TelnetDataType.TERMINAL_TYPE_IS            ,
         TelnetDataType.AARD_SUBNEG                 ,      
         TelnetDataType.GMCP_SUBNEG                 ,
         TelnetDataType.GA_CMD                      ,
         TelnetDataType.UNKNOWN_SUBNEGOTIATION      
     ).registerAsNotSupported(
         TelnetDataType.SE_CMD                      ,
         TelnetDataType.NOP_CMD                     ,
         TelnetDataType.DM_CMD                      ,
         TelnetDataType.BRK_CMD                     ,
         TelnetDataType.IP_CMD                      ,
         TelnetDataType.AO_CMD                      ,
         TelnetDataType.AYT_CMD                     ,
         TelnetDataType.EC_CMD                      ,
         TelnetDataType.EL_CMD                      ,
         TelnetDataType.SB_CMD                      
         ).build();
    //@formatter:on

  public static TelnetDataHandlerBus getMinimalClient() {
    return INSTANCE;
  }

  

}
