package it.clem.mud.protocol.data.handlers.impl;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.handlers.TelnetDataHandlerByType;

public abstract class TelnetDataHandlerByTypeBaseImpl<T extends TelnetData> implements TelnetDataHandlerByType<T> {
  private final TelnetDataType[] types;

  public TelnetDataHandlerByTypeBaseImpl(TelnetDataType[] types) {
    super();
    this.types = types;
  }

  public TelnetDataHandlerByTypeBaseImpl(TelnetDataType type) {
    this(new TelnetDataType[] { type });
  }

  @Override
  public TelnetDataType[] handles() {
    return types;
  }
}
