package it.clem.mud.protocol.data.handlers.impl;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetSubnegotiation;

public class SendTerminalType extends TelnetDataHandlerByTypeBaseImpl<TelnetSubnegotiation>{

  public SendTerminalType() {
    super(TelnetDataType.TERMINAL_TYPE_SEND);
  }

  @Override
  public void handle(TelnetSubnegotiation data, AardTelnetContext context) {
    context.sendTerminalType();
  }

  
}
