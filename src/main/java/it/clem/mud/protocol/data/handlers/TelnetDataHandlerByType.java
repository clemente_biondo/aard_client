package it.clem.mud.protocol.data.handlers;


import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetDataType;

public interface TelnetDataHandlerByType<T extends TelnetData> extends TelnetDataHandler<T>{
  TelnetDataType[] handles();
}
