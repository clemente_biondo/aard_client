package it.clem.mud.protocol.data.handlers.impl;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetCommandWithOption;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetWill;

public class HandleServerWillGMCP extends TelnetDataHandlerByTypeBaseImpl<TelnetWill>{

  public HandleServerWillGMCP() {
    super(TelnetDataType.WILL_GMCP);
  }

  @Override
  public void handle(TelnetWill data, AardTelnetContext context) {
    context.replyWithConfirmationOf((TelnetCommandWithOption) data);
    context.sendGMCPHandshake();
    
  }

}
