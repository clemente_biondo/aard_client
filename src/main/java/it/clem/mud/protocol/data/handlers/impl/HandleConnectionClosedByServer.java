package it.clem.mud.protocol.data.handlers.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.protocol.data.TelnetConnectionClosedByServer;
import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetDataType;

/**
 * Notifica al game loop l'end of stream del receiver e termina il server loop.
 * @author clem
 *
 */
public class HandleConnectionClosedByServer extends TelnetDataHandlerByTypeBaseImpl<TelnetConnectionClosedByServer>{
  @SuppressWarnings("unused")
  private final static Logger log = LoggerFactory.getLogger(HandleConnectionClosedByServer.class);
  public HandleConnectionClosedByServer() {
    super(TelnetDataType.CONNECTION_CLOSED_BY_SERVER);
  }

  @Override
  public void handle(TelnetConnectionClosedByServer data, AardTelnetContext context) {
    context.fireConnectionClosedByServerEvent();
  }

}
