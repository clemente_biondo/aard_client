package it.clem.mud.protocol.data.impl;

import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetSequencedData;

public abstract class TelnetSequencedDataImpl extends TelnetDataImpl implements TelnetSequencedData {
  /**
   * 
   */
  private static final long serialVersionUID = 6501417494574723673L;
  private final int sequenceId;

  public TelnetSequencedDataImpl(int sequenceId, TelnetDataType type) {
    super(type);
    this.sequenceId = sequenceId;
  }

  @Override
  public int getSeqId() {
    return sequenceId;
  }

}
