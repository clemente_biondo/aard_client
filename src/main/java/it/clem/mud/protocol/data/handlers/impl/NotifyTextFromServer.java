package it.clem.mud.protocol.data.handlers.impl;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetText;

public class NotifyTextFromServer extends TelnetDataHandlerByTypeBaseImpl<TelnetText>{

  public NotifyTextFromServer() {
    super(TelnetDataType.TEXT);
  }

  @Override
  public void handle(TelnetText data, AardTelnetContext context) {
    context.fireTextSentFromServerEvent(data.getContent());
  }

  
}
