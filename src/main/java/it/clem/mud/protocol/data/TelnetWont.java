package it.clem.mud.protocol.data;

import it.clem.mud.protocol.TelnetProtocolResources;
import it.clem.mud.protocol.data.impl.TelnetCommandWithOptionImpl;

public class TelnetWont extends TelnetCommandWithOptionImpl {

	/**
   * 
   */
  private static final long serialVersionUID = -6519707923635799833L;

  public TelnetWont(Integer sequenceIdOfSource,int sequenceId,TelnetDataType type,int option) { 
		super(sequenceIdOfSource,sequenceId,type,TelnetProtocolResources.WONT, option);
	}
}