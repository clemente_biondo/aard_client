package it.clem.mud.protocol.data.impl;

import java.util.Arrays;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetDataType;

public abstract class TelnetDataImpl implements TelnetData {
  /**
   * 
   */
  private static final long serialVersionUID = 5390935992229892297L;
  private final TelnetDataType type;
  
  public TelnetDataImpl(TelnetDataType type) {
    super();
    this.type = type;
  }

  @Override
  public TelnetDataType getType() {
    return type;
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(this.getBytes());
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof TelnetData))
      return false;
    return sameOf((TelnetData) obj);
  }

  @Override
  public boolean sameBytes(byte[] bytes) {
    return Arrays.equals(this.getBytes(), bytes);
  }

  @Override
  public boolean sameOf(TelnetData data) {
    return this.sameBytes(data.getBytes());
  }

  @Override
  public boolean isUnknown() {
    return type.isUnknown();
  }

  @Override
  public boolean itDenotesConnectionClosedByServer() {
    return type.itDenotesConnectionClosedByServer();
  }

}
