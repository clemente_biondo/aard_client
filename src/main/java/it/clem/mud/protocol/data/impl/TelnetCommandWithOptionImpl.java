package it.clem.mud.protocol.data.impl;

import java.util.Optional;

import it.clem.mud.protocol.TelnetProtocolResources;
import it.clem.mud.protocol.data.TelnetCommandWithOption;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.util.LangUtils;

public class TelnetCommandWithOptionImpl extends TelnetSequencedDataImpl implements TelnetCommandWithOption {
	/**
   * 
   */
  private static final long serialVersionUID = 8006508630770614683L;
  private final int code;
	private final int option;
	private final byte[] byteArr;
	private final Integer sequenceIdOfSource;

	public TelnetCommandWithOptionImpl(Integer sequenceIdOfSource,int sequenceId,TelnetDataType type,int code, int option) {
		super(sequenceId,type);
		this.code = code;
		this.option = option;
		this.byteArr = LangUtils.arrOfIntToArrOfByte(TelnetProtocolResources.IAC, code, option);
		this.sequenceIdOfSource=sequenceIdOfSource;
	}

	@Override
	public byte[] getBytes() {
		return byteArr;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public int getOption() {
		return option;
	}

  @Override
  public String toString() {
    return getSeqId() + ":<IAC " + TelnetProtocolResources.CODES[code]+" "+TelnetProtocolResources.OPTIONS[option]+">";
  }

  @Override
  public Optional<Integer> getSeqIdSource() {
    return Optional.ofNullable(sequenceIdOfSource);
  }
  
 
  @Override
  public boolean isAReply() {
    return sequenceIdOfSource!= null;
  }

  @Override
  public boolean isANegativeReply() {
    return isAReply();
  }

  
	
}
