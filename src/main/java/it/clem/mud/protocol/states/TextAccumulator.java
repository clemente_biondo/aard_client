package it.clem.mud.protocol.states;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import it.clem.mud.protocol.AardProtocolResources;
import it.clem.mud.protocol.data.TelnetText;
import it.clem.mud.shared.telnet.TelnetDataFactory;

public class TextAccumulator {
  private final TelnetDataFactory factory;
  private final ByteArrayOutputStream out = new ByteArrayOutputStream();

  public TextAccumulator(TelnetDataFactory factory) {
    super();
    this.factory = factory;
  }

  public boolean isEmpty() {
    return out.size()==0;
  }

  public void accumulate(int b) {
    if (b<0)return;
    out.write(b);
  }
  
  public byte[] popBytes()  {
    byte[] res= out.toByteArray();
    out.reset();
    return res;
  }
  
  private String popText()  {
    String res;
    try {
      res = out.toString(AardProtocolResources.ENCODING);
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e.getMessage(),e);
    }
    //res = out.toString();
    out.reset();
    return res;
  }
  
  public TelnetText popAsTelnetText() {
    if (isEmpty()){
      return null;
    }
    return factory.getTelnetText(popText());
  }
  
}
