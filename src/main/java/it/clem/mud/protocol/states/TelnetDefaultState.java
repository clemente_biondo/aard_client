package it.clem.mud.protocol.states;

import static it.clem.mud.protocol.TelnetProtocolResources.*;


public class TelnetDefaultState extends TelnetStateThatCloseAndSend {

  public TelnetDefaultState(TelnetStateFactory factory) {
    super(factory);
  }

  @Override
  protected TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator){
    if (b == IAC) {
      return getFactory().lastWasIAC();
    }
    textAccumulator.accumulate(b);
    if (b == CR ){
      return getFactory().lastWasCR();
    }else if(b==LF){
      return getFactory().lastWasLFWithoutCRBefore();
    }
    return this;    
  }

}