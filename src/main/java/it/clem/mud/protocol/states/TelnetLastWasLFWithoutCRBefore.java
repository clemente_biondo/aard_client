package it.clem.mud.protocol.states;

import static it.clem.mud.protocol.TelnetProtocolResources.*;

//AARD usa LF CR al posto dello standard CR LF
public class TelnetLastWasLFWithoutCRBefore extends TelnetStateThatCloseAndSend {

  public TelnetLastWasLFWithoutCRBefore(TelnetStateFactory factory) {
    super(factory);
  }

  @Override
  protected TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator) {
    if (b == IAC) {
      return getFactory().lastWasIAC();
    }
    
    textAccumulator.accumulate(b);
    if (b == CR) {
      dataAccumulator.accumulate(textAccumulator.popAsTelnetText());
    }
    //LF non preceduto o seguito da CR non fa nulla
    return getFactory().defaultState();
  }

}
