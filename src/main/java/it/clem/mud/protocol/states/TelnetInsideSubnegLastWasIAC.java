package it.clem.mud.protocol.states;

import static it.clem.mud.protocol.TelnetProtocolResources.*;


public class TelnetInsideSubnegLastWasIAC extends TelnetStateThatCloseAndNotSend {
  private final int option;

  public TelnetInsideSubnegLastWasIAC(TelnetStateFactory factory, int option) {
    super(factory);
    this.option = option;
  }

  @Override
  protected TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator) {
    if (b == IAC) {
      textAccumulator.accumulate(IAC);
      return getFactory().insideSubneg(option);
    }
    if (b == SE) {
      dataAccumulator.addSubnegotiationCommand(option, textAccumulator.popBytes());  
      return getFactory().defaultState();
    } else {
      throw new IllegalStateException("option:" + b);
    }

  }

}
