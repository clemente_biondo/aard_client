package it.clem.mud.protocol.states;

import static it.clem.mud.protocol.TelnetProtocolResources.IAC;

public class TelnetInsideSubneg extends TelnetStateThatCloseAndNotSend  {
  private final int option;

  public TelnetInsideSubneg(TelnetStateFactory factory, int option) {
    super(factory);
    this.option = option;
  }

  @Override
  public TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator) {
    if (b == IAC) {
      return getFactory().insideSubnegLastWasIAC(option);
    }
    textAccumulator.accumulate(b);
    return this;
  }

}
