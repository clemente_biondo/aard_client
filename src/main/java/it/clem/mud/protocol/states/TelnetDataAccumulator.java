package it.clem.mud.protocol.states;

import java.util.LinkedList;
import java.util.Queue;

import it.clem.mud.protocol.data.TelnetConnectionClosedByServer;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.shared.telnet.TelnetDataFactory;

public class TelnetDataAccumulator {
  private final TelnetDataFactory factory;
  private final Queue<TelnetData> accumulator = new LinkedList<>();
  private boolean closed=false;
  
  public TelnetDataAccumulator(TelnetDataFactory factory) {
    super();
    this.factory = factory;
  }

  public void accumulate(TelnetData data) {
    assert !closed;
    if (data== null){
      return;
    }
    accumulator.add(data);
  }

  public TelnetData popData() {
    if (closed && accumulator.isEmpty()){
      return TelnetConnectionClosedByServer.getInstance();
    }
    return accumulator.poll();
  }

  public void addSubnegotiationCommand(int option,byte[] content) {
    accumulate(factory.getSubnegotionation(option, content));
  }
  
  public void addCommand(int code) {
    accumulate(factory.getCommand(code));
  }
  
  public void addCommandWithOption(int code,int option) {
    accumulate(factory.getCommandWithOption(code, option));
  }
  
  public boolean isEmpty(){
    return !closed && accumulator.isEmpty();
  }
  
  public void addTelnetClosed() {
    assert !closed;
    closed=true;
  }
}
