package it.clem.mud.protocol.states;

import static it.clem.mud.protocol.TelnetProtocolResources.*;

public class TelnetStateFactoryImpl implements TelnetStateFactory {
  private final TelnetLastWasIAC               lastWasIAC;
  private final TelnetLastWasCR                lastWasCR;
  private final TelnetLastWasLFWithoutCRBefore lastWasLFWithoutCRBefore;

  private final TelnetDefaultState defaultState;
  private final TelnetOptionState  willState;
  private final TelnetOptionState  wontState;
  private final TelnetOptionState  doState;
  private final TelnetOptionState  dontState;
  private final TelnetSubnegStart  subnegStartState;
  // private final TelnetSubnegStart insideSubnegState;

  private final TelnetInsideSubneg           subnegCMP2;
  private final TelnetInsideSubnegLastWasIAC subnegCMP2_IAC;

  private final TelnetInsideSubneg           subnegAARD;
  private final TelnetInsideSubnegLastWasIAC subnegAARD_IAC;

  private final TelnetInsideSubneg           subnegTTY;
  private final TelnetInsideSubnegLastWasIAC subnegTTY_IAC;

  private final TelnetInsideSubneg           subnegGMCP;
  private final TelnetInsideSubnegLastWasIAC subnegGMCP_IAC;

  public TelnetStateFactoryImpl() {
    super();
    this.lastWasIAC = new TelnetLastWasIAC(this);
    this.defaultState = new TelnetDefaultState(this);
    this.willState = new TelnetOptionState(this, WILL);
    this.wontState = new TelnetOptionState(this, WONT);
    this.doState = new TelnetOptionState(this, DO);
    this.dontState = new TelnetOptionState(this, DONT);
    this.subnegStartState = new TelnetSubnegStart(this);
    this.lastWasCR = new TelnetLastWasCR(this);
    this.lastWasLFWithoutCRBefore = new TelnetLastWasLFWithoutCRBefore(this);
    this.subnegCMP2 = new TelnetInsideSubneg(this, CMP2);
    this.subnegCMP2_IAC = new TelnetInsideSubnegLastWasIAC(this, CMP2);
    this.subnegAARD = new TelnetInsideSubneg(this, AARD);
    this.subnegAARD_IAC = new TelnetInsideSubnegLastWasIAC(this, AARD);
    this.subnegTTY = new TelnetInsideSubneg(this, TERMINAL_TYPE);
    this.subnegTTY_IAC = new TelnetInsideSubnegLastWasIAC(this, TERMINAL_TYPE);

    this.subnegGMCP = new TelnetInsideSubneg(this, GMCP);
    this.subnegGMCP_IAC = new TelnetInsideSubnegLastWasIAC(this, GMCP);

  }

  @Override
  public TelnetLastWasIAC lastWasIAC() {
    return this.lastWasIAC;
  }

  @Override
  public TelnetDefaultState defaultState() {
    return this.defaultState;
  }

  @Override
  public TelnetOptionState optionState(int code) {
    switch (code) {
    case WILL:
      return this.willState;
    case WONT:
      return this.wontState;
    case DO:
      return this.doState;
    case DONT:
      return this.dontState;
    default:
      throw new IllegalArgumentException("code:" + code);
    }
  }

  @Override
  public TelnetInsideSubneg insideSubneg(int option) {
    switch (option) {
    case TERMINAL_TYPE:
      return subnegTTY;
    case CMP2:
      return subnegCMP2;
    case AARD:
      return subnegAARD;
    case GMCP:
      return subnegGMCP;

    default:
      throw new IllegalArgumentException("option:" + option);
    }

  }

  @Override
  public TelnetSubnegStart subnegStart() {
    return subnegStartState;
  }

  @Override
  public TelnetInsideSubnegLastWasIAC insideSubnegLastWasIAC(int option) {
    switch (option) {
    case TERMINAL_TYPE:
      return subnegTTY_IAC;
    case CMP2:
      return subnegCMP2_IAC;
    case AARD:
      return subnegAARD_IAC;
    case GMCP:
      return subnegGMCP_IAC;
    default:
      throw new IllegalArgumentException("option:" + option);
    }

  }

  @Override
  public TelnetLastWasCR lastWasCR() {
    return lastWasCR;
  }

  @Override
  public TelnetLastWasLFWithoutCRBefore lastWasLFWithoutCRBefore() {
    return lastWasLFWithoutCRBefore;
  }
}
