package it.clem.mud.protocol.states;

public interface TelnetState {
  TelnetState next(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator);
}