package it.clem.mud.protocol.states;

import static it.clem.mud.protocol.TelnetProtocolResources.*;

public class TelnetLastWasCR extends TelnetStateThatCloseAndSend {

  public TelnetLastWasCR(TelnetStateFactory factory) {
    super(factory);
  }

  @Override
  protected TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator) {
    if (b == IAC) {
      return getFactory().lastWasIAC();
    }
    
    textAccumulator.accumulate(b);
    if (b == LF) {
      dataAccumulator.accumulate(textAccumulator.popAsTelnetText());
    }
    //CR non preceduto o seguito da LF non fa nulla 
    return getFactory().defaultState();
  }

}
