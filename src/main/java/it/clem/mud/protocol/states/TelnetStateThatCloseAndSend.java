package it.clem.mud.protocol.states;

public abstract class TelnetStateThatCloseAndSend extends TelnetStateBase {

  public TelnetStateThatCloseAndSend(TelnetStateFactory factory) {
    super(factory);
  }

  @Override
  public final TelnetState next(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator) {
    if (b<0){
      dataAccumulator.accumulate(textAccumulator.popAsTelnetText());
      if (b == -1){
        dataAccumulator.addTelnetClosed();
      }
      return this;
    }    
    return nextIfNotClosed(b, textAccumulator, dataAccumulator);
  }

  protected abstract TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator);
}
