package it.clem.mud.protocol.states;

public interface TelnetStateFactory {
  TelnetLastWasLFWithoutCRBefore lastWasLFWithoutCRBefore();
  TelnetLastWasCR lastWasCR();
  TelnetLastWasIAC lastWasIAC();
  TelnetDefaultState defaultState();
  TelnetOptionState optionState(int code);
  TelnetSubnegStart subnegStart();
  TelnetInsideSubneg insideSubneg(int option);
  TelnetInsideSubnegLastWasIAC insideSubnegLastWasIAC(int option);
}
