package it.clem.mud.protocol.states;

import static it.clem.mud.protocol.TelnetProtocolResources.*;

public class TelnetLastWasIAC extends TelnetStateThatCloseAndNotSend {

  public TelnetLastWasIAC(TelnetStateFactory factory) {
    super(factory);
  }

  @Override
  public TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator){
    if (b == IAC) {
      textAccumulator.accumulate(b);
      return getFactory().defaultState();
    }
    //se sono qua IAC � seguito da un comando quindi devo scaricare il testo accumulato
    dataAccumulator.accumulate(textAccumulator.popAsTelnetText());
    
    if (b == SB) {
      return getFactory().subnegStart();
    }
    if (isTwoBytesCode(b)) {
      dataAccumulator.addCommand(b);
      return getFactory().defaultState();
    }
    if (isThreeBytesCode(b)) {
      return getFactory().optionState(b);
    }
    throw new IllegalArgumentException("code:" + b);
  }

}
