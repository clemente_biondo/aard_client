package it.clem.mud.protocol.states;


public class TelnetSubnegStart  extends TelnetStateThatCloseAndNotSend{

  public TelnetSubnegStart(TelnetStateFactory factory) {
    super(factory);
  }

  @Override
  protected TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator){
    return getFactory().insideSubneg(b);
  }


}
