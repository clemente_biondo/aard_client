package it.clem.mud.protocol.states;

public class TelnetOptionState extends TelnetStateThatCloseAndNotSend {
  private final int code;

  public TelnetOptionState(TelnetStateFactory factory, int code) {
    super(factory);
    this.code = code;
  }

  @Override
  protected TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator){
    dataAccumulator.addCommandWithOption(code, b);
    return getFactory().defaultState();
  }

}
