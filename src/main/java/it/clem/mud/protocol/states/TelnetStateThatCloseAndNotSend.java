package it.clem.mud.protocol.states;

public abstract class TelnetStateThatCloseAndNotSend extends TelnetStateBase {

  public TelnetStateThatCloseAndNotSend(TelnetStateFactory factory) {
    super(factory);
  }

  @Override
  public final TelnetState next(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator) {
    if (b < 0) {
      if (b == -1) {
        dataAccumulator.addTelnetClosed();
      }
      return this;
    }
    return nextIfNotClosed(b, textAccumulator, dataAccumulator);
  }

  protected abstract TelnetState nextIfNotClosed(int b, TextAccumulator textAccumulator, TelnetDataAccumulator dataAccumulator);
}
