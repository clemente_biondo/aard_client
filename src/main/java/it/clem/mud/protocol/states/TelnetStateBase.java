package it.clem.mud.protocol.states;


public abstract class TelnetStateBase implements TelnetState {
  private final TelnetStateFactory factory;

  public TelnetStateBase(TelnetStateFactory factory) {
    super();
    this.factory = factory;
  }

  protected TelnetStateFactory getFactory() {
    return factory;
  }
  

}
