/**
 *  Contiene le classi di proprietà esclusiva e non condivise del thread che dialoga via telnet con il server remoto. 
 */
package it.clem.mud.protocol;