package it.clem.mud.protocol;

import it.clem.mud.util.LangUtils;

//http://www.networksorcery.com/enp/rfc/rfc854.txt
public class TelnetProtocolResources {

  // CODES
  public static final int SE = 240; // SE 0xf0 End of subnegotiation parameters.
  public static final int NOP = 241; // NOP 0xf1 No operation.
  public static final int DM = 242; // Data Mark 0xf2 The data stream portion of a Synch. This should always be
  // accompanied by a TCP Urgent notification.
  public static final int BRK = 243; // Break 0xf3 NVT character BRK.
  public static final int IP = 244; // Interrupt Process 0xf4 The function IP.
  public static final int AO = 245; // Abort output 0xf5 The function AO.
  public static final int AYT = 246; // Are You There 0xf6 The function AYT.
  public static final int EC = 247; // Erase character 0xf7 The function EC.
  public static final int EL = 248; // Erase Line 0xf8 The function EL.
  public static final int GA = 249; // Go ahead 0xf9 The GA signal.
  public static final int SB = 250; // SB 0xfa Indicates that what follows is subnegotiation of the indicated
  // option.
  public static final int WILL = 251; // WILL (option code) 0xfb Indicates the desire to begin performing, or
  // confirmation that you are now performing, the indicated option.
  public static final int WONT = 252; // WON'T (option code) 0xfc Indicates the refusal to perform, or continue
  // performing, the indicated option.
  public static final int DO = 253; // DO (option code) 0xfd Indicates the request that the other party perform, or
  // confirmation that you are expecting the other party to perform, the indicated
  // option.
  public static final int DONT = 254; // DON'T (option code) 0xfe Indicates the demand that the other party stop
  // performing, or confirmation that you are no longer expecting the other party
  // to perform, the indicated option.

  public static final int IAC = 255; // IAC 0xff Data Byte 255.
  //@formatter:off
	public final static String[] CODES = new String[] { 
			"0","1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", 
			"16","17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", 
			"32","33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", 
			"48","49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63",
			"64","65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79",
			"80","81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95",
			"96","97", "98" ,"99", "100","101","102","103","104","105","106","107","108","109","110","111",
			"112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127",
			"128","129","130","131","132","133","134","135","136","137","138","139","140","141","142","143",
			"144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159",
			"160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175",
			"176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191",
			"192","193","194","195","196","197","198","199","200","201","202","203","204","205","206","207",
			"208","209","210","211","212","213","214","215","216","217","218","219","220","221","222","223",
			"224","225","226","227","228","229","230","231","232","233","234","235","236","237","238","239", 
			"SE","NOP","DM", "BRK","IP", "AO", "AYT","EC", "EL", "GA", "SB", "WILL",	"WONT", "DO", "DONT", "IAC" };
	
  public final static String[] OPTIONS = new String[] { 
      "0", "ECHO",  "2",  "Suppress Go Ahead",  "4",  "5",  "Timing Mark",  "7",  "8",   "9",  "10", "11", "12", "13", "14", "15", 
      "16","17", "18", "19", "20", "21", "22", "23", "TERMINAL-TYPE", "END-OF-RECORD", "26", "27", "28", "29", "30", "NAWS", 
      "Terminal Speed","33", "Linemode", "35", "36", "37", "38", "New Environment Option", "40", "41", "42", "43", "44", "45", "46", "47", 
      "48","49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63",
      "64","65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", 
      "80","81", "82", "83", "84", "CMP1", "CMP2", "87", "88", "89", "90", "91", "92", "93", "94", "95", 
      "96","97", "98" ,"99", "100","101","AARD","103","104","105","106","107","108","109","110","111",
      "112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127",
      "128","129","130","131","132","133","134","135","136","137","138","139","140","141","142","143",
      "144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159",
      "160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175",
      "176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191",
      "192","193","194","195","196","197","198","199","ATCP","GMCP","202","203","204","205","206","207",
      "208","209","210","211","212","213","214","215","216","217","218","219","220","221","222","223",
      "224","225","226","227","228","229","230","231","232","233","234","235","236","237","238","239",
      "240","241","242","243","244","245","246","247","248","249","250","251","252","253","254","255" };	
  //@formatter:on
  
  public static final byte[] SUBNEG_START_COMPRESSION= new byte[]{-1,-6,86,-1,-16};
  
  public static final int TERMINAL_TYPE_IS = 0; 
  public static final int TERMINAL_TYPE_SEND = 1;
  
  // OPTIONS
  public static final int ECHO = 1; // ECHO
  public static final int TERMINAL_TYPE = 24; // TERMINAL-TYPE 0x18 terminal type information https://www.ietf.org/rfc/rfc1091.txt
  // https://tools.ietf.org/html/rfc884
  public static final int NAWS = 31; // NAWS 0x1f Negotiate About Window Size https://www.ietf.org/rfc/rfc1073.txt
  public static final int CMP1 = 85; // 0x55 MCCP Compress
  public static final int CMP2 = 86; // 0x56 MCCP Compress2
  public static final int ATCP = 200; // 0xC8 Achaea Telnet Client Protocol
  public static final int GMCP = 201; // 0xC9 Generic MUD Communication Protocol
  public static final int AARD = 102; // AARD-STATUS 0x66

  // Printer codes
  public static final int NUL = 0; // NULL. No Operation
  public static final int BELL = 7; // BELL. Produces an audible or visible signal (which does NOT move the print head).
  public static final int BS = 8; // Back Space. Moves the print head one character position towards the left margin.
  public static final int HT = 9; // Horizontal Tab. Moves the printer to the next horizontal tab stop. It remains unspecified how either party determines or
                                  // establishes where such tab stops are located.
  public static final int LF = 10; //0A \n Line Feed. Moves the printer to the next print line, keeping the same horizontal position.
  public static final int VT = 11; // Vertical Tab. Moves the printer to the next vertical tab stop. It remains unspecified how either party determines or
                                   // establishes where such tab stops are located.
  public static final int FF = 12; // Form Feed. Moves the printer to the top of the next page, keeping the same horizontal position.
  public static final int CR = 13; // 0D \r Carriage Return. Moves the printer to the left margin of the current line.

  public static final String CRLF_NORMAL_EOL="\r\n";
  public static final String LFCR_AARD_EOL="\n\r";
  
  //http://www.freesoft.org/CIE/RFC/1123/31.htm
  //The Telnet protocol defines the sequence CR LF to mean "end- of-line".
  //CR LF and CR NUL MUST have the same effect on an ASCII server host when received as input over a Telnet connection
  //AARD se ne frega ed usa LF CR
  //https://zuggsoft.com/forums/viewtopic.php?t=31260&postdays=0&postorder=asc&start=0
  
  // http://www.aardwolf.com/blog/2008/07/10/telnet-negotiation-control-mud-client-interaction/

  //IAC WILL ECHO (server will echo) è una tecnica per sopprimere il local echo e si usa per chiedere la password.
  //nel mio caso a will rispondo do ed a wont dont, per il resto non devo fare nulla
  
  // TODO:aggiungere options e relative descrizioni

  public static boolean isTwoBytesCode(int code) {
    return code >= SE && code <= SB;
  }

  public static boolean isThreeBytesCode(int code) {
    return code >= WILL && code <= DONT;
  }

  public static int confirmCommandWithOption(int code) {
    switch (code) {
    case WILL:
      return DO;
    case DO:
      return WILL;
    case DONT:
      return WONT;
    case WONT:
      return DONT;
    default:
      throw new IllegalArgumentException("Code:" + code);
    }

  }
  


  public static int refuseCommandWithOption(int code) {
    switch (code) {
    case WILL:
      return DONT;
    case DO:
      return WONT;
    default:
      throw new IllegalArgumentException("Code:" + code);
    }
  }
  public static int negateCommand(int code) {
    switch (code) {
    case WILL:
      return WONT;
    case DO:
      return DONT;
    case WONT:
      return WILL;
    case DONT:
      return DO;
    default:
      throw new IllegalArgumentException("Code:" + code);
    }
  }
  public static int oppositeCommand(int code) {
    switch (code) {
    case WILL:
      return DONT;
    case DO:
      return WONT;
    case WONT:
      return DO;
    case DONT:
      return WILL;
    default:
      throw new IllegalArgumentException("Code:" + code);
    }
  }

  public static int simmetricCommand(int code) {
    switch (code) {
    case WILL:
      return DO;
    case DO:
      return WILL;
    case WONT:
      return DONT;
    case DONT:
      return WONT;
    default:
      throw new IllegalArgumentException("Code:" + code);
    }
  }

  
  public static byte[] IAC_SB=LangUtils.arrOfIntToArrOfByte(IAC,SB);
  public static byte[] IAC_SE=LangUtils.arrOfIntToArrOfByte(IAC,SE);
  public static byte[] IAC_DO_CMP2=LangUtils.arrOfIntToArrOfByte(IAC,DO,CMP2);
  public static byte[] IAC_DONT_CMP1=LangUtils.arrOfIntToArrOfByte(IAC,DONT,CMP1);
  public static byte[] IAC_DO_AARD=LangUtils.arrOfIntToArrOfByte(IAC,DO,AARD);
  public static byte[] IAC_DONT_ATCP=LangUtils.arrOfIntToArrOfByte(IAC,DONT,ATCP);
  public static byte[] IAC_DO_GMCP=LangUtils.arrOfIntToArrOfByte(IAC,DO,GMCP);
  public static byte[] IAC_WILL_AARD=LangUtils.arrOfIntToArrOfByte(IAC,WILL,AARD);
  public static byte[] IAC_WILL_TTY=LangUtils.arrOfIntToArrOfByte(IAC,WILL,TERMINAL_TYPE);
  public static byte[] IAC_WONT_NAWS=LangUtils.arrOfIntToArrOfByte(IAC,WONT,NAWS);

  public final static  byte[] START_COMPRESSION_SUBNEG = LangUtils.arrOfIntToArrOfByte(IAC,SB,CMP2,IAC,SE);
  
  public final static  byte[] REQUEST_TERMINAL_SEND = LangUtils.arrOfIntToArrOfByte(IAC,SB,TERMINAL_TYPE,TERMINAL_TYPE_SEND,IAC,SE);
  public final static  byte[] REQUEST_TERMINAL_SEND_CONTENT = LangUtils.arrOfIntToArrOfByte(TERMINAL_TYPE_SEND);

  public final static  byte[] RESPOND_TERMINAL_IS_MUSHCLIENT= LangUtils.arrOfIntToArrOfByte(IAC,SB,TERMINAL_TYPE,TERMINAL_TYPE_IS,77,85,83,72,99,108,105,101,110,116,45,65,97,114,100,IAC,SE);
  public final static  byte[] RESPOND_TERMINAL_IS_MUSHCLIENT_CONTENT = LangUtils.arrOfIntToArrOfByte(/*IAC,SB,TERMINAL_TYPE,*/TERMINAL_TYPE_IS,77,85,83,72,99,108,105,101,110,116,45,65,97,114,100/*,IAC,SE*/);

}
