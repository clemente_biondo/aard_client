package it.clem.mud.protocol.telnet.reader.impl;

import java.io.IOException;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetText;
import it.clem.mud.protocol.telnet.reader.SkipableTelnetReader;
import it.clem.mud.protocol.telnet.reader.TelnetReader;

public class SkipableTelnetReaderImpl implements SkipableTelnetReader {
	private final TelnetReader tr;

	public SkipableTelnetReaderImpl(TelnetReader tr) {
		super();
		this.tr = tr;
	}

	@Override
	public TelnetData read() throws IOException {
		return tr.read();
	}

	@Override
	public Result skip(int n) throws IOException {
		Result.Builder res = Result.builder();
		for (int i = 0; i < n; i++) {
			res.addSkipped(read());
		}
		return res.build();
	}

	@Override
	public Result readUntil(Predicate<TelnetData> predicate) throws IOException {
		Result.Builder res = Result.builder();
		TelnetData data;
		for (;;) {
			data = read();
			if (predicate.test(data)) {
				res.setReceived(data);
			} else {
				res.addSkipped(data);
			}
			if (res.receiverClosed() || res.receiverMatched()) {
				break;
			}
		}
		return res.build();
	}

	@Override
	public Result readUntilText(String regexp) throws IOException {
		return readUntilText(Pattern.compile(regexp));
	}

	@Override
	public Result readUntilText(Pattern pattern) throws IOException {
		return readUntil((td) -> td instanceof TelnetText && pattern.matcher(td.toString()).matches());
	}

	@Override
	public Result readUntilText(Class<TelnetData> clazz) throws IOException {
		return readUntil((td) -> clazz.isInstance(td));
	}

	@Override
	public Result readUntilTextContains(String text) throws IOException {
		return readUntil((td) -> td instanceof TelnetText && td.toString().contains(text));
	}

  public void startCompression() {
    tr.startCompression();
  }

  public void stopCompression() {
    tr.stopCompression();
  }

  @Override
  public void close() throws IOException {
    tr.close();
  }

}
