package it.clem.mud.protocol.telnet.reader.impl;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.states.TelnetDataAccumulator;
import it.clem.mud.protocol.states.TelnetState;
import it.clem.mud.protocol.states.TelnetStateFactory;
import it.clem.mud.protocol.states.TelnetStateFactoryImpl;
import it.clem.mud.protocol.states.TextAccumulator;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.telnet.TelnetDataFactory;

public class TelnetReaderImpl implements TelnetReader {
  private final static Logger log               = LoggerFactory.getLogger(TelnetReaderImpl.class);

  private final TelnetDataFactory tFactory;
  private final TelnetStateFactory factory = new TelnetStateFactoryImpl();
  private final SourceStream sourceStream;
  private final TelnetDataAccumulator dataAccumulator;
  private TelnetState                 currState       = factory.defaultState();


  public TelnetReaderImpl(SourceStream sourceStream,TelnetDataFactory tFactory) {
    super();
    this.sourceStream = sourceStream;
    this.tFactory=tFactory;
    this.dataAccumulator= new TelnetDataAccumulator(tFactory);
  }
  
//  public TelnetReceiverImpl(InputStream is) {
//    this(new SourceStreamImpl(is));
//  }
  
  @Override
  public void close() {
    try {
      this.sourceStream.close();
    } catch (IOException ioe) {
      log.error("Errore durante la chiusura dell'inputstream compresso:" + ioe.getMessage(), ioe);
    }
  }
  
  @Override
  public TelnetData read() throws IOException {
    final TextAccumulator textAccumulator = new TextAccumulator(tFactory);
    while (dataAccumulator.isEmpty()) {
      currState = currState.next(sourceStream.read(), textAccumulator, dataAccumulator);
    }
    TelnetData data= dataAccumulator.popData();
//    if(log.isTraceEnabled()){
//        log.trace("Ricevuto: "+data);
//    }
    return data;
  }

  public void startCompression() {
    sourceStream.startCompression();
  }

  public void stopCompression() {
    sourceStream.stopCompression();
  }

 
}
