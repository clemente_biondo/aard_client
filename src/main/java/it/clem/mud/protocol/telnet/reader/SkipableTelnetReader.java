package it.clem.mud.protocol.telnet.reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import com.google.common.collect.ImmutableList;

import it.clem.mud.protocol.data.TelnetData;

public interface SkipableTelnetReader extends TelnetReader {

	Result skip(int n) throws IOException;

	Result readUntil(Predicate<TelnetData> predicate) throws IOException;

	Result readUntilText(Pattern pattern) throws IOException;

	Result readUntilText(Class<TelnetData> clazz) throws IOException;

	Result readUntilText(String regexp) throws IOException;
	
	Result readUntilTextContains(String text) throws IOException;

	class Result {
		public final List<TelnetData> skipped;
		public final TelnetData received;

		public Result(List<TelnetData> skipped, TelnetData received) {
			super();
			this.skipped = ImmutableList.copyOf(skipped);
			this.received = received;
		}

		public static Builder builder() {
			return new Builder();
		}

		public static class Builder {
			private final List<TelnetData> skipped = new ArrayList<>();
			private TelnetData received;

			public Builder addSkipped(TelnetData data) {
				if (!receiverClosed()) {
					skipped.add(data);
				}
				return this;
			}

			public boolean receiverMatched() {
				return received != null;
			}

			public boolean receiverClosed() {
				int size = skipped.size();
				if (size > 0 && skipped.get(size - 1).itDenotesConnectionClosedByServer()) {
					return true;
				}
				return false;
			}

			public Builder setReceived(TelnetData received) {
				this.received = received;
				return this;
			}

			public Result build() {
				return new Result(skipped, received);
			}

		}
	}

}
