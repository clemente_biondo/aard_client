package it.clem.mud.protocol.telnet.reader.impl;

import java.io.IOException;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.shared.recording.ServerRecordProducer;

public class RecordableTelnetReader implements TelnetReader {
  private final TelnetReader tr;
  private final ServerRecordProducer recordProducer;

  public RecordableTelnetReader(TelnetReader tr, ServerRecordProducer recordProducer) {
    super();
    this.tr = tr;
    this.recordProducer = recordProducer;
  }

  @Override
  public TelnetData read() throws IOException {
    TelnetData data = null;
    try {
      data = tr.read();
      return data;
    } finally {
      recordProducer.recordTelnetDataReceived(data);
    }
  }

  @Override
  public void startCompression() {
    tr.startCompression();
  }

  @Override
  public void stopCompression() {
    tr.stopCompression();
  }

  @Override
  public void close() throws IOException {
    tr.close();
  }

}
