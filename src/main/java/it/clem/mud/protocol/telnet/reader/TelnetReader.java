package it.clem.mud.protocol.telnet.reader;

import java.io.IOException;
import it.clem.mud.protocol.data.TelnetData;

public interface TelnetReader extends AutoCloseable{
  TelnetData read() throws IOException;
  void startCompression();
  void stopCompression();
  void close() throws IOException;
}
