package it.clem.mud.protocol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.handlers.NVTSupport;
import it.clem.mud.protocol.data.handlers.TelnetDataHandlerBus;
import it.clem.mud.protocol.loop.AardTelnetContextImpl;
import it.clem.mud.protocol.loop.RecordableAardTelnetContext;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.protocol.telnet.reader.impl.RecordableTelnetReader;
import it.clem.mud.protocol.telnet.reader.impl.TelnetReaderImpl;
import it.clem.mud.shared.game.event.queue.ServerEventProducer;
import it.clem.mud.shared.recording.ServerRecordProducer;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;

public class AardServerThread extends Thread {
  private final static Logger log = LoggerFactory.getLogger(AardServerThread.class);
  private final ServerEventProducer eventSender;
  private final SourceStream sourceStream;
  public final static String THREAD_NAME = "aardwolf-server";

  private final NVTSupport nvtSupport;
  private final TelnetWriter sender;
  private final TelnetDataFactory factory;
  private final ServerRecordProducer recordQueue;

  public AardServerThread(ServerRecordProducer recordQueue, ServerEventProducer eventSender, SourceStream sourceStream,
      TelnetWriter sender, NVTSupport nvtSupport,TelnetDataFactory factory) {
    super(THREAD_NAME);
    this.eventSender = eventSender;
    this.sourceStream = sourceStream;
    this.sender = sender;
    this.nvtSupport = nvtSupport;
    this.recordQueue = recordQueue;
    this.factory=factory;
  }

  @Override
  public void run() {
    log.trace("thread avviato.");
    final TelnetDataHandlerBus telnetDataHandlerBus = getTelnetDataHandlerBus(nvtSupport);
    final TelnetReader receiver = getTelnetReceiver(sourceStream, recordQueue,factory);
    final AardTelnetContext aardTelnetContext = getAardTelnetContext(recordQueue, eventSender, receiver, sender,factory);
    try {
      // Oltre che in test, questo thread viene interrotto dal game loop
      // in tutti i casi in cui quello riceve un evento che richiede la disconnessione, ad es su richiesta del client
      while (!Thread.currentThread().isInterrupted()) {
        TelnetData data = receiver.read();
        telnetDataHandlerBus.handle(data, aardTelnetContext);
        if (data.itDenotesConnectionClosedByServer()) {
          log.debug("connessione chiusa dal server, server loop terminato ");
          break;
        }
      }
    } catch (Exception ex) {
      log.error("thread terminato a seguito di eccezione:" + ex.getMessage(), ex);
      eventSender.fireServerErrorEvent(ex.getMessage());
    }
    if (Thread.currentThread().isInterrupted()) {
      log.trace("thread terminato a seguito di interruzione ");
    }
    log.trace("thread terminato.");
  }

  // E' protected per consentire i test
  protected TelnetReader getTelnetReceiver(SourceStream sourceStream, ServerRecordProducer recordQueue,TelnetDataFactory factory) {
    return new RecordableTelnetReader(new TelnetReaderImpl(sourceStream,factory), recordQueue);
  }

  // E' protected per consentire i test
  protected AardTelnetContext getAardTelnetContext(ServerRecordProducer recordQueue, ServerEventProducer eventSender,
      TelnetReader receiver, TelnetWriter sender,TelnetDataFactory factory) {
    return new RecordableAardTelnetContext(
            new AardTelnetContextImpl(eventSender, receiver, sender, factory), recordQueue);
  }

  // E' protected per consentire i test
  protected TelnetDataHandlerBus getTelnetDataHandlerBus(NVTSupport client) {
    return TelnetDataHandlerBus.getClient(client);
  }

  // public static AardServerThread forTest(ServerRecordProducer recordQueue,ServerEventProducer eventSender,
  // TelnetDataHandlerBus telnetDataHandlerBus,
  // TelnetSender sender, SourceStream sourceStream, AardTelnetContext mock) {
  // return new AardServerThread(recordQueue,eventSender, sourceStream, sender, null) {
  // @Override AardTelnetContext getAardTelnetContext(ServerRecordProducer recordQueue,ServerEventProducer eventSender,
  // TelnetReceiver receiver,
  // TelnetSender sender) {
  // return mock;
  // }
  //
  // @Override TelnetDataHandlerBus getTelnetDataHandlerBus(NVTSupport client) {
  // return telnetDataHandlerBus;
  // }
  // };
  // }
  //
  // public static AardServerThread forTest(ServerRecordProducer recordQueue,ServerEventProducer eventSender,
  // TelnetDataHandlerBus telnetDataHandlerBus,
  // TelnetSender sender, SourceStream sourceStream, TelnetDataFactory factory) {
  // return new AardServerThread(recordQueue,eventSender, sourceStream, sender, null) {
  // @Override AardTelnetContext getAardTelnetContext(ServerRecordProducer recordQueue,ServerEventProducer eventSender,
  // TelnetReceiver receiver,
  // TelnetSender sender) {
  // return new AardTelnetContextImpl(recordQueue,eventSender, receiver, sender, factory);
  // }
  //
  // @Override TelnetDataHandlerBus getTelnetDataHandlerBus(NVTSupport client) {
  // return telnetDataHandlerBus;
  // }
  // };
  // }

}
