package it.clem.mud.protocol.loop;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

import it.clem.mud.protocol.data.TelnetCommandWithOption;
import static it.clem.mud.protocol.TelnetProtocolResources.*;
import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetGMCPData;
import it.clem.mud.protocol.data.TelnetPositiveCommandWithOption;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.shared.game.event.queue.ServerEventProducer;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;

/*
 L'attuale versione di AardTelnetContext è passiva nella negoziazione delle opzioni, 
 ossia non propone nulla al server ma accetta o rifiuta le sue proposte Se questa cosa
 in futuro dovesse cambiare con l'aggiunta in interfaccia di metodi quali proponiAAA, 
 proponiBBB allora tutta l'implementazione che stabilisce se una opzione è attiva e 
 se un comando è già stato negoziato va riscrittia ed è molto più cmplessa.
 */
public class AardTelnetContextImpl implements AardTelnetContext {
  private final ServerEventProducer eventQueue;
  private final TelnetReader receiver;
  private final TelnetWriter sender;
  private final TelnetDataFactory factory;
  private final static Logger log = LoggerFactory.getLogger(AardTelnetContextImpl.class);

  private Map<Integer, Map<Integer, Boolean>> alreadyEstablished = new TreeMap<>();// option,command
  public final Set<Integer> clientOptionsActive = new TreeSet<>();
  public final Set<Integer> serverOptionsActive = new TreeSet<>();
  public final static Map<Integer, Boolean> OPTIONS_NOT_NEGOTIATED = ImmutableMap.of(WILL, Boolean.FALSE, WONT,
      Boolean.FALSE, DO, Boolean.FALSE, DONT, Boolean.FALSE);

  public AardTelnetContextImpl(ServerEventProducer eventQueue, TelnetReader receiver, TelnetWriter sender,
      TelnetDataFactory factory) {
    super();
    this.eventQueue = eventQueue;
    this.receiver = receiver;
    this.sender = sender;
    this.factory = factory;
  }

  private void send(TelnetData data) throws UncheckedIOException {
    try {
      sender.write(data);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private void replyCommand(TelnetCommandWithOption data) throws UncheckedIOException {
    if (isCommandAlreadyEstablished(data)) {
      ignore(data);
    } else {
      send(data);
      rememberCommandAlreadyEstablished(data);
      if (data.getCode() == WONT) {// sto rispondendo a do/dont option
        clientOptionsActive.remove(data.getOption());
      } else if (data.getCode() == WILL) {// sto rispondendo a do option
        clientOptionsActive.add(data.getOption());
      } else if (data.getCode() == DONT) {// sto rispondendo a will/wont option
        serverOptionsActive.remove(data.getOption());
      } else if (data.getCode() == DO) {// sto rispondendo a will option
        serverOptionsActive.add(data.getOption());
      }
    }
  }

  @Override
  public void startCompression() {
    receiver.startCompression();
  }

  @Override
  public void stopCompression() {
    receiver.stopCompression();
  }

  @Override
  public void replyWithConfirmationOf(TelnetCommandWithOption data) throws UncheckedIOException {
    TelnetCommandWithOption reply = factory.confirm(data);
    log.debug("Conferma di:" + data + "con " + reply);
    replyCommand(reply);
  }

  @Override
  public void replyWithRefusalOf(TelnetPositiveCommandWithOption data) throws UncheckedIOException {
    TelnetCommandWithOption reply = factory.refuse(data);
    log.debug("Rifiuto di:" + data + "con " + reply);
    replyCommand(reply);

  }

  @Override
  public void ignore(TelnetData data) {
    log.warn("Ignorato:" + data.toString());
  }

  @Override
  public void signalAsNotSupported(TelnetData data) {
    throw new java.lang.UnsupportedOperationException(data.getType().name());
  }

  @Override
  public void fireServerErrorEvent(String message) {
    eventQueue.fireServerErrorEvent(message);
  }

  @Override
  public void fireConnectionClosedByServerEvent() {
    eventQueue.fireConnectionClosedByServerEvent();
  }

  @Override
  public void fireTextSentFromServerEvent(String text) {
    eventQueue.fireTextSentFromServerEvent(text);
  }
  
  @Override
  public void fireMapTagUpdated(String map) {
    eventQueue.fireMapTagUpdated(map);
  }
  
  @Override
  public void sendGMCPCommand(String todo) throws UncheckedIOException {
    // TODO Auto-generated method stub

  }

  @Override
  public void sendTerminalType() throws UncheckedIOException {
    send(factory.getTerminalIsMushclient());

  }

  private void rememberCommandAlreadyEstablished(TelnetCommandWithOption cmd) {
    int code = cmd.getCode();
    int option = cmd.getOption();
    //WILL=251 WONT = 252 DO = 253 DONT = 254
    if (code < WILL || code > DONT) {
      throw new IllegalArgumentException(code + "");
    }
    if (option < 0 || option > 255) {
      throw new IllegalArgumentException(option + "");
    }
    Map<Integer, Boolean> lastStatusForOption = alreadyEstablished.get(option);
    if (lastStatusForOption == null) {
      alreadyEstablished.put(option, lastStatusForOption = new TreeMap<>(OPTIONS_NOT_NEGOTIATED));
      lastStatusForOption.put(code, true);// already established
    } else {
      boolean alreadyEstablished = lastStatusForOption.get(code);
      if (!alreadyEstablished) {
        // sblocco l'opposta
        lastStatusForOption.put(code, true);
        lastStatusForOption.put(negateCommand(code), false);//will
      }

    }

  }

  private boolean isCommandAlreadyEstablished(TelnetCommandWithOption data) {
    return alreadyEstablished.getOrDefault(data.getOption(), OPTIONS_NOT_NEGOTIATED).get(data.getCode());
  }

  @Override
  public boolean isServerOptionActive(int option) {
    return serverOptionsActive.contains(option);
  }

  @Override
  public boolean isClientOptionActive(int option) {
    return clientOptionsActive.contains(option);
  }

  @Override
  public void sendGMCPHandshake() {
    for(TelnetGMCPData tc:factory.getGMCPHandshake()) {
      send(tc);
    }
    
  }

 

}
