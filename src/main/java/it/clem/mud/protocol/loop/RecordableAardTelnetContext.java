package it.clem.mud.protocol.loop;

import java.io.UncheckedIOException;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetCommandWithOption;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetPositiveCommandWithOption;
import it.clem.mud.shared.recording.ServerRecordProducer;

public class RecordableAardTelnetContext implements AardTelnetContext {
  private final AardTelnetContext tc;
  private final ServerRecordProducer recordQueue;

  public RecordableAardTelnetContext(AardTelnetContext tc, ServerRecordProducer recordQueue) {
    super();
    this.tc = tc;
    this.recordQueue = recordQueue;
  }

  @Override
  public void fireServerErrorEvent(String message) {
    tc.fireServerErrorEvent(message);
  }

  @Override
  public void fireConnectionClosedByServerEvent() {
    tc.fireConnectionClosedByServerEvent();
  }

  @Override
  public void fireTextSentFromServerEvent(String text) {
    tc.fireTextSentFromServerEvent(text);
  }

  public void fireMapTagUpdated(String map) {
    tc.fireMapTagUpdated(map);
  }

  @Override
  public void sendGMCPCommand(String todo) throws UncheckedIOException {
    try {
      tc.sendGMCPCommand(todo);
    } finally {
      // TODO: recordQueue.
    }
  }

  @Override
  public void sendTerminalType() throws UncheckedIOException {
    try {
      tc.sendTerminalType();
    } finally {
      recordQueue.recordTerminalTypeSent();
    }
  }

  @Override
  public void replyWithConfirmationOf(TelnetCommandWithOption data) throws UncheckedIOException {
    try {
      tc.replyWithConfirmationOf(data);
    } finally {
      recordQueue.recordReplyWithConfirmationOf(data);
    }

  }

  @Override
  public void replyWithRefusalOf(TelnetPositiveCommandWithOption data) throws UncheckedIOException {
    try {
      tc.replyWithRefusalOf(data);
    } finally {
      recordQueue.replyWithRefusalOf(data);
    }
  }

  @Override
  public void ignore(TelnetData data) {
    try {
      tc.ignore(data);
    } finally {
      recordQueue.recordTelnetDataIgnored(data);
    }
  }

  @Override
  public void startCompression() {
    try {
      tc.startCompression();
    } finally {
      recordQueue.recordStartCompression();
    }
  }

  @Override
  public void stopCompression() {
    try {
      tc.stopCompression();
    } finally {
      recordQueue.recordStopCompression();
    }
  }

  @Override
  public void signalAsNotSupported(TelnetData data) {
    try {
      tc.signalAsNotSupported(data);
    } finally {
      recordQueue.recordSignalAsNotSupported(data);
    }
  }

  @Override
  public boolean isServerOptionActive(int option) {
    return tc.isServerOptionActive(option);
  }

  @Override
  public boolean isClientOptionActive(int option) {
    return tc.isClientOptionActive(option);
  }

  @Override
  public void sendGMCPHandshake() {
    tc.sendGMCPHandshake();
    recordQueue.recordGMCPHandshakeSent();
  }

}