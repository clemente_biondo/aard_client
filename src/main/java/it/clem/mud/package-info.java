/**
 *  Il progetto è composto da quattro thread e l'organizzazione in package ne rappresenta le relative competenze.
 *  Cio' al fine di minimizzare i bug legati alla concorrenza.
 *  <ul>
 *  <li> 
 *  <strong>Client</strong>: esegue la GUI ed è quindi responsabile dell'I/O con l'utente.
 *  Gioca il ruolo di producer rispetto al gedt che agisce da consumer tramite la GameEventQueue.
 *  Lo gedt comunica a sua volta al client tramite i metodi di ClientCommunicationStrategy.
 *  La modalità in cui ccs si rende thread safe dipende dal tipo di client, ad es. per swing  utilizza i meccanismi offerti dallo swingworker, mentre in console usa synchronized.
 *  </li>
 *  <li><strong>Protocol</strong>: dialoga con il server tramite SourceStream e TargetStream, due astrazioni di comodo di InputStream ed OutputStream.
 *  Come anche client, è anch'esso un producer per la GameEventQueue di gedt. Lo gedt può in autonomia inviare output al server, quindi entrambi i thread insistono su TelnetSender che sincronizza l'accesso ad TargetStream.
 *  </li>  
 *  <li><strong>GEDT</strong>:Il consumer degli eventi di gioco pubblicati da client e protocol. La sua reponsabilità è notificare gli eventi ai listener che possono reagire tramite il gamecontext (inviare comandi al server, oppure output di varia natura al client)
 *  </li>
 *  <li><strong>Logger</strong>: registra:<ul>
 *  <li>La coda degli eventi, in modo da poterne fare il replay in test e per mettere a puntoi nuovi listeners.</li> 
 *  <li>Lo scambio di I/O con il server, con il client e gli eventi prodotti , tutto con i relativi timing, in modo da poterne fare il replay. Da questo log possono essere generati una serie di files derivati in modo da potere effettuare una analisi del traffico dati.</li>
 *  Per non appesantire le varia attività Logger è un thread autonomo che riceve dagli altri tre thread informazioni tramite una coda specifica. Il logger viene creato alla connessione e terminato alla chiusura.
 *  </ul>
 *  </li>  
 *  </ul>
 *   
 */
package it.clem.mud;