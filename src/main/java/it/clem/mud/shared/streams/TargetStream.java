package it.clem.mud.shared.streams;

import java.io.Flushable;
import java.io.IOException;

public interface TargetStream extends AutoCloseable,Flushable {
  void close() throws IOException;
  void write(byte b[]) throws IOException;
}
