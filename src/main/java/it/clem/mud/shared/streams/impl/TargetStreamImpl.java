package it.clem.mud.shared.streams.impl;

import java.io.IOException;
import java.io.OutputStream;

import it.clem.mud.shared.streams.TargetStream;

public class TargetStreamImpl implements TargetStream {
  private final OutputStream out;

  public TargetStreamImpl(OutputStream out) {
    super();
    this.out = out;
  }

  @Override
  public void write(byte[] b) throws IOException {
    out.write(b);
  }

  @Override
  public void flush() throws IOException {
    out.flush();
  }

  @Override
  public void close() throws IOException {
    out.close();
  }

}
