package it.clem.mud.shared.streams.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.recorder.RecorderThread;
import it.clem.mud.shared.streams.SourceStream;

public class RecordableSourceStream implements SourceStream {
  private final SourceStream ss;
  private final OutputStream olog;
  private final static Logger log = LoggerFactory.getLogger(RecordableSourceStream.class);

  public RecordableSourceStream(SourceStream ss, File recordingDirectory) throws FileNotFoundException {
    super();
    this.ss = ss;
    this.olog = new FileOutputStream(new File(recordingDirectory, RecorderThread.INPUT_FILE));
  }

  @Override
  public void close() throws IOException {
    try {
      ss.close();
    } finally {
      try {
        olog.close();
      } catch (Exception e) {
        log.error(e.getMessage(), e);
      }
    }
  }

  @Override
  public void startCompression() {
    ss.startCompression();
  }

  @Override
  public void stopCompression() {
    ss.stopCompression();
  }

  @Override
  public int read() throws IOException {
    int b = -1;
    try {
      b = ss.read();
      return b;
    } finally {
      if (b > -1) {
        try {
          olog.write(b);
        } catch (Exception e) {
          log.error(e.getMessage(), e);
        }
      }

    }
  }

}
