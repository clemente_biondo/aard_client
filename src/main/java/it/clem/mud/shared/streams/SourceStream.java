package it.clem.mud.shared.streams;

import java.io.IOException;

public interface SourceStream extends AutoCloseable {
  int READ_TIMEOUT_FLAG = -2;
  int STREAM_CLOSED     = -1;

  void close() throws IOException;

  void startCompression();

  void stopCompression();

  int read() throws IOException;
}
