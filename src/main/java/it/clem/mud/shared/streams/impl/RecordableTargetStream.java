package it.clem.mud.shared.streams.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.recorder.RecorderThread;
import it.clem.mud.shared.streams.TargetStream;

public class RecordableTargetStream implements TargetStream {
  private final TargetStream ts;
  private final OutputStream olog;
  private final static Logger log = LoggerFactory.getLogger(RecordableTargetStream.class);

  public RecordableTargetStream(TargetStream ts, File recordingDirectory) throws FileNotFoundException {
    super();
    this.ts = ts;
    this.olog = new FileOutputStream(new File(recordingDirectory, RecorderThread.OUTPUT_FILE));
  }

  @Override
  public void close() throws IOException {
    ts.close();
  }

  @Override
  public void write(byte[] b) throws IOException {
    try {
      ts.write(b);
    } finally {
      try {
        olog.write(b);
      } catch (Exception e) {
        log.error(e.getMessage(), e);
      }
    }
  }

  @Override
  public void flush() throws IOException {
    try {
      ts.flush();
    } finally {
      try {
        olog.flush();
      } catch (Exception e) {
        log.error(e.getMessage(), e);
      }
    }
  }

}
