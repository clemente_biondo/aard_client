package it.clem.mud.shared.streams.impl;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.util.zip.InflaterInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.shared.streams.SourceStream;

public class SourceStreamImpl implements SourceStream {
  private final InputStream isNonCompressed;
  private final InputStream isCompressed;
  private volatile InputStream activeInputStream;
  private final static Logger log = LoggerFactory.getLogger(SourceStreamImpl.class);

  public SourceStreamImpl(InputStream isNonCompressed) {
    super();
    this.activeInputStream = this.isNonCompressed = isNonCompressed;
    this.isCompressed = new InflaterInputStream(isNonCompressed);
  }

  @Override
  public void close() throws IOException {
    this.isCompressed.close();
  }

  /**
   * @return -2 se in timeout
   * @throws IOException
   */
  @Override
  public int read() throws IOException {
    try {
      return activeInputStream.read();
    } catch (SocketTimeoutException stex) {
      return -2;
    } catch (EOFException eof) {
      return -1;
    } catch (Exception stex) {
      log.error(stex.getMessage(), stex);
      return -1;
    }
  }

  @Override
  public void startCompression() {
    this.activeInputStream = this.isCompressed;
  }

  @Override
  public void stopCompression() {
    this.activeInputStream = this.isNonCompressed;
  }

}
