package it.clem.mud.shared.view.console;

import javax.annotation.concurrent.ThreadSafe;

import it.clem.mud.shared.view.ClientCommunicationStrategy;

@ThreadSafe
public class ConsoleClientCommunicationStrategy implements ClientCommunicationStrategy {

  @Override
  public void sendTextToClient(String text) {
    synchronized(System.out){
      System.out.print(text);  
    }
  }

  @Override
  public void notifyServerError(String error) {
    synchronized(System.err){
      System.err.print(error);
    }
  }

  @Override
  public void notifyServerStarted() {
    System.out.print("server started");  
    
  }

  @Override
  public void notifyServerClosed() {
    System.out.print("server closed");  
  }

  @Override
  public void updateMapTag(String map) {
    synchronized(System.out){
      System.out.print(map);  
    }
  }

}
