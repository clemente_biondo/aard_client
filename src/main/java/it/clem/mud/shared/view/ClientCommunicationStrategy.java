package it.clem.mud.shared.view;

import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public interface ClientCommunicationStrategy {
  void sendTextToClient(String text);
  void updateMapTag(String map);
  //TODO: server error non va bene, devo sapere se siamo connessi oppure no, faccio due eventi 
  //oppure aggiungo un metodo qua?
  void notifyServerError(String error);
  void notifyServerStarted();
  void notifyServerClosed();
}
