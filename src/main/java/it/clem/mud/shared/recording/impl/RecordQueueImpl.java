package it.clem.mud.shared.recording.impl;

import java.util.concurrent.BlockingQueue;

import javax.annotation.concurrent.ThreadSafe;

import it.clem.mud.protocol.data.TelnetCommandWithOption;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetPositiveCommandWithOption;
import it.clem.mud.recorder.Record;
import it.clem.mud.recorder.RecordType;
import it.clem.mud.shared.game.event.GameEvent;
import it.clem.mud.shared.recording.RecordQueue;

@ThreadSafe
public class RecordQueueImpl implements RecordQueue{
  private final BlockingQueue<Record> queue;

  public RecordQueueImpl(BlockingQueue<Record> queue) {
    super();
    this.queue = queue;
  }

  @Override
  public void recordGameEvent(GameEvent event) {
    queue.add(new Record(RecordType.GameEvent, event));
  }

  @Override
  public void recordTelnetDataIgnored(TelnetData data) {
    queue.add(new Record(RecordType.TelnetDataIgnored, data));
  }

  @Override
  public void recordTelnetDataSent(TelnetData data) {
    queue.add(new Record(RecordType.TelnetDataSent, data));
  }

  @Override
  public void recordTelnetDataReceived(TelnetData data) {
    queue.add(new Record(RecordType.TelnetDataReceived, data));
  }

  @Override
  public void recordStartCompression() {
    queue.add(new Record(RecordType.CompressionStarted, null));
  }

  @Override
  public void recordStopCompression() {
    queue.add(new Record(RecordType.CompressionStopped, null));
  }

  @Override
  public void recordSignalAsNotSupported(TelnetData data) {
    queue.add(new Record(RecordType.TelnetDataNotSupported, data));
  }

  @Override
  public Record take() throws InterruptedException {
    return queue.take();
  }

  @Override
  public void stopRecording() {
    queue.add(new Record(RecordType.StopRecordingSession, null));
  }

  @Override
  public void recordTerminalTypeSent() {
    queue.add(new Record(RecordType.TerminalTypeSent, null));
  }
  
  @Override
  public void recordGMCPHandshakeSent() {
    queue.add(new Record(RecordType.GMCPHandshakeSent, null));
  }

  @Override
  public void recordReplyWithConfirmationOf(TelnetCommandWithOption data) {
    queue.add(new Record(RecordType.ReplyWithConfirmationOf, data));
  }

  @Override
  public void replyWithRefusalOf(TelnetPositiveCommandWithOption data) {
    queue.add(new Record(RecordType.ReplyWithRefusalOf, data));
  }
  
}
