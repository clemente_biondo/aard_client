package it.clem.mud.shared.recording;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.shared.game.event.GameEvent;

public interface GameEventThreadDispatcherProducer {
  void recordGameEvent(GameEvent event);
  void stopRecording();
  void recordTelnetDataSent(TelnetData data);
}
