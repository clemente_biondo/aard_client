package it.clem.mud.shared.recording;

import it.clem.mud.protocol.data.TelnetCommandWithOption;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetPositiveCommandWithOption;

public interface ServerRecordProducer {
  void recordTelnetDataIgnored(TelnetData data);
  void recordTelnetDataReceived(TelnetData data);
  void recordStartCompression();
  void recordStopCompression();
  void recordSignalAsNotSupported(TelnetData data);
  void recordTerminalTypeSent();
  void recordReplyWithConfirmationOf(TelnetCommandWithOption data);
  void replyWithRefusalOf(TelnetPositiveCommandWithOption data);
  void recordGMCPHandshakeSent();
}
