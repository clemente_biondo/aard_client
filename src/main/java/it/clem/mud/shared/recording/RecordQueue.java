package it.clem.mud.shared.recording;

import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public interface RecordQueue extends ServerRecordProducer, ClientRecordProducer,GameEventThreadDispatcherProducer,RecordConsumer{
   
}
