package it.clem.mud.shared.recording;

import it.clem.mud.recorder.Record;

public interface RecordConsumer {
  Record take() throws InterruptedException;
}
