package it.clem.mud.shared.game.event.queue;

import it.clem.mud.shared.game.event.GameEvent;

/**
 * Tramite questo metodo bloccante, il thread che esegue il game loop può state in attesa di eventi per poi elaborarli serialmente non appena si verificano.
 * 
 *
 */
public interface GameEventConsumer {
  GameEvent take() throws InterruptedException;
}
