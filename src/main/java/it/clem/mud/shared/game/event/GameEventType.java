package it.clem.mud.shared.game.event;

import java.io.Serializable;

public enum GameEventType implements Serializable {
  //@formatter:off
  ConnectionClosedByServer(true,EventSource.Server),
  DisconnectByClient(true,EventSource.Client),
  ServerError(true,EventSource.Server), 
  MapTagUpdatedFromServer(false,EventSource.Server),
  TextSentFromServer(false,EventSource.Server), 
  TextSentFromClient(false,EventSource.Client),
  GMCPRequestFromClient(false,EventSource.Client);
  //@formatter:on

  private final boolean     endsGameLoop;
  private final EventSource source;

  private GameEventType(boolean endsGameLoop, EventSource source) {
    this.endsGameLoop = endsGameLoop;
    this.source = source;
  }

  public EventSource getSource() {
    return this.source;
  }

  public boolean endsGameLoop() {
    return endsGameLoop;
  }

}
