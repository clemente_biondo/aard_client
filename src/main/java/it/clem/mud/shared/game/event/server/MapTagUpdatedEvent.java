package it.clem.mud.shared.game.event.server;

import it.clem.mud.shared.game.event.GameEventImpl;
import it.clem.mud.shared.game.event.GameEventType;

public class MapTagUpdatedEvent extends GameEventImpl {
  private static final long serialVersionUID = 1L;
  private final String      map;

  public MapTagUpdatedEvent(int seqId, String map) {
    super(seqId, GameEventType.MapTagUpdatedFromServer);
    this.map = map;
  }

  public String getMap() {
    return map;
  }

  @Override
  public String toString() {
    return "map:" + map + ", " + super.toString();
  }

}
