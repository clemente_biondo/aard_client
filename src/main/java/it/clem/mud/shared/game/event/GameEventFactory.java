package it.clem.mud.shared.game.event;

import javax.annotation.concurrent.ThreadSafe;

import it.clem.mud.shared.game.event.client.DisconnectByClientEvent;
import it.clem.mud.shared.game.event.client.GMCPRequestType;
import it.clem.mud.shared.game.event.client.RequestGMCPDataEvent;
import it.clem.mud.shared.game.event.client.TextSentFromClientEvent;
import it.clem.mud.shared.game.event.server.ConnectionClosedByServerEvent;
import it.clem.mud.shared.game.event.server.MapTagUpdatedEvent;
import it.clem.mud.shared.game.event.server.ServerErrorEvent;
import it.clem.mud.shared.game.event.server.TextSentFromServerEvent;

//TODO:  sdoppiare eventi e factory tra client e server
@ThreadSafe
public interface GameEventFactory {
  ServerErrorEvent getServerErrorEvent(String message);

  ConnectionClosedByServerEvent getConnectionClosedByServerEvent();

  TextSentFromClientEvent getTextSentFromClientEvent(String text);

  TextSentFromServerEvent getTextSentFromServerEvent(String text);
  
  DisconnectByClientEvent  getDisconnectByClientEvent();

  MapTagUpdatedEvent getMapTagUpdatedEvent(String map);
  
  RequestGMCPDataEvent getGMCPRequestByType(GMCPRequestType requestType);
}
