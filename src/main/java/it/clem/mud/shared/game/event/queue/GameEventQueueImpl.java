package it.clem.mud.shared.game.event.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.concurrent.ThreadSafe;

import it.clem.mud.shared.game.event.GameEvent;
import it.clem.mud.shared.game.event.GameEventFactory;
import it.clem.mud.shared.game.event.GameEventFactoryImpl;
import it.clem.mud.shared.game.event.client.GMCPRequestType;

@ThreadSafe
public class GameEventQueueImpl implements GameEventQueue {
  private final BlockingQueue<GameEvent> eventQueue;
  private final GameEventFactory         gameEventFactory;

  public GameEventQueueImpl(BlockingQueue<GameEvent> eventQueue, GameEventFactory gameEventFactory) {
    super();
    this.eventQueue = eventQueue;
    this.gameEventFactory = gameEventFactory;
  }

  @Override
  public GameEvent take() throws InterruptedException {
    return eventQueue.take();
  }

  @Override
  public void fireTextSentFromClientEvent(String text) {
    eventQueue.add(gameEventFactory.getTextSentFromClientEvent(text));
  }

  @Override
  public void fireServerErrorEvent(String message) {
    eventQueue.add(gameEventFactory.getServerErrorEvent(message));

  }

  @Override
  public void fireConnectionClosedByServerEvent() {
    eventQueue.add(gameEventFactory.getConnectionClosedByServerEvent());
  }

  @Override
  public void fireTextSentFromServerEvent(String text) {
    eventQueue.add(gameEventFactory.getTextSentFromServerEvent(text));
  }

  @Override
  public void fireMapTagUpdated(String map) {
    eventQueue.add(gameEventFactory.getMapTagUpdatedEvent(map));
  }

  @Override
  public void fireDisconnectByClientEvent() {
    eventQueue.add(gameEventFactory.getDisconnectByClientEvent());
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private BlockingQueue<GameEvent> eventQueue;
    private GameEventFactory         gameEventFactory;

    public Builder setEventQueue(BlockingQueue<GameEvent> eventQueue) {
      this.eventQueue = eventQueue;
      return this;
    }

    public Builder setGameEventFactory(GameEventFactory gameEventFactory) {
      this.gameEventFactory = gameEventFactory;
      return this;
    }

    public GameEventQueue build() {
      return new GameEventQueueImpl(eventQueue != null ? eventQueue : new LinkedBlockingQueue<GameEvent>(),
          gameEventFactory != null ? gameEventFactory : new GameEventFactoryImpl());
    }
  }

  @Override
  public void GMCPRefreshAll() {
    GMCPRequestType[] arr = GMCPRequestType.values();
    for (int i = 0; i < arr.length; i++) {
      eventQueue.add(gameEventFactory.getGMCPRequestByType(arr[i]));
    }
  }

  @Override
  public void GMCPRequestArea() {
    eventQueue.add(gameEventFactory.getGMCPRequestByType(GMCPRequestType.Area));
  }

  @Override
  public void GMCPRequestChar() {
    eventQueue.add(gameEventFactory.getGMCPRequestByType(GMCPRequestType.Character));
  }

  @Override
  public void GMCPRequestQuest() {
    eventQueue.add(gameEventFactory.getGMCPRequestByType(GMCPRequestType.Quest));
  }

  @Override
  public void GMCPRequestRoom() {
    eventQueue.add(gameEventFactory.getGMCPRequestByType(GMCPRequestType.Room));
  }

  @Override
  public void GMCPRequestSectors() {
    eventQueue.add(gameEventFactory.getGMCPRequestByType(GMCPRequestType.Sectors));
  }

}