package it.clem.mud.shared.game.event.client;

import javax.annotation.concurrent.Immutable;

import it.clem.mud.shared.game.event.GameEventImpl;
import it.clem.mud.shared.game.event.GameEventType;

@Immutable
public class DisconnectByClientEvent  extends GameEventImpl {
    private static final long serialVersionUID = 1L;
    
    public DisconnectByClientEvent(int seqId) {
      super(seqId,GameEventType.DisconnectByClient);
    }

  }
