package it.clem.mud.shared.game.event;

/**
 * Il thread/sottosistema che ha originato l'evento.
 * Client corrisponde alla UI
 * Server corrisponde al receiver di dati telnet emessi dal server.
 * Derived sono eventi che scaturiscono nel thread che esegue il game loop a seguito di altri eventi.
 * In tutti i casi gli eventi sono consumenti dal thread che esegue il game loop.
 */
public enum EventSource {
  Client,Server
  
}
