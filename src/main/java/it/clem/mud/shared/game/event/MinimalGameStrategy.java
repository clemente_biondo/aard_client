package it.clem.mud.shared.game.event;

import it.clem.mud.gedt.event.bus.GameEventBus;
import it.clem.mud.gedt.event.bus.impl.GameEventBusImpl;

public class MinimalGameStrategy {
  private final static GameEventBus INSTANCE =
      GameEventBusImpl.builder()
      .addNonConsumableTextSentFromServerEventListener((ev,gc)->gc.sendTextToClient(ev.getText()))
      .addNonConsumableTextSentFromClientEventListener((ev,gc)->gc.sendTextToServer(ev.getText()))
      .addNonConsumableServerErrorEventListener((ev,gc)->gc.notifyServerError(ev.getErrorMessage()))
      .build();
      
  public static GameEventBus getMinimalGameStrategy () {
    return INSTANCE;
  }


}
