package it.clem.mud.shared.game;

public enum GameStrategy {
  None,//non fa nulla
  Minimal,//invia l'input del server al client e l'input del client al server senza ulteriori manipolazioni
  Passive,//legge solo i dati ma non agisce attivamente
  ActiveStepped//agisce attivamente ma ogni passo /decisione è triggerato da un click utente 
}
