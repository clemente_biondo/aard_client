package it.clem.mud.shared.game;

import java.io.IOException;
import java.io.UncheckedIOException;
import javax.annotation.concurrent.ThreadSafe;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.shared.game.event.client.GMCPRequestType;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;
import it.clem.mud.shared.view.ClientCommunicationStrategy;

@ThreadSafe
public class GameContextImpl implements GameContext {
	private final TelnetWriter writer;
	private final TelnetDataFactory telnetDataFactory;
	private final ClientCommunicationStrategy clientStrategy;
	
	public GameContextImpl(TelnetDataFactory telnetDataFactory,TelnetWriter writer,ClientCommunicationStrategy clientStrategy) {
		super();
		this.writer=writer;
		this.clientStrategy=clientStrategy;
		this.telnetDataFactory=telnetDataFactory;
	}

	@Override
	public void sendTextToServer(String text) {
	  String textToSend=text.replaceAll("[\r \n\t]+$", "")+"\r\n";
	  sendDataToServer(telnetDataFactory.getTelnetText(textToSend));
	}
	
	private void sendDataToServer(TelnetData data) {
    try {
      this.writer.write(data);
    } catch (IOException e) {
      throw new UncheckedIOException(e.getMessage(),e);
    }
  }
	
	@Override
  public void sendTextToClient(String text) {
    clientStrategy.sendTextToClient(text);
  }

	@Override
  public void notifyServerError(String error) {
    clientStrategy.notifyServerError(error);
  }

  public void notifyServerStarted() {
    clientStrategy.notifyServerStarted();
  }

  public void notifyServerClosed() {
    clientStrategy.notifyServerClosed();
  }

  public void updateMapTag(String map) {
    clientStrategy.updateMapTag(map);
  }

  @Override
  public void sendGMCPRequestToServer(GMCPRequestType gmcpRequestType) {
    sendDataToServer(telnetDataFactory.getTelnetGMCPClientRequest(gmcpRequestType));
  }

}
