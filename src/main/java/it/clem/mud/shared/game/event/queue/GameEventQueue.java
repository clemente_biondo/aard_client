package it.clem.mud.shared.game.event.queue;

/**
 * La coda degli eventi del gioco. Tre thread inseriscono eventi (Vedi Eventsource) ma solo uno li elabora( il game loop)
 * 
 *
 */
public interface GameEventQueue extends ServerEventProducer, ClientEventProducer, GameEventConsumer{

}
