package it.clem.mud.shared.game.event;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.annotation.concurrent.Immutable;

@Immutable
public interface GameEvent extends Serializable{

  LocalDateTime getCreationTimestamp();
  
  GameEventType getType();

  EventSource getSource();

  boolean endsGameLoop();
  
  int getSeqId();

 // void notify(GameContext gc);

}