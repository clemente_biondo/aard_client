package it.clem.mud.shared.game.event.server;


import javax.annotation.concurrent.Immutable;

import it.clem.mud.shared.game.event.GameEventImpl;
import it.clem.mud.shared.game.event.GameEventType;

/**
 * 
 * Evento sollevato dal telnet thread per indicare che il thread è stato terminato dal server "gracefully"
 *
 */
@Immutable
public final class ConnectionClosedByServerEvent extends GameEventImpl {
  private static final long serialVersionUID = 1L;
  
  public ConnectionClosedByServerEvent(int seqId) {
    super(seqId,GameEventType.ConnectionClosedByServer);
  }

}
