package it.clem.mud.shared.game;

import javax.annotation.concurrent.ThreadSafe;

import it.clem.mud.shared.game.event.client.GMCPRequestType;
import it.clem.mud.shared.view.ClientCommunicationStrategy;

/**
 * Consente ai listener di reagire agli eventi.
 * 
 *
 */
@ThreadSafe
public interface GameContext extends ClientCommunicationStrategy{
  void sendTextToServer(String text);

  void sendGMCPRequestToServer(GMCPRequestType gmcpRequestType);
}
