package it.clem.mud.shared.game.event.server;

import javax.annotation.concurrent.Immutable;

import it.clem.mud.shared.game.event.GameEventImpl;
import it.clem.mud.shared.game.event.GameEventType;

/**
 * 
 * Evento sollevato dal telnet thread per indicare che il thread è terminato a seguito di un errore del server
 *
 */
@Immutable
public class ServerErrorEvent extends GameEventImpl{
  private static final long serialVersionUID = 1L;
  private final String      errorMessage;

  public ServerErrorEvent(int seqId,String errorMessage) {
    super(seqId,GameEventType.ServerError);
    this.errorMessage = errorMessage;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  @Override
  public String toString() {
    return "errorMessage:" + errorMessage + ", "+super.toString();
  }

}
