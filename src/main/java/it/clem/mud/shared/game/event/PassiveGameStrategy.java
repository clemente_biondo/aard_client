package it.clem.mud.shared.game.event;

import it.clem.mud.gedt.event.bus.GameEventBus;
import it.clem.mud.gedt.event.bus.impl.GameEventBusImpl;

public class PassiveGameStrategy {
  private final static GameEventBus INSTANCE =
      GameEventBusImpl.builder()
      .addNonConsumableRequestGMCPDataEventListener((ev,gc)->gc.sendGMCPRequestToServer(ev.getGMCPRequestType()))
      .addNonConsumableMapTagUpdatedEventListener((ev,gc)->gc.updateMapTag(ev.getMap()))
      .addNonConsumableTextSentFromServerEventListener((ev,gc)->gc.sendTextToClient(ev.getText()))
      .addNonConsumableTextSentFromClientEventListener((ev,gc)->gc.sendTextToServer(ev.getText()))
      .addNonConsumableServerErrorEventListener((ev,gc)->gc.notifyServerError(ev.getErrorMessage()))
      .build();
      
  public static GameEventBus getPassiveGameStrategy () {
    return INSTANCE;
  }


}
