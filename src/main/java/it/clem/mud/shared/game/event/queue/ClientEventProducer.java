package it.clem.mud.shared.game.event.queue;

/**
 * Interfaccia utilizzata dal thread che controlla la User Interface per inoltrare le azioni utente che hanno importanza per il resto del sistema. 
 * Come per gli altri due casi (vedi EventSource) il destinatario di questi eventi è sempre il thread che esegue il game loop. 
 *
 */
public interface ClientEventProducer {
  void fireTextSentFromClientEvent(String text);
  void fireDisconnectByClientEvent();
  void GMCPRefreshAll();
  void GMCPRequestArea();
  void GMCPRequestChar();
  void GMCPRequestQuest();
  void GMCPRequestRoom();
  void GMCPRequestSectors();
}
