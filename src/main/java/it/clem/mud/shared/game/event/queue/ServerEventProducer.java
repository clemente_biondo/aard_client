package it.clem.mud.shared.game.event.queue;

/**
 * Tramite questa interfaccia il thread che ha la reference al server reciever puo' notificare gli eventi che scaturiscono dalla lettura del traffico inbound
 * dal server al thread che esegue il game loop.
 * Ogni metodo di questa interfaccia è una informazione che arriva dal server e necessita di essere notificata al resto del sistema. 
 *
 */
public interface ServerEventProducer {

  void fireServerErrorEvent(String message);

  void fireConnectionClosedByServerEvent();

  void fireTextSentFromServerEvent(String text);
  
  void fireMapTagUpdated(String map);
  
  void fireGMCPTick();
}
