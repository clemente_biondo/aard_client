package it.clem.mud.shared.game.event;

import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.concurrent.Immutable;

import it.clem.mud.shared.game.event.client.DisconnectByClientEvent;
import it.clem.mud.shared.game.event.client.GMCPRequestType;
import it.clem.mud.shared.game.event.client.RequestGMCPDataEvent;
import it.clem.mud.shared.game.event.client.TextSentFromClientEvent;
import it.clem.mud.shared.game.event.server.ConnectionClosedByServerEvent;
import it.clem.mud.shared.game.event.server.MapTagUpdatedEvent;
import it.clem.mud.shared.game.event.server.ServerErrorEvent;
import it.clem.mud.shared.game.event.server.TextSentFromServerEvent;

//TODO: separare la factory in eventi serverside ed eventi client side?
@Immutable
public class GameEventFactoryImpl implements GameEventFactory {
  private final AtomicInteger sequenceId= new AtomicInteger();

  public GameEventFactoryImpl() {
    super();
  }

  @Override
  public ServerErrorEvent getServerErrorEvent(String errorMessage) {
    return new ServerErrorEvent(sequenceId.incrementAndGet(),errorMessage);
  }

  @Override
  public ConnectionClosedByServerEvent getConnectionClosedByServerEvent() {
    return new ConnectionClosedByServerEvent(sequenceId.incrementAndGet());
  }

  @Override
  public TextSentFromClientEvent getTextSentFromClientEvent(String text) {
    return new TextSentFromClientEvent(sequenceId.incrementAndGet(),text);
  }

  @Override
  public TextSentFromServerEvent getTextSentFromServerEvent(String text) {
    return new TextSentFromServerEvent(sequenceId.incrementAndGet(),text);
  }

  @Override
  public DisconnectByClientEvent getDisconnectByClientEvent() {
    return new DisconnectByClientEvent(sequenceId.incrementAndGet());
  }

  @Override
  public MapTagUpdatedEvent getMapTagUpdatedEvent(String map) {
    return new MapTagUpdatedEvent(sequenceId.incrementAndGet(),map);
  }

  @Override
  public RequestGMCPDataEvent getGMCPRequestByType(GMCPRequestType requestType) {
    return new RequestGMCPDataEvent(sequenceId.incrementAndGet(),requestType);
  }

//  public static GameEventFactory getGameEventFactory(GameEventSubscriber subscriber){
//    return new GameEventFactoryImpl(GameEventTypeListenersFactoryImpl.builder().subscribe(subscriber).build());
//  }
  
//  public static Builder builder(){
//    return new Builder();
//  }
//  
//  public static class Builder{
//    private final GameEventSubscriber subscriber = new GameEventSubscriber();
//    
//    public Builder register(GameEventListener listener) {
//      subscriber.register(listener);
//      return this;
//    }
//
//    public GameEventFactory build(){
//      return getGameEventFactory(subscriber);
//    }
//  }
}
