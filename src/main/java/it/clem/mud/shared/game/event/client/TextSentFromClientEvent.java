package it.clem.mud.shared.game.event.client;

import javax.annotation.concurrent.Immutable;

import it.clem.mud.shared.game.event.GameEventImpl;
import it.clem.mud.shared.game.event.GameEventType;

@Immutable
public class TextSentFromClientEvent extends GameEventImpl {
  private static final long serialVersionUID = 1L;
  private final String      text;

  public TextSentFromClientEvent(int seqId,String text) {
    super(seqId,GameEventType.TextSentFromClient);
    this.text = text;
  }

  public String getText() {
    return text;
  }

  @Override
  public String toString() {
    return "text:" + text + ", "+super.toString();
  }

}
