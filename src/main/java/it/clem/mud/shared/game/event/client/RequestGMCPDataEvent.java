package it.clem.mud.shared.game.event.client;

import it.clem.mud.shared.game.event.GameEventImpl;
import it.clem.mud.shared.game.event.GameEventType;

public class RequestGMCPDataEvent extends GameEventImpl{
   /**
   * 
   */
  private static final long serialVersionUID = -5833187196038567323L;
  private  final GMCPRequestType requestType;

  public RequestGMCPDataEvent(int seqId,GMCPRequestType requestType) {
    super(seqId,GameEventType.GMCPRequestFromClient);
    this.requestType = requestType;
  }

  public GMCPRequestType getGMCPRequestType() {
    return requestType;
  }

  @Override
  public String toString() {
    return  requestType + ", "+super.toString();
  }
}
