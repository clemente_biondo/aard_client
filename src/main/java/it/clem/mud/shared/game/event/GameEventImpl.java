package it.clem.mud.shared.game.event;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.concurrent.Immutable;


@Immutable
public class GameEventImpl implements GameEvent {

	private static final long serialVersionUID = 1L;
	private final LocalDateTime creationTimestamp = LocalDateTime.now();
	//TODO: se voglio veramente serializzare gli eventi per farne il replay
	//il type va agganciato quando riattivo l'oggetto oppure in alternativa devo passivare
	//anche tutti i listener ed il relativo stato (!!!!). Oppure ancora separo l'evento dal tipo
	//ed in fase di notifica li riassocio con una immutable map basata sul name del tipo 
	//private final GameEventType<T> type;
  private final GameEventType type;
  private final int seqId;  
	
  public GameEventImpl(int seqId,GameEventType type) {
		super();
		this.type = type;
		this.seqId=seqId;
	}

	/* (non-Javadoc)
   * @see it.clem.mud.game.events.impl.GameEvent#getCreationTimestamp()
   */
	@Override
  public LocalDateTime getCreationTimestamp() {
		return creationTimestamp;
	}
	
  @Override
  public GameEventType getType() {
    return type;
  }

	
	/* (non-Javadoc)
   * @see it.clem.mud.game.events.impl.GameEvent#getSource()
   */
	@Override
  public EventSource getSource() {
    return type.getSource();
  }

  /* (non-Javadoc)
   * @see it.clem.mud.game.events.impl.GameEvent#endsGameLoop()
   */
  @Override
  public boolean endsGameLoop() {
		return type.endsGameLoop();
	}

//
//
//	/* (non-Javadoc)
//   * @see it.clem.mud.game.events.impl.GameEvent#notify(it.clem.mud.game.loop.GameContext)
//   */
//	@Override
//  @SuppressWarnings("unchecked")
//	public void notify(GameContext gc) {
//		type.notifyEvent((T) this, gc);
//	}

  @Override
  public String toString() {
    return "at "+ creationTimestamp.format(DateTimeFormatter.ISO_DATE_TIME) + ", type=" + type ;
  }

  @Override
  public int getSeqId() {
    return seqId;
  }

}
