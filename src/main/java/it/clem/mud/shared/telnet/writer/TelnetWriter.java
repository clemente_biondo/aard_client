package it.clem.mud.shared.telnet.writer;

import java.io.IOException;

import javax.annotation.concurrent.ThreadSafe;

import it.clem.mud.protocol.data.TelnetData;

@ThreadSafe
public interface TelnetWriter extends AutoCloseable/*,Flushable*/{
	void write(TelnetData data) throws IOException;
	void close() throws IOException;
}
