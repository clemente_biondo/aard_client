package it.clem.mud.shared.telnet;

import static it.clem.mud.protocol.TelnetProtocolResources.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.concurrent.ThreadSafe;

import org.apache.commons.lang3.exception.ContextedRuntimeException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import it.clem.mud.protocol.data.impl.TelnetCommandImpl;
import it.clem.mud.shared.game.event.client.GMCPRequestType;
import it.clem.mud.util.LangUtils;
import it.clem.mud.protocol.TelnetProtocolResources;
import it.clem.mud.protocol.data.TelnetCommand;
import it.clem.mud.protocol.data.TelnetCommandWithOption;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetDo;
import it.clem.mud.protocol.data.TelnetDont;
import it.clem.mud.protocol.data.TelnetGA;
import it.clem.mud.protocol.data.TelnetGMCPData;
import it.clem.mud.protocol.data.TelnetPositiveCommandWithOption;
import it.clem.mud.protocol.data.TelnetSubnegotiation;
import it.clem.mud.protocol.data.TelnetText;
import it.clem.mud.protocol.data.TelnetWill;
import it.clem.mud.protocol.data.TelnetWont;

@ThreadSafe
public class TelnetDataFactory {
  private final AtomicInteger sequenceId = new AtomicInteger(0);

  // //IAC SB CMP2 IAC SE
  // public final static TelnetSubnegotiation START_COMPRESSION_SUBNEG = new TelnetSubnegotiation(CMP2);
  // public final static TelnetSubnegotiation SEND_TERMINAL = new
  // TelnetSubnegotiation(TERMINAL_TYPE,TERMINAL_TYPE_SEND);
  // //MUSHclient-Aard
  // public final static TelnetSubnegotiation TERMINAL_IS_MUSHCLIENT = new
  // TelnetSubnegotiation(TERMINAL_TYPE,TERMINAL_TYPE_IS,77,85,83,72,99,108,105,101,110,116,45,65,97,114,100);

  public TelnetDataFactory() {
    super();
  }

  // public static void resetSequenceId(){
  // sequenceId.set(0);
  // }
  //
  public TelnetWill getWill(int option) {
    switch (option) {
    case ECHO:
      return new TelnetWill(null, sequenceId.incrementAndGet(), TelnetDataType.WILL_ECHO, option);
    case CMP2:
      return new TelnetWill(null, sequenceId.incrementAndGet(), TelnetDataType.WILL_CMP2, option);
    case CMP1:
      return new TelnetWill(null, sequenceId.incrementAndGet(), TelnetDataType.WILL_CMP1, option);
    case AARD:
      return new TelnetWill(null, sequenceId.incrementAndGet(), TelnetDataType.WILL_AARD, option);
    case ATCP:
      return new TelnetWill(null, sequenceId.incrementAndGet(), TelnetDataType.WILL_ATCP, option);
    case GMCP:
      return new TelnetWill(null, sequenceId.incrementAndGet(), TelnetDataType.WILL_GMCP, option);
    default:
      return new TelnetWill(null, sequenceId.incrementAndGet(), TelnetDataType.UNKNOWN_WILL, option);
    }
  }

  public TelnetGA getGA() {
    return new TelnetGA(sequenceId.incrementAndGet());
  }

  public TelnetDo getDo(int option) {
    switch (option) {
    case ECHO:
      return new TelnetDo(null, sequenceId.incrementAndGet(), TelnetDataType.DO_ECHO, option);
    case CMP2:
      return new TelnetDo(null, sequenceId.incrementAndGet(), TelnetDataType.DO_CMP2, option);
    case TERMINAL_TYPE:
      return new TelnetDo(null, sequenceId.incrementAndGet(), TelnetDataType.DO_TERMINAL_TYPE, option);
    case NAWS:
      return new TelnetDo(null, sequenceId.incrementAndGet(), TelnetDataType.DO_NAWS, option);
    case AARD:
      return new TelnetDo(null, sequenceId.incrementAndGet(), TelnetDataType.DO_AARD, option);
    default:
      return new TelnetDo(null, sequenceId.incrementAndGet(), TelnetDataType.UNKNOWN_DO, option);
    }
  }

  public TelnetDont getDont(int option) {
    switch (option) {
    case CMP2:
      return new TelnetDont(null, sequenceId.incrementAndGet(), TelnetDataType.DONT_CMP2, option);
    case CMP1:
      return new TelnetDont(null, sequenceId.incrementAndGet(), TelnetDataType.DONT_CMP1, option);
    case AARD:
      return new TelnetDont(null, sequenceId.incrementAndGet(), TelnetDataType.DONT_AARD, option);
    case ATCP:
      return new TelnetDont(null, sequenceId.incrementAndGet(), TelnetDataType.DONT_ATCP, option);
    case GMCP:
      return new TelnetDont(null, sequenceId.incrementAndGet(), TelnetDataType.DONT_GMCP, option);
    default:
      return new TelnetDont(null, sequenceId.incrementAndGet(), TelnetDataType.UNKNOWN_DONT, option);
    }
  }

  public TelnetWont getWont(int option) {
    switch (option) {
    case CMP2:
      return new TelnetWont(null, sequenceId.incrementAndGet(), TelnetDataType.WONT_CMP2, option);
    case TERMINAL_TYPE:
      return new TelnetWont(null, sequenceId.incrementAndGet(), TelnetDataType.WONT_TTY, option);
    case NAWS:
      return new TelnetWont(null, sequenceId.incrementAndGet(), TelnetDataType.WONT_NAWS, option);
    default:
      return new TelnetWont(null, sequenceId.incrementAndGet(), TelnetDataType.UNKNOWN_WONT, option);
    }
  }

  public TelnetCommand getCommand(int code) {
    switch (code) {
    case SE:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.SE_CMD, code);
    case NOP:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.NOP_CMD, code);
    case DM:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.DM_CMD, code);
    case BRK:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.BRK_CMD, code);
    case IP:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.IP_CMD, code);
    case AO:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.AO_CMD, code);
    case AYT:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.AYT_CMD, code);
    case EC:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.EC_CMD, code);
    case EL:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.EL_CMD, code);
    case GA:
      return getGA();
    case SB:
      return new TelnetCommandImpl(sequenceId.incrementAndGet(), TelnetDataType.SB_CMD, code);
    default:
      throw new ContextedRuntimeException("Codice non ammesso").addContextValue("code", code);

    }
  }

  public TelnetCommandWithOption getCommandWithOption(int code, int option) {
    switch (code) {
    case WILL:
      return getWill(option);
    case WONT:
      return getWont(option);
    case DO:
      return getDo(option);
    case DONT:
      return getDont(option);
    default:
      throw new ContextedRuntimeException("Codice non ammesso").addContextValue("code", code).addContextValue("option",
          option);
    }
  }

  public TelnetSubnegotiation getSubnegotionation(int option, byte... content) {
    if (option == GMCP) {
      return new TelnetGMCPData(sequenceId.incrementAndGet(), content);
    } else {
      return new TelnetSubnegotiation(sequenceId.incrementAndGet(), option, content);
    }

  }

  public TelnetSubnegotiation getSubnegotionation(int option, int... content) {
    return getSubnegotionation(option, LangUtils.arrOfIntToArrOfByte(content));
  }

  public TelnetSubnegotiation getStartCompressionSubneg() {
    return getSubnegotionation(CMP2);
  }

  public TelnetSubnegotiation getSendTerminal() {
    return getSubnegotionation(TERMINAL_TYPE, REQUEST_TERMINAL_SEND_CONTENT);
  }

  public TelnetSubnegotiation getTerminalIsMushclient() {
    return getSubnegotionation(TERMINAL_TYPE, RESPOND_TERMINAL_IS_MUSHCLIENT_CONTENT);
  }

  public TelnetCommandWithOption refuse(TelnetPositiveCommandWithOption cmd) {
    return getCommandWithOption(TelnetProtocolResources.refuseCommandWithOption(cmd.getCode()), cmd.getOption());
  }

  public TelnetCommandWithOption confirm(TelnetCommandWithOption cmd) {
    return getCommandWithOption(TelnetProtocolResources.confirmCommandWithOption(cmd.getCode()), cmd.getOption());
  }

  public List<TelnetGMCPData> getGMCPHandshake() {
    Builder<TelnetGMCPData> builder = ImmutableList.builder();
    for (String s : TelnetGMCPData.HANDSHAKE_SEQUENCE) {
      builder.add(new TelnetGMCPData(sequenceId.incrementAndGet(), s));
    }
    return builder.build();
  }

  public List<TelnetGMCPData> getGMCPRequestAll() {
    Builder<TelnetGMCPData> builder = ImmutableList.builder();
    for (String s : TelnetGMCPData.REQUEST_ALL) {
      builder.add(new TelnetGMCPData(sequenceId.incrementAndGet(), s));
    }
    return builder.build();
  }

  public TelnetGMCPData getTelnetGMCP(String content) {
    return new TelnetGMCPData(sequenceId.incrementAndGet(), content);
  }

  public TelnetGMCPData requestArea() {
    return getTelnetGMCP(TelnetGMCPData.CLIENT_REQUEST_AREA);
  }

  public TelnetGMCPData requestChar() {
    return getTelnetGMCP(TelnetGMCPData.CLIENT_REQUEST_CHAR);
  }

  public TelnetGMCPData requestQuest() {
    return getTelnetGMCP(TelnetGMCPData.CLIENT_REQUEST_QUEST);
  }

  public TelnetGMCPData requestRoom() {
    return getTelnetGMCP(TelnetGMCPData.CLIENT_REQUEST_ROOM);
  }

  public TelnetGMCPData requestSectors() {
    return getTelnetGMCP(TelnetGMCPData.CLIENT_REQUEST_SECTORS);
  }

  public TelnetText getTelnetText(String textToSend) {
    return new TelnetText(sequenceId.incrementAndGet(), textToSend);
  }

  public TelnetData getTelnetGMCPClientRequest(GMCPRequestType gmcpRequestType) {
    switch (gmcpRequestType) {
    case Area:
      return requestArea();
    case Character:
      return requestChar();
    case Quest:
      return requestQuest();
    case Room:
      return requestRoom();
    case Sectors:
      return requestSectors();
    default:
      throw new IllegalStateException();
    }
  }

}
