package it.clem.mud.shared.telnet.writer.impl;

import java.io.IOException;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.shared.streams.TargetStream;
import it.clem.mud.shared.telnet.writer.TelnetWriter;

/**
 * E' shared tra game loop e server loop
 */
@ThreadSafe
public class TelnetWriterImpl implements TelnetWriter {
  private final static Logger                   log = LoggerFactory.getLogger(TelnetWriterImpl.class);
  private final @GuardedBy("this") TargetStream out;


  public TelnetWriterImpl(TargetStream out) {
    super();
    this.out = out;
  }

  @Override
  public synchronized void write(TelnetData data) throws IOException {
    if (log.isTraceEnabled()) {
      log.trace("Invio: " + data);
    }
    out.write(data.getBytes());
    out.flush();// se decommento il metodo flush questo va commentato
  }

  /*
   * @Override public synchronized void flush() throws IOException { if (log.isTraceEnabled()) {
   * log.trace("flushing..."); } out.flush(); }
   */
  @Override
  public synchronized void close() throws IOException {
    log.trace("closing...");
    out.close();
  }

}
