package it.clem.mud.shared.telnet.writer.impl;

import java.io.IOException;

import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.shared.recording.GameEventThreadDispatcherProducer;
import it.clem.mud.shared.telnet.writer.TelnetWriter;

public class RecordableTelnetWriter implements TelnetWriter {
  private final GameEventThreadDispatcherProducer recordProducer;
  private final TelnetWriter ts;

  public RecordableTelnetWriter(TelnetWriter ts, GameEventThreadDispatcherProducer recordProducer) {
    super();
    this.recordProducer = recordProducer;
    this.ts = ts;
  }

  @Override
  public void write(TelnetData data) throws IOException {
    try {
      ts.write(data);
    } finally {
      recordProducer.recordTelnetDataSent(data);
    }
  }

  @Override
  public void close() throws IOException {
    ts.close();
  }

}
