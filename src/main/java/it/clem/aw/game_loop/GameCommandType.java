package it.clem.aw.game_loop;

public enum GameCommandType {
  ConnectToServer, ReadDataFromServer,WriteToServer,DiconnectFromServer
}
