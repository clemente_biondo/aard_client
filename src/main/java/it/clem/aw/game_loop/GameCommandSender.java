package it.clem.aw.game_loop;

public interface GameCommandSender {

  void sendReadDataFromServerCommand();
  
  void sendConnectToServerCommand();

  void sendDiconnectFromServerCommand();
  
}
