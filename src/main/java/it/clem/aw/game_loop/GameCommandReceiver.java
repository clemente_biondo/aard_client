package it.clem.aw.game_loop;

public interface GameCommandReceiver {
  GameCommand receive() throws InterruptedException;
}
