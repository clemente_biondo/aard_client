package it.clem.aw.game_loop;

import it.clem.aw.ProtocolResources.AddressList;
import it.clem.aw.game_event.GameEventListener;
import it.clem.aw.game_event.GameEventBus;
import it.clem.aw.game_event.GameEventType;
import it.clem.aw.game_event.events.ConnectionEvent;

public class GameLoopRunner implements Runnable {
  private final GameCommandBus commandBus; // = new LinkedBlockingQueue<>();
  private final AddressList adress;
  private final GameEventBus eventBus;

  public GameLoopRunner(GameCommandBus commandBus, AddressList adress, GameEventListener listener) {
    super();
    this.commandBus = commandBus;
    this.adress = adress;
    this.eventBus = new GameEventBus(listener);
  }

  public void run() {
    GameCommandHandler ch = new GameCommandHandler( commandBus, adress, eventBus );
    commandBus.sendConnectToServerCommand();
    try {
      while (true) {
        if (ch.routeCommand(commandBus.receive())) {
          break;
        }
      }
    } catch (InterruptedException ie) {
      ie.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }//todo:servererror
  }

}
