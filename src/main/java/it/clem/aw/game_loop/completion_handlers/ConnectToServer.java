package it.clem.aw.game_loop.completion_handlers;

import java.nio.channels.CompletionHandler;

import it.clem.aw.ProtocolResources.AddressList;
import it.clem.aw.game_event.GameEventBus;
import it.clem.aw.game_event.GameEventListener;
import it.clem.aw.game_loop.GameCommandBus;
import it.clem.aw.game_loop.GameCommandSender;

public class ConnectToServer implements CompletionHandler<Void, Void>{
  private final GameEventBus eventBus;
  private final AddressList adress;
  private final GameCommandBus commandBus;
  
  public ConnectToServer(GameEventBus eventBus, AddressList adress, GameCommandBus commandBus) {
    super();
    this.eventBus = eventBus;
    this.adress = adress;
    this.commandBus = commandBus;
  }

  @Override
  public void completed(Void result, Void context) {
    eventBus.fireConnectionEstablished(adress);
    commandBus.sendReadDataFromServerCommand();
  }

  @Override
  public void failed(Throwable exc, Void context) {
    eventBus.fireServerError(exc.getMessage());
    commandBus.sendDiconnectFromServerCommand();
  }

}
