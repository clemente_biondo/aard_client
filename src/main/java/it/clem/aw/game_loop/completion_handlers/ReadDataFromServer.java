package it.clem.aw.game_loop.completion_handlers;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;

import it.clem.aw.ProtocolResources.AddressList;
import it.clem.aw.game_event.GameEventBus;
import it.clem.aw.game_loop.GameCommandBus;

public class ReadDataFromServer implements CompletionHandler<Integer, ByteBuffer> {
  private final GameEventBus eventBus;
  private final GameCommandBus commandBus;
  
  public ReadDataFromServer(GameEventBus eventBus, GameCommandBus commandBus) {
    super();
    this.eventBus = eventBus;
    this.commandBus = commandBus;
  }


  @Override
  public void completed(Integer result, ByteBuffer buffer) {
    if (result==-1) {
      eventBus.fireServerError("Connessione chiusa dal server.");
      commandBus.sendDiconnectFromServerCommand();
    } else {
      buffer.flip();
      eventBus.fireTextReceivedFromServer(new String(buffer.array()));
      buffer.clear();
      commandBus.sendReadDataFromServerCommand();
    }
  }

  @Override
  public void failed(Throwable exc, ByteBuffer buffer) {
    eventBus.fireServerError(exc.getMessage());
  }


}
