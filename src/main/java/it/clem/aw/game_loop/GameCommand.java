package it.clem.aw.game_loop;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;

public class GameCommand implements Serializable {

  private final static AtomicInteger sequenceGenerator = new AtomicInteger();
  private final LocalDateTime        creationdateTime  = LocalDateTime.now();
  private final Integer              sequenceId        = sequenceGenerator.incrementAndGet();
  private final Serializable         payload;
  private final GameCommandType      type;

  public GameCommand(GameCommandType type) {
    this(type, null);
  }

  public GameCommand(GameCommandType type, Serializable payload) {
    super();
    this.payload = payload;
    this.type = type;
  }

  public LocalDateTime getCreationdateTime() {
    return creationdateTime;
  }

  public Integer getSequenceId() {
    return sequenceId;
  }

  public Serializable getPayload() {
    return payload;
  }

  public GameCommandType getType() {
    return type;
  }

}
