package it.clem.aw.game_loop;

import java.util.concurrent.BlockingQueue;

import it.clem.aw.ProtocolResources.AddressList;

//threadsafe
public class GameCommandBusImpl implements GameCommandBus{
  private final BlockingQueue<GameCommand> commandBus;
  
  public GameCommandBusImpl(AddressList adress, BlockingQueue<GameCommand> queue) {
    super();
    this.commandBus = queue;
  }
  
  @Override
  public GameCommand receive() throws InterruptedException{
    return commandBus.take();
  }
  
  @Override
  public void sendReadDataFromServerCommand(){
    commandBus.add(new GameCommand(GameCommandType.ReadDataFromServer));
  }
  
  @Override
  public void sendConnectToServerCommand(){
    commandBus.add(new GameCommand(GameCommandType.ConnectToServer));
  }
  
  @Override
  public void sendDiconnectFromServerCommand(){
    commandBus.add(new GameCommand(GameCommandType.DiconnectFromServer));
  }
}
