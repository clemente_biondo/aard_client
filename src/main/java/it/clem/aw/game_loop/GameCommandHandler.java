package it.clem.aw.game_loop;

import static it.clem.aw.ProtocolResources.AddressList.TeleHack;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import it.clem.aw.ProtocolResources.AddressList;
import it.clem.aw.game_event.GameEventBus;
import it.clem.aw.game_event.GameEventListener;
import it.clem.aw.game_event.GameEventType;
import it.clem.aw.game_event.events.ConnectionEvent;
import it.clem.aw.game_event.events.TextEvent;
import it.clem.aw.game_loop.completion_handlers.ConnectToServer;
import it.clem.aw.game_loop.completion_handlers.ReadDataFromServer;
import it.clem.mud.protocol.AardProtocolResources;

public class GameCommandHandler {
  private final GameEventBus eventBus;
  private final AddressList adress;
  private AsynchronousSocketChannel channelToServer;

  private final ByteBuffer incomingServerDataBuffer = ByteBuffer.allocateDirect(16 * 1024);
  private final CompletionHandler<Void, Void> connectToServer;
  private final CompletionHandler<Integer, ByteBuffer> readDataFromServer;

  public GameCommandHandler(GameCommandBus commandBus, AddressList adress, GameEventBus eventBus) {
    super();
    this.eventBus = eventBus;
    this.adress = adress;
    this.connectToServer = new ConnectToServer(eventBus, adress, commandBus);
    this.readDataFromServer = new ReadDataFromServer(eventBus, commandBus);
  }

  public boolean routeCommand(GameCommand cmd) throws InterruptedException {
    switch (cmd.getType()) {
    case ConnectToServer:
      return connectToServer();
    case ReadDataFromServer:
      return readDataFromServer();
    case WriteToServer:
      return writeDataToServer((String)cmd.getPayload());
    case DiconnectFromServer:
      return disconnectFromServer();
    default:
      throw new IllegalArgumentException(cmd.getType().name());
    }
  }
  
  private boolean disconnectFromServer() throws InterruptedException {
    try {
      channelToServer.shutdownInput();
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      channelToServer.shutdownOutput();
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      Thread.sleep(1000);
    } finally {
      try {
        channelToServer.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    return false;
  }
  
  private boolean writeDataToServer(String text) throws InterruptedException {
    channelToServer.write(AardProtocolResources.ENCODING_CHARSET.encode(CharBuffer.wrap(text)));
    return true;
  }
  
  private boolean readDataFromServer() {
    channelToServer.read(incomingServerDataBuffer, incomingServerDataBuffer, this.readDataFromServer);
    return true;
  }

  private boolean connectToServer() {
    try {
      AsynchronousChannelGroup asynchronousChannelGroup = AsynchronousChannelGroup
          //.withThreadPool(Executors.newSingleThreadExecutor());
          .withThreadPool(Executors.newCachedThreadPool());
      channelToServer = AsynchronousSocketChannel.open(asynchronousChannelGroup);
      channelToServer.connect(adress.getAddress(), null, connectToServer);
    } catch (Exception ex) {
      ex.printStackTrace();
      eventBus.fireServerError(ex.getMessage());
      return false;
    }
    return true;
  }

}
