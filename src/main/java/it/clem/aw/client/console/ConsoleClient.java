package it.clem.aw.client.console;

import it.clem.aw.game_event.GameEventListener;
import it.clem.aw.game_event.events.ConnectionEvent;
import it.clem.aw.game_event.events.TextEvent;

public class ConsoleClient implements GameEventListener{

  public static void main(String[] args) {
    // TODO Auto-generated method stub

  }

  @Override //eseguito da un altro thread
  public void connectionClosed(ConnectionEvent event) {
    notify("connection closed"+event);
    
  }

  @Override //eseguito da un altro thread
  public void connectionEstablished(ConnectionEvent event) {
    notify("connection established"+event);
    
  }

  @Override //eseguito da un altro thread
  public void textReceived(TextEvent event) {
    notify(event.getText());
  }

  //eseguito da un altro thread
  //tolgo synchronized perche' System.out lo � gi�
  private /*synchronized*/ void notify(String s) {
    System.out.println(s);
  }
}
