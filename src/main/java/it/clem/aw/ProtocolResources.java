package it.clem.aw;

import static it.clem.aw.LangUtils.getInetSocketAddress;

import java.net.InetSocketAddress;

public class ProtocolResources {
  
  public static enum AddressList {
    CubanBar,TeleHack;
    public final InetSocketAddress getAddress() {
      switch (this) {
      case CubanBar:
        return getInetSocketAddress(52, 88, 68, 92, 1234);
      case TeleHack:
        return new InetSocketAddress("telehack.com", 23);
        
      default:
        throw new IllegalStateException(this.name());
      }
    }
  }
  
  
}
