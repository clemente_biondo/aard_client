package it.clem.aw;

import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class LangUtils {


  public static InetAddress getInetAddress(byte b1, byte b2, byte b3, byte b4) {
    try {
      return InetAddress.getByAddress(new byte[] { b1, b2, b3, b4 });
    } catch (UnknownHostException e) {
      throw new UncheckedIOException(e.getMessage(), e);
    }
  }

  public static InetAddress getInetAddress(int b1, int b2, int b3, int b4) {
    return getInetAddress((byte) b1, (byte) b2, (byte) b3, (byte) b4);
  }

  public static InetSocketAddress getInetSocketAddress(int b1, int b2, int b3, int b4, int port) {
    return getInetSocketAddress((byte) b1, (byte) b2, (byte) b3, (byte) b4, port);
  }

  public static InetSocketAddress getInetSocketAddress(byte b1, byte b2, byte b3, byte b4, int port) {
    return new InetSocketAddress(getInetAddress(b1, b2, b3, b4), port);
  }
}
