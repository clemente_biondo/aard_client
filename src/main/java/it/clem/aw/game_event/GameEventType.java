package it.clem.aw.game_event;

public enum GameEventType {
  ConnectionEstablished,Disconnected,ServerSentText,ServerError
}
