package it.clem.aw.game_event;


import it.clem.aw.game_event.events.ConnectionEvent;
import it.clem.aw.game_event.events.TextEvent;

public interface GameEventListener {
  void connectionClosed(ConnectionEvent event);
  void connectionEstablished(ConnectionEvent event);
  void textReceived(TextEvent event);
  void serverError(TextEvent event);
}
