package it.clem.aw.game_event;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;

public class GameEvent implements Serializable {

  private final static AtomicInteger sequenceGenerator = new AtomicInteger();
  private final LocalDateTime        creationdateTime  = LocalDateTime.now();
  private final Integer              sequenceId        = sequenceGenerator.incrementAndGet();
  private final GameEventType        type;

  public GameEvent(GameEventType type) {
    super();
    this.type = type;
  }

  public LocalDateTime getCreationdateTime() {
    return creationdateTime;
  }

  public Integer getSequenceId() {
    return sequenceId;
  }

  public GameEventType getType() {
    return type;
  }

}
