package it.clem.aw.game_event;

import it.clem.aw.ProtocolResources.AddressList;
import it.clem.aw.game_event.events.ConnectionEvent;
import it.clem.aw.game_event.events.TextEvent;

public class GameEventBus {
  private final GameEventListener listener;

  public GameEventBus(GameEventListener listener) {
    super();
    this.listener = listener;
  }

  public void fireConnectionEstablished(AddressList adress) {
    listener.connectionEstablished(new ConnectionEvent(GameEventType.ConnectionEstablished, adress));
  }

  public void fireServerError(String message) {
    listener.serverError(new TextEvent(GameEventType.ServerError, message));
  }

  public void fireConnectionClosed(AddressList adress) {
    listener.connectionClosed(new ConnectionEvent(GameEventType.Disconnected, adress));
  }

  public void fireTextReceivedFromServer(String message) {
    listener.textReceived(new TextEvent(GameEventType.ServerSentText, message));
  }

}
