package it.clem.aw.game_event.events;

import it.clem.aw.game_event.GameEvent;
import it.clem.aw.game_event.GameEventType;

public class TextEvent extends GameEvent {

  private static final long serialVersionUID = 5367165677946171850L;
  private final String      text;

  public TextEvent(GameEventType type, String text) {
    super(type);
    this.text = text;
  }

  public String getText() {
    return text;
  }

}
