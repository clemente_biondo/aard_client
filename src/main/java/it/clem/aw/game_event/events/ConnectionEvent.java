package it.clem.aw.game_event.events;

import it.clem.aw.ProtocolResources.AddressList;
import it.clem.aw.game_event.GameEvent;
import it.clem.aw.game_event.GameEventType;

public class ConnectionEvent extends GameEvent {

  private static final long serialVersionUID = 5367165677946171850L;
  private final AddressList server;

  public ConnectionEvent(GameEventType type, AddressList server) {
    super(type);
    this.server = server;
  }

  public AddressList getServer() {
    return server;
  }

}
