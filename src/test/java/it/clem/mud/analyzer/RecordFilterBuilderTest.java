package it.clem.mud.analyzer;

import it.clem.mud.recorder.Record;
import it.clem.mud.recorder.RecordType;
import it.clem.mud.recorder.analyzer.RecordFilterBuilder;
import it.clem.mud.shared.game.event.GameEventFactory;
import it.clem.mud.shared.game.event.GameEventFactoryImpl;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import static org.junit.Assert.*;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

public class RecordFilterBuilderTest {
  private RecordFilterBuilder builder = new RecordFilterBuilder();
  private TelnetDataFactory tFactory;
  private GameEventFactory gFactory;

  @Before
  public void setUp() {
    builder = new RecordFilterBuilder();
    tFactory = new TelnetDataFactory();
    gFactory = new GameEventFactoryImpl();
  }

  @Test
  public void testEmpty() {
    // fa passare tutto
    Predicate<Record> p = builder.build();
    for (int i = 0; i < RecordType.values().length; i++) {
      assertTrue(p.test(new Record(RecordType.values()[i], new Integer(i))));
    }
  }

  @Test
  public void testblacklistAll1() {
    // fa passare tutto
    Predicate<Record> p = builder.whitelist().build();
    for (int i = 0; i < RecordType.values().length; i++) {
      assertFalse(p.test(new Record(RecordType.values()[i], new Integer(i))));
    }

  }

  @Test
  public void testblacklistAll2() {
    // fa passare tutto
    Predicate<Record> p = builder.exclude(RecordType.values()).build();
    for (int i = 0; i < RecordType.values().length; i++) {
      assertFalse(p.test(new Record(RecordType.values()[i], new Integer(i))));
    }

  }

  @Test
  public void testTenet() {
    Predicate<Record> p= RecordFilterBuilder.ONLY_TELNET_COMMANDS;
    TelnetDataFactory f=new TelnetDataFactory();

    assertTrue( p.test(new Record(RecordType.TelnetDataReceived,tFactory.getGA())));  
    assertFalse( p.test(new Record(RecordType.GameEvent, gFactory.getTextSentFromServerEvent("abc"))));  
    assertFalse( p.test(new Record(RecordType.TelnetDataReceived, f.getTelnetText("def"))));  
  }
}
