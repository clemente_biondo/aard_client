package it.clem.mud.integration.swing;

import java.io.IOException;
import java.io.InputStream;
import it.clem.mud.TestUtils;
import it.clem.mud.client.swing.MainWindow;
import it.clem.mud.gedt.socket.AbstractSocket;
import it.clem.mud.gedt.socket.impl.AbstractSocketImpl;
import it.clem.mud.shared.streams.impl.SourceStreamImpl;
import it.clem.mud.shared.streams.impl.TargetStreamImpl;
import it.clem.mud.util.TimedInputStream;

public class DisplayXtermTextAndMapTag {

  public static void main(String[] args) throws IOException {
    InputStream tis = new TimedInputStream(TestUtils.getBufferedInputStream("aard_xterm_due/in.bin"));
    AbstractSocket socket= new AbstractSocketImpl(new SourceStreamImpl(tis) ,new TargetStreamImpl(System.out));
    
    javax.swing.SwingUtilities.invokeLater(() -> {
      MainWindow main = TestUtils.getMainWindow(socket,"Test Map Tag");
      main.pack();
      main.setVisible(true);
    });
  }

}
