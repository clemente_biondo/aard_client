package it.clem.mud.game;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.gedt.GameLoop;
import it.clem.mud.gedt.event.bus.GameEventBus;
import it.clem.mud.gedt.event.bus.impl.GameEventBusImpl;
import it.clem.mud.gedt.event.listener.ConnectionClosedByServerEventListener;
import it.clem.mud.gedt.event.listener.GenericGameEventListener;
import it.clem.mud.gedt.event.listener.ServerErrorEventListener;
import it.clem.mud.gedt.event.listener.TextSentFromClientEventListener;
import it.clem.mud.gedt.event.listener.TextSentFromServerEventListener;
import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.event.GameEventFactory;
import it.clem.mud.shared.game.event.GameEventFactoryImpl;
import it.clem.mud.shared.game.event.client.TextSentFromClientEvent;
import it.clem.mud.shared.game.event.queue.GameEventConsumer;
import it.clem.mud.shared.game.event.server.ConnectionClosedByServerEvent;
import it.clem.mud.shared.game.event.server.ServerErrorEvent;
import it.clem.mud.shared.game.event.server.TextSentFromServerEvent;
import it.clem.mud.shared.recording.GameEventThreadDispatcherProducer;

@RunWith(MockitoJUnitRunner.class)
public class GameLoopTest {
    @SuppressWarnings("unused")
    private final static Logger log =LoggerFactory.getLogger(GameLoopTest.class);
    
    private GameLoop gameLoop;
    
    private GameEventBus gameBus;
    
    private GameEventFactory gameEventFactory;
    
    @Mock private GameEventConsumer eventQueue;
    @Mock private GameContext    gameContext;
    @Mock private GameEventThreadDispatcherProducer recordProducer;
    @Before
    public void setUp() {
      gameEventFactory = new GameEventFactoryImpl();
    }

    
    @Test(timeout=500)
    public void testGenericNonConsumableEvent1() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      GenericGameEventListener gl = mock(GenericGameEventListener.class);
      when(gl.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder()
          .addGenericEventListener(gl)
          .build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromClientEvent("cli"),
        gameEventFactory.getTextSentFromServerEvent("srv"),
        gameEventFactory.getConnectionClosedByServerEvent()
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 3 eventi caricati
      verify(eventQueue,times(3)).take();

      //le 3 invocazioni attese
      InOrder ov = inOrder(gl);
      ov.verify(gl,times(1)).onGameEvent(any(TextSentFromClientEvent.class), any());
      ov.verify(gl,times(1)).onGameEvent(any(TextSentFromServerEvent.class), any());
      ov.verify(gl,times(1)).onGameEvent(any(ConnectionClosedByServerEvent.class), any());
      verifyNoMoreInteractions(gameContext,eventQueue,gl);
    }
    
    @Test(timeout=500)
    public void testGenericNonConsumableEvent2() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      GenericGameEventListener gl = mock(GenericGameEventListener.class);
      when(gl.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder()
          .addGenericEventListener(gl)
          .build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromClientEvent("cli"),
        gameEventFactory.getTextSentFromServerEvent("srv"),
        gameEventFactory.getServerErrorEvent("err")
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 3 eventi caricati
      verify(eventQueue,times(3)).take();

      //le 3 invocazioni attese
      InOrder ov = inOrder(gl);
      ov.verify(gl,times(1)).onGameEvent(any(TextSentFromClientEvent.class), any());
      ov.verify(gl,times(1)).onGameEvent(any(TextSentFromServerEvent.class), any());
      ov.verify(gl,times(1)).onGameEvent(any(ServerErrorEvent.class), any());
      verifyNoMoreInteractions(gameContext,eventQueue,gl);
    }
    
    @Test(timeout=500)
    public void testTwoGenericNonConsumableEvent1() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      GenericGameEventListener gl1 = mock(GenericGameEventListener.class);
      GenericGameEventListener gl2 = mock(GenericGameEventListener.class);
      when(gl1.onGameEvent(any(), any())).thenReturn(false);
      when(gl2.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder()
          .addGenericEventListener(gl1)
          .addGenericEventListener(gl2)
          .build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromClientEvent("cli"),
        gameEventFactory.getTextSentFromServerEvent("srv"),
        gameEventFactory.getConnectionClosedByServerEvent()
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 3 eventi caricati
      verify(eventQueue,times(3)).take();

      //le 3 invocazioni attese
      InOrder ov = inOrder(gl1,gl2);
      ov.verify(gl1,times(1)).onGameEvent(any(TextSentFromClientEvent.class), any());
      ov.verify(gl2,times(1)).onGameEvent(any(TextSentFromClientEvent.class), any());
      ov.verify(gl1,times(1)).onGameEvent(any(TextSentFromServerEvent.class), any());
      ov.verify(gl2,times(1)).onGameEvent(any(TextSentFromServerEvent.class), any());
      ov.verify(gl1,times(1)).onGameEvent(any(ConnectionClosedByServerEvent.class), any());
      ov.verify(gl2,times(1)).onGameEvent(any(ConnectionClosedByServerEvent.class), any());
      verifyNoMoreInteractions(gameContext,eventQueue,gl1,gl2);
    }
    
    @Test(timeout=500)
    public void testTwoGenericNonConsumableEvent2() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      GenericGameEventListener gl1 = mock(GenericGameEventListener.class);
      GenericGameEventListener gl2 = mock(GenericGameEventListener.class);
      when(gl1.onGameEvent(any(), any())).thenReturn(false);
      when(gl2.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder()
          .addGenericEventListener(gl1)
          .addGenericEventListener(gl2)
          .build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromClientEvent("cli"),
        gameEventFactory.getTextSentFromServerEvent("srv"),
        gameEventFactory.getServerErrorEvent("err")
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 3 eventi caricati
      verify(eventQueue,times(3)).take();

      //le 3 invocazioni attese
      InOrder ov = inOrder(gl1,gl2);
      ov.verify(gl1,times(1)).onGameEvent(any(TextSentFromClientEvent.class), any());
      ov.verify(gl2,times(1)).onGameEvent(any(TextSentFromClientEvent.class), any());
      ov.verify(gl1,times(1)).onGameEvent(any(TextSentFromServerEvent.class), any());
      ov.verify(gl2,times(1)).onGameEvent(any(TextSentFromServerEvent.class), any());
      ov.verify(gl1,times(1)).onGameEvent(any(ServerErrorEvent.class), any());
      ov.verify(gl2,times(1)).onGameEvent(any(ServerErrorEvent.class), any());
      verifyNoMoreInteractions(gameContext,eventQueue,gl1,gl2);
    }
    @Test(timeout=500)
    public void testTwoGenericConsumableEvent1() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      GenericGameEventListener gl1 = mock(GenericGameEventListener.class);
      GenericGameEventListener gl2 = mock(GenericGameEventListener.class);
      when(gl1.onGameEvent(any(), any())).thenReturn(true);
      when(gl2.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder()
          .addGenericEventListener(gl1)
          .addGenericEventListener(gl2)
          .build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromClientEvent("cli"),
        gameEventFactory.getTextSentFromServerEvent("srv"),
        gameEventFactory.getConnectionClosedByServerEvent()
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 3 eventi caricati
      verify(eventQueue,times(3)).take();

      //le 4 invocazioni attese
      InOrder ov = inOrder(gl1,gl2);
      ov.verify(gl1,times(1)).onGameEvent(any(TextSentFromClientEvent.class), any());
      ov.verify(gl1,times(1)).onGameEvent(any(TextSentFromServerEvent.class), any());
      ov.verify(gl1,times(1)).onGameEvent(any(ConnectionClosedByServerEvent.class), any());
      verifyNoMoreInteractions(gameContext,eventQueue,gl1,gl2);
    }
    
    @Test(timeout=500)
    public void testTwoGenericConsumableEvent2() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      GenericGameEventListener gl1 = mock(GenericGameEventListener.class);
      GenericGameEventListener gl2 = mock(GenericGameEventListener.class);
      when(gl1.onGameEvent(any(), any())).thenReturn(true);
      when(gl2.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder()
          .addGenericEventListener(gl1)
          .addGenericEventListener(gl2)
          .build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromClientEvent("cli"),
        gameEventFactory.getTextSentFromServerEvent("srv"),
        gameEventFactory.getServerErrorEvent("err")
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 3 eventi caricati
      verify(eventQueue,times(3)).take();

      //le 4 invocazioni attese
      InOrder ov = inOrder(gl1,gl2);
      ov.verify(gl1,times(1)).onGameEvent(any(TextSentFromClientEvent.class), any());
      ov.verify(gl1,times(1)).onGameEvent(any(TextSentFromServerEvent.class), any());
      ov.verify(gl1,times(1)).onGameEvent(any(ServerErrorEvent.class), any());
      verifyNoMoreInteractions(gameContext,eventQueue,gl1,gl2);
    }

    @Test(timeout=1000)
    public void testMultipleNonConsumableEvent1() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      TextSentFromClientEventListener gl1 = mock(TextSentFromClientEventListener.class);
      GenericGameEventListener gl2 = mock(GenericGameEventListener.class);
      TextSentFromServerEventListener gl3 = mock(TextSentFromServerEventListener.class);
      ServerErrorEventListener gl4 = mock(ServerErrorEventListener.class);
      ConnectionClosedByServerEventListener gl5 = mock(ConnectionClosedByServerEventListener.class);
      GenericGameEventListener gl6 = mock(GenericGameEventListener.class);
      
      when(gl1.onTextSentFromClient(any(), any())).thenReturn(false);
      when(gl2.onGameEvent(any(), any())).thenReturn(false);
      when(gl3.onTextSentFromServer(any(), any())).thenReturn(false);
      when(gl4.onServerError(any(), any())).thenReturn(false);
      when(gl5.onConnectionClosedByServer(any(), any())).thenReturn(false);
      when(gl6.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder().add(gl1,gl2,gl3,gl4,gl5,gl6).build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromServerEvent("srv2"),
        gameEventFactory.getTextSentFromClientEvent("cli1"),
        gameEventFactory.getTextSentFromServerEvent("srv1"),
        gameEventFactory.getTextSentFromClientEvent("cli2"),
        gameEventFactory.getConnectionClosedByServerEvent()
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 5 eventi caricati
      verify(eventQueue,times(5)).take();

      //le invocazioni attese
      InOrder ov = inOrder(gl1,gl2,gl3,gl4,gl5,gl6);
      ov.verify(gl2,times(1)).onGameEvent( argThat(e -> e instanceof TextSentFromServerEvent && e.toString().contains("srv2")), any());
      ov.verify(gl3,times(1)).onTextSentFromServer(argThat(e -> e.getText().equals("srv2")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("srv2")), any());
      ov.verify(gl1,times(1)).onTextSentFromClient(argThat(e -> e.getText().equals("cli1")), any());
      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli1")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli1")), any());
      ov.verify(gl2,times(1)).onGameEvent( argThat(e -> e.toString().contains("srv1")), any());
      ov.verify(gl3,times(1)).onTextSentFromServer(argThat(e -> e.getText().equals("srv1")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("srv1")), any());
      ov.verify(gl1,times(1)).onTextSentFromClient(argThat(e -> e.getText().equals("cli2")), any());
      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli2")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli2")), any());
      ov.verify(gl2,times(1)).onGameEvent(any(ConnectionClosedByServerEvent.class) , any());
      ov.verify(gl5,times(1)).onConnectionClosedByServer(any(ConnectionClosedByServerEvent.class) , any());
      ov.verify(gl6,times(1)).onGameEvent(any(ConnectionClosedByServerEvent.class) , any());

      
      verifyNoMoreInteractions(gameContext,eventQueue,gl1,gl2,gl3,gl4,gl5,gl6);
    }
    
    @Test(timeout=1000)
    public void testMultipleNonConsumableEvent2() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      TextSentFromClientEventListener gl1 = mock(TextSentFromClientEventListener.class);
      GenericGameEventListener gl2 = mock(GenericGameEventListener.class);
      TextSentFromServerEventListener gl3 = mock(TextSentFromServerEventListener.class);
      ServerErrorEventListener gl4 = mock(ServerErrorEventListener.class);
      ConnectionClosedByServerEventListener gl5 = mock(ConnectionClosedByServerEventListener.class);
      GenericGameEventListener gl6 = mock(GenericGameEventListener.class);
      
      when(gl1.onTextSentFromClient(any(), any())).thenReturn(false);
      when(gl2.onGameEvent(any(), any())).thenReturn(false);
      when(gl3.onTextSentFromServer(any(), any())).thenReturn(false);
      when(gl4.onServerError(any(), any())).thenReturn(false);
      when(gl5.onConnectionClosedByServer(any(), any())).thenReturn(false);
      when(gl6.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder().add(gl1,gl2,gl3,gl4,gl5,gl6).build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromServerEvent("srv2"),
        gameEventFactory.getTextSentFromClientEvent("cli1"),
        gameEventFactory.getTextSentFromServerEvent("srv1"),
        gameEventFactory.getTextSentFromClientEvent("cli2"),
        gameEventFactory.getServerErrorEvent("err1")
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 5 eventi caricati
      verify(eventQueue,times(5)).take();

      //le invocazioni attese
      InOrder ov = inOrder(gl1,gl2,gl3,gl4,gl5,gl6);
      ov.verify(gl2,times(1)).onGameEvent( argThat(e -> e instanceof TextSentFromServerEvent && e.toString().contains("srv2")), any());
      ov.verify(gl3,times(1)).onTextSentFromServer(argThat(e -> e.getText().equals("srv2")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("srv2")), any());
      ov.verify(gl1,times(1)).onTextSentFromClient(argThat(e -> e.getText().equals("cli1")), any());
      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli1")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli1")), any());
      ov.verify(gl2,times(1)).onGameEvent( argThat(e -> e.toString().contains("srv1")), any());
      ov.verify(gl3,times(1)).onTextSentFromServer(argThat(e -> e.getText().equals("srv1")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("srv1")), any());
      ov.verify(gl1,times(1)).onTextSentFromClient(argThat(e -> e.getText().equals("cli2")), any());
      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli2")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli2")), any());
      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("err1")), any());
      ov.verify(gl4,times(1)).onServerError(argThat(e -> e.getErrorMessage().equals("err1")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("err1")), any());

      
      verifyNoMoreInteractions(gameContext,eventQueue,gl1,gl2,gl3,gl4,gl5,gl6);
    }
    @Test(timeout=1000)
    public void testMultipleConsumableEvent1() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      TextSentFromClientEventListener gl1 = mock(TextSentFromClientEventListener.class);
      GenericGameEventListener gl2 = mock(GenericGameEventListener.class);
      TextSentFromServerEventListener gl3 = mock(TextSentFromServerEventListener.class);
      ServerErrorEventListener gl4 = mock(ServerErrorEventListener.class);
      ConnectionClosedByServerEventListener gl5 = mock(ConnectionClosedByServerEventListener.class);
      GenericGameEventListener gl6 = mock(GenericGameEventListener.class);

      //gl1 consuma cli1,cli2
      when(gl1.onTextSentFromClient(any(), any())).thenReturn(true);
      //gl2 consuma srv2,err2 ma non cli2
      when(gl2.onGameEvent(any(), any())).then((i)->i.getArgument(0).toString().contains("2, "));
      when(gl3.onTextSentFromServer(any(), any())).thenReturn(false);
      //gl4 consuma err1 ma non err2
      when(gl4.onServerError(any(), any())).thenReturn(true);
      when(gl5.onConnectionClosedByServer(any(), any())).thenReturn(false);
      when(gl6.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder().add(gl1,gl2,gl3,gl4,gl5,gl6).build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromServerEvent("srv2"),
        gameEventFactory.getTextSentFromClientEvent("cli1"),
        gameEventFactory.getTextSentFromServerEvent("srv1"),
        gameEventFactory.getTextSentFromClientEvent("cli2"),
        gameEventFactory.getConnectionClosedByServerEvent()
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 5 eventi caricati
      verify(eventQueue,times(5)).take();

      //le invocazioni attese
      InOrder ov = inOrder(gl1,gl2,gl3,gl4,gl5,gl6);
      ov.verify(gl2,times(1)).onGameEvent( argThat(e -> e instanceof TextSentFromServerEvent && e.toString().contains("srv2")), any());
//      ov.verify(gl3,times(1)).onTextSentFromServer(argThat(e -> e.getText().equals("srv2")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("srv2")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("err1")), any());
      ov.verify(gl1,times(1)).onTextSentFromClient(argThat(e -> e.getText().equals("cli1")), any());
//      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli1")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli1")), any());
      ov.verify(gl2,times(1)).onGameEvent( argThat(e -> e.toString().contains("srv1")), any());
      ov.verify(gl3,times(1)).onTextSentFromServer(argThat(e -> e.getText().equals("srv1")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("srv1")), any());
//      ov.verify(gl4,times(1)).onServerError(argThat(e -> e.getErrorMessage().equals("err2")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("err2")), any());
      ov.verify(gl1,times(1)).onTextSentFromClient(argThat(e -> e.getText().equals("cli2")), any());
//      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli2")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli2")), any());
      ov.verify(gl2,times(1)).onGameEvent(any(ConnectionClosedByServerEvent.class) , any());
      ov.verify(gl5,times(1)).onConnectionClosedByServer(any(ConnectionClosedByServerEvent.class) , any());
      ov.verify(gl6,times(1)).onGameEvent(any(ConnectionClosedByServerEvent.class) , any());
      
      verifyNoMoreInteractions(gameContext,eventQueue,gl1,gl2,gl3,gl4,gl5,gl6);
    }
    
    @Test(timeout=1000)
    public void testMultipleConsumableEvent2() throws Exception{
      //verifico che il listener generico venga chiamato per tutti gli eventi
      TextSentFromClientEventListener gl1 = mock(TextSentFromClientEventListener.class);
      GenericGameEventListener gl2 = mock(GenericGameEventListener.class);
      TextSentFromServerEventListener gl3 = mock(TextSentFromServerEventListener.class);
      ServerErrorEventListener gl4 = mock(ServerErrorEventListener.class);
      ConnectionClosedByServerEventListener gl5 = mock(ConnectionClosedByServerEventListener.class);
      GenericGameEventListener gl6 = mock(GenericGameEventListener.class);

      //gl1 consuma cli1,cli2
      when(gl1.onTextSentFromClient(any(), any())).thenReturn(true);
      //gl2 consuma srv2,err2 ma non cli2
      when(gl2.onGameEvent(any(), any())).then((i)->i.getArgument(0).toString().contains("2, "));
      when(gl3.onTextSentFromServer(any(), any())).thenReturn(false);
      //gl4 consuma err1 ma non err2
      when(gl4.onServerError(any(), any())).thenReturn(true);
      when(gl5.onConnectionClosedByServer(any(), any())).thenReturn(false);
      when(gl6.onGameEvent(any(), any())).thenReturn(false);
      
      gameBus = GameEventBusImpl.builder().add(gl1,gl2,gl3,gl4,gl5,gl6).build();
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);

      //carico tutti gli eventi possibili. Mi aspetto che vengano intercettati tutti   
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromServerEvent("srv2"),
        gameEventFactory.getTextSentFromClientEvent("cli1"),
        gameEventFactory.getTextSentFromServerEvent("srv1"),
        gameEventFactory.getTextSentFromClientEvent("cli2"),
        gameEventFactory.getServerErrorEvent("err1")
      );
      
      //avvio il test
      gameLoop.doLoop();

      //i 7 eventi caricati
      verify(eventQueue,times(5)).take();

      //le invocazioni attese
      InOrder ov = inOrder(gl1,gl2,gl3,gl4,gl5,gl6);
      ov.verify(gl2,times(1)).onGameEvent( argThat(e -> e instanceof TextSentFromServerEvent && e.toString().contains("srv2")), any());
//      ov.verify(gl3,times(1)).onTextSentFromServer(argThat(e -> e.getText().equals("srv2")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("srv2")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("err1")), any());
      ov.verify(gl1,times(1)).onTextSentFromClient(argThat(e -> e.getText().equals("cli1")), any());
//      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli1")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli1")), any());
      ov.verify(gl2,times(1)).onGameEvent( argThat(e -> e.toString().contains("srv1")), any());
      ov.verify(gl3,times(1)).onTextSentFromServer(argThat(e -> e.getText().equals("srv1")), any());
      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("srv1")), any());
//      ov.verify(gl4,times(1)).onServerError(argThat(e -> e.getErrorMessage().equals("err2")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("err2")), any());
      ov.verify(gl1,times(1)).onTextSentFromClient(argThat(e -> e.getText().equals("cli2")), any());
//      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli2")), any());
//      ov.verify(gl6,times(1)).onGameEvent(argThat(e -> e.toString().contains("cli2")), any());
      ov.verify(gl2,times(1)).onGameEvent(argThat(e -> e.toString().contains("err1")), any());
      ov.verify(gl4,times(1)).onServerError(argThat(e -> e.getErrorMessage().equals("err1")), any());
      
      verifyNoMoreInteractions(gameContext,eventQueue,gl1,gl2,gl3,gl4,gl5,gl6);
    }
    //TODO: testqare il consume,gli eventi specifici,etc.
    //loggare in ed out stream in esecuzione, e fare anche event sourcing
    //finire i test di runnable game loop e minimal
    //adeguare standard loop
    //provare il cuban cafe e poi aard in versioni minimal stdout

}
