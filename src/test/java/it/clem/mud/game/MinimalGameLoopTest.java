package it.clem.mud.game;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.gedt.GameLoop;
import it.clem.mud.gedt.event.bus.GameEventBus;
import it.clem.mud.gedt.event.bus.impl.GameEventBusImpl;
import it.clem.mud.shared.game.GameContext;
import it.clem.mud.shared.game.GameStrategy;
import it.clem.mud.shared.game.event.GameEventFactory;
import it.clem.mud.shared.game.event.GameEventFactoryImpl;
import it.clem.mud.shared.game.event.queue.GameEventConsumer;
import it.clem.mud.shared.recording.GameEventThreadDispatcherProducer;

@RunWith(MockitoJUnitRunner.class)
public class MinimalGameLoopTest {
    @SuppressWarnings("unused")
    private final static Logger log =LoggerFactory.getLogger(MinimalGameLoopTest.class);
    
    private GameLoop gameLoop;
    
    private GameEventBus gameBus;
    
    private GameEventFactory gameEventFactory;
    
    @Mock private GameEventConsumer eventQueue;
    @Mock private GameContext    gameContext;
    @Mock private GameEventThreadDispatcherProducer recordProducer;
    
    @Before
    public void setUp() {
      gameBus = GameEventBusImpl.getBusByStrategy(GameStrategy.Minimal);
      gameLoop = new GameLoop(eventQueue, gameContext, gameBus,recordProducer);
      gameEventFactory = new GameEventFactoryImpl();
    }

    @Test(timeout=500)
    public void testLoopEnd() throws Exception{
      when(eventQueue.take())
       .thenReturn(gameEventFactory.getConnectionClosedByServerEvent());
      gameLoop.doLoop();
      verify(eventQueue,times(1)).take();
      verifyNoMoreInteractions(gameContext,eventQueue);
    }

    @Test(timeout=500)
    public void testText1() throws Exception{
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromServerEvent("abc"),
        gameEventFactory.getTextSentFromServerEvent("def"),
        gameEventFactory.getTextSentFromClientEvent("ghi"),
        gameEventFactory.getConnectionClosedByServerEvent()
      );
      gameLoop.doLoop();
      verify(eventQueue,times(4)).take();
      verify(gameContext,times(1)).sendTextToServer(any());
      verify(gameContext,times(2)).sendTextToClient(any());
      verifyNoMoreInteractions(gameContext,eventQueue);
    }
    @Test(timeout=500)
    public void testText2() throws Exception{
      when(eventQueue.take()).thenReturn(
        gameEventFactory.getTextSentFromServerEvent("abc"),
        gameEventFactory.getTextSentFromServerEvent("def"),
        gameEventFactory.getTextSentFromClientEvent("ghi"),
        gameEventFactory.getServerErrorEvent("Error")
      );
      gameLoop.doLoop();
      verify(eventQueue,times(4)).take();
      verify(gameContext,times(1)).sendTextToServer(any());
      verify(gameContext,times(2)).sendTextToClient(any());
      verify(gameContext,times(1)).notifyServerError(any());
      
      verifyNoMoreInteractions(gameContext,eventQueue);
    }
    
    @Test(timeout=500)
    public void serverErrorShouldTerminateLoop() throws Exception{
      when(eventQueue.take()).thenReturn(gameEventFactory.getServerErrorEvent("Error"));
      gameLoop.doLoop();
      verify(eventQueue,times(1)).take();
      verify(gameContext,times(1)).notifyServerError("Error");
      verifyNoMoreInteractions(gameContext,eventQueue);
    }
}

