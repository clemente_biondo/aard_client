package it.clem.mud.game;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.mock.RunnableGameLoopMock;
import it.clem.mud.protocol.data.handlers.NVTSupport;
import it.clem.mud.shared.game.GameStrategy;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.game.event.queue.GameEventQueueImpl;
import it.clem.mud.shared.recording.RecordQueue;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.streams.impl.TargetStreamImpl;
import it.clem.mud.shared.view.ClientCommunicationStrategy;

import static org.mockito.Mockito.*;
import static it.clem.mud.TestUtils.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MinimalRunnableGameLoopTest {
  @SuppressWarnings("unused")
  private final static Logger log =LoggerFactory.getLogger(MinimalRunnableGameLoopTest.class);
  
  private GameEventQueue eventBus; 

  private NVTSupport nvtSupport ;
  
  private ByteArrayOutputStream bout;
  
  @Before
  public void setUp() {
    eventBus = GameEventQueueImpl.builder().build();
    nvtSupport = NVTSupport.Minimal;
    bout = new ByteArrayOutputStream(16000);
  }

  /**
   * Simula la connessione ad un server che chiude immediatamente la connessione. 
   */
  @Test
  public void testLoopEndForNone() throws InterruptedException, IOException{
    SourceStream sourceStream = mock (SourceStream.class);
    when(sourceStream.read()).thenReturn(-1);
    RecordQueue recordQueue = mock(RecordQueue.class);
    ClientCommunicationStrategy clientCommStrategy = mock(ClientCommunicationStrategy.class);
    
    Thread loop= RunnableGameLoopMock.builder()
    .setEventBus(eventBus)
    .setNvtSupport(nvtSupport)
    .setGameStrategy(GameStrategy.None)
    .setClientStrategy(clientCommStrategy)
    .setStreams(sourceStream, new TargetStreamImpl(bout))
    .setRecordQueue(recordQueue)
    .buildThread();
    
    assertFalse("Il game loop non e' terminato",startAndWaitForCompletion(loop, 500));
    
    verify(clientCommStrategy,times(1)).notifyServerStarted();
    verify(sourceStream,times(1)).read();
    verify(sourceStream,times(1)).close();
    verify(clientCommStrategy,times(1)).notifyServerClosed();
    verifyNoMoreInteractions(sourceStream,clientCommStrategy);
    assertEquals(0, bout.toByteArray().length);
  }

  @Test
  public void testLoopEndForMinimal() throws InterruptedException, IOException{
    SourceStream sourceStream = mock (SourceStream.class);
    RecordQueue recordQueue = mock(RecordQueue.class);
    ClientCommunicationStrategy clientCommStrategy = mock(ClientCommunicationStrategy.class);
    when(sourceStream.read()).thenReturn(-1);

    Thread loop= RunnableGameLoopMock.builder()
    .setEventBus(eventBus)
    .setNvtSupport(nvtSupport)
    .setGameStrategy(GameStrategy.Minimal)
    .setClientStrategy(clientCommStrategy)
    .setStreams(sourceStream, new TargetStreamImpl(bout))
    .setRecordQueue(recordQueue)
    .buildThread();

    assertFalse("Il game loop non e' terminato",startAndWaitForCompletion(loop, 500));
    
    verify(sourceStream,times(1)).read();
    verify(sourceStream,times(1)).close();
    verify(clientCommStrategy,times(1)).notifyServerStarted();
    verify(clientCommStrategy,times(1)).notifyServerClosed();
    verifyNoMoreInteractions(sourceStream,clientCommStrategy);
    assertEquals(0, bout.toByteArray().length);
  }

  @Test
  public void testSendText() throws InterruptedException, IOException{
    SourceStream sourceStream = mock (SourceStream.class);
    ClientCommunicationStrategy clientCommStrategy = mock(ClientCommunicationStrategy.class);
    RecordQueue recordQueue = mock(RecordQueue.class);
    when(sourceStream.read()).thenReturn((int)'A',(int)'B',(int)'C',-1);
    
    Thread loop= RunnableGameLoopMock.builder()
    .setEventBus(eventBus)
    .setNvtSupport(nvtSupport)
    .setGameStrategy(GameStrategy.Minimal)
    .setClientStrategy(clientCommStrategy)
    .setStreams(sourceStream, new TargetStreamImpl(bout))
    .setRecordQueue(recordQueue)
    .buildThread();
    
    assertFalse("Il game loop non e' terminato",startAndWaitForCompletion(loop, 500));
    
    verify(sourceStream,times(4)).read();
    verify(sourceStream,times(1)).close();
    verify(clientCommStrategy,times(1)).notifyServerStarted();
    verify(clientCommStrategy).sendTextToClient("ABC");
    verify(clientCommStrategy,times(1)).notifyServerClosed();
    verifyNoMoreInteractions(sourceStream,clientCommStrategy);
    assertEquals(0, bout.toByteArray().length);
  }
  
  @Test
  public void testTwoLineText() throws InterruptedException, IOException{
    SourceStream sourceStream = mock (SourceStream.class);
    ClientCommunicationStrategy clientCommStrategy = mock(ClientCommunicationStrategy.class);
    RecordQueue recordQueue = mock(RecordQueue.class);
    when(sourceStream.read()).thenReturn((int)'A',(int)'B',(int)'C',(int)'\r',(int)'\n',(int)'d',-1);
    
    Thread loop= RunnableGameLoopMock.builder()
    .setEventBus(eventBus)
    .setNvtSupport(nvtSupport)
    .setGameStrategy(GameStrategy.Minimal)
    .setClientStrategy(clientCommStrategy)
    .setStreams(sourceStream, new TargetStreamImpl(bout))
    .setRecordQueue(recordQueue)
    .buildThread();
    
    assertFalse("Il game loop non e' terminato",startAndWaitForCompletion(loop, 500));
    
    InOrder vo=inOrder(clientCommStrategy);
    verify(sourceStream,times(7)).read();
    verify(sourceStream,times(1)).close();
    vo.verify(clientCommStrategy,times(1)).notifyServerStarted();
    vo.verify(clientCommStrategy,times(1)).sendTextToClient("ABC\r\n");
    vo.verify(clientCommStrategy,times(1)).sendTextToClient("d");
    vo.verify(clientCommStrategy,times(1)).notifyServerClosed();
    verifyNoMoreInteractions(sourceStream,clientCommStrategy);
    assertEquals(0, bout.toByteArray().length);
  }

  @Test
  public void testTwoLineAndTimeout() throws InterruptedException, IOException{
    SourceStream sourceStream = mock (SourceStream.class);
    RecordQueue recordQueue = mock(RecordQueue.class);

    ClientCommunicationStrategy clientCommStrategy = mock(ClientCommunicationStrategy.class);
    when(sourceStream.read()).thenReturn((int)'A',(int)'B',-2,(int)'C',(int)'\r',(int)'\n',(int)'d',-1);
    
    Thread loop= RunnableGameLoopMock.builder()
    .setEventBus(eventBus)
    .setNvtSupport(nvtSupport)
    .setGameStrategy(GameStrategy.Minimal)
    .setClientStrategy(clientCommStrategy)
    .setStreams(sourceStream, new TargetStreamImpl(bout))
    .setRecordQueue(recordQueue)
    .buildThread();
    
    assertFalse("Il game loop non e' terminato",startAndWaitForCompletion(loop, 500));
    
    InOrder vo=inOrder(clientCommStrategy);
    verify(sourceStream,times(8)).read();
    verify(sourceStream,times(1)).close();
    vo.verify(clientCommStrategy,times(1)).notifyServerStarted();
    vo.verify(clientCommStrategy,times(1)).sendTextToClient("AB");
    vo.verify(clientCommStrategy,times(1)).sendTextToClient("C\r\n");
    vo.verify(clientCommStrategy,times(1)).sendTextToClient("d");
    vo.verify(clientCommStrategy,times(1)).notifyServerClosed();
    verifyNoMoreInteractions(sourceStream,clientCommStrategy);
    assertEquals(0, bout.toByteArray().length);
  }

}
