package it.clem.mud.util;

import org.junit.Test;

import it.clem.mud.util.proxy_inspector.TelnetBufferPrinter;
import it.clem.mud.util.proxy_inspector.TelnetBufferPrinter.AnalysisResult;

import java.io.IOException;

import static it.clem.mud.TestUtils.*;
import static it.clem.mud.protocol.TelnetProtocolResources.*;
import static org.junit.Assert.*;

public class TelnetBufferPrinterTest {

  @Test
  public void alysisResult(){

    //formatter:off
    AnalysisResult ar = AnalysisResult.builder()
      .addCommandWithOption(WILL, ECHO)
      .addCommandWithOption(WILL, ECHO)
      .addCommandWithOption(WILL, NAWS)
      .addCommandWithOption(WONT, ECHO)
      .addCommandWithOption(WONT, ECHO)
      .addCommandWithOption(WONT, ECHO)
      .addCommandWithOption(SB, CMP1)
      .addCommandWithOption(SB, CMP2)
      .addCommandWithOption(SB, CMP2)
      .build();
    //formatter:on
    assertEquals(3, ar.getOptionCount(WONT, ECHO));
    assertEquals(1, ar.getOptionCount(SB, CMP1));
    assertEquals(0, ar.getOptionCount(DO, CMP1));
    assertEquals(0, ar.getOptionCount(WILL, CMP1));
    assertEquals(3, ar.countDistinctCodes());
    assertEquals(5, ar.countCodes());
    assertEquals(9, ar.count());
  }
  
  @Test
  public void alysisRealconv() throws IOException{
    TelnetBufferPrinter printer= new TelnetBufferPrinter();
    AnalysisResult ar= doWithInputStreamLoadedWithRealConversationUncompressed(printer::analyzeAard);
    assertEquals(3120+58+2, ar.getTextlines());//3120 sequenze LFCR 0A0D+ 58 sequenze 0D0A +2 text lines non seguite da lfcr
    assertEquals(239953,ar.getProtocolBytesRead()+ar.getTextBytesRead());
    assertEquals(202049,ar.getTextBytesRead());
    assertEquals(37904,ar.getProtocolBytesRead());
    //IAC SB GMCP FF FA C9 -> 255
    //IAC SB AARD FF FA 66 -> 40
    //IAC SB CMP2 ff fa 56 -> 1
    //IAC SB TERMINAL_TYPE ff fa 18 -> 1
    //IAC SB FF FA -> 255+ 40+2?  -> 297
    assertEquals(297,ar.getCodeCount(SB));
    assertEquals(255,ar.getOptionCount(SB,GMCP));
    assertEquals(40,ar.getOptionCount(SB,AARD));
    assertEquals(1,ar.getOptionCount(SB,TERMINAL_TYPE));
    assertEquals(1,ar.getOptionCount(SB,CMP2));
    assertEquals(297+10,ar.count());//Ogni subneg sono 2 IAC
    assertEquals(6,ar.getCodeCount(WILL));
    assertEquals(1,ar.getCodeCount(WONT));
    assertEquals(3,ar.getCodeCount(DO));
    assertEquals(0,ar.getCodeCount(DONT));
    assertEquals(1,ar.getOptionCount(WONT,ECHO));

    //Nel file non ci sono IAC IAC. Sono presenti 604 FF
    //di questi 594 IAC sono dei 297 subneg
    //6 IAC WILL FF FB
    //1 IAC WONT FF FC
    //3 IAC DO   FF FD
    //0 IAC DONT FF FE

    //208 0D FF
    //
    //[^\x0D^\xF0]\xFF[^\xF0] -> 307
    //
  }
  
}
