package it.clem.mud.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimedInputStream extends FilterInputStream {
  private final int millis = 100;
  private final static Logger log=LoggerFactory.getLogger(TimedInputStream.class);

  public TimedInputStream(InputStream in) {
    super(in);
  }

  @Override
  public int read() throws IOException {
    int b= super.read();
    if (b=='\n') {
      try {
        Thread.sleep(millis);
      } catch (InterruptedException e) {
        log.error(e.getMessage(),e);
      }
    }
    return b;
  }
  
}
