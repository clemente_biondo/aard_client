package it.clem.mud.mock;

import it.clem.mud.protocol.data.AbleToIgnoreTelnetData;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.loop.AardTelnetContextImpl;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.shared.game.event.queue.ServerEventProducer;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;

public class AardTelnetContextMock extends AardTelnetContextImpl {
  private final AbleToIgnoreTelnetData ignorable;

  public AardTelnetContextMock(ServerEventProducer eventQueue, TelnetReader receiver, TelnetWriter sender,
      TelnetDataFactory factory, AbleToIgnoreTelnetData ignorable) {
    super(eventQueue, receiver, sender, factory);
    this.ignorable = ignorable;
  }

  public AardTelnetContextMock(ServerEventProducer eventQueue, TelnetReader receiver, TelnetWriter sender,
      TelnetDataFactory factory) {
    this(eventQueue, receiver, sender, factory, null);
  }

  @Override
  public void ignore(TelnetData data) {
    if (ignorable != null) {
      ignorable.ignore(data);
    } else {
      super.ignore(data);
    }

  }

}
