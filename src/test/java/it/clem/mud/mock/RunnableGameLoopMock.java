package it.clem.mud.mock;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.tuple.Pair;

import it.clem.mud.gedt.RunnableGameLoop;
import it.clem.mud.gedt.event.bus.GameEventBus;
import it.clem.mud.gedt.socket.AbstractSocket;
import it.clem.mud.gedt.socket.impl.AbstractSocketImpl;
import it.clem.mud.protocol.data.handlers.NVTSupport;
import it.clem.mud.shared.game.GameStrategy;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.recording.GameEventThreadDispatcherProducer;
import it.clem.mud.shared.recording.RecordQueue;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.streams.TargetStream;
import it.clem.mud.shared.telnet.writer.TelnetWriter;
import it.clem.mud.shared.view.ClientCommunicationStrategy;
import it.clem.mud.util.HostWithPort;

public class RunnableGameLoopMock extends RunnableGameLoop {
  private final TelnetWriter telnetSender;
  private final GameEventBus gameEventBus;
  private final AbstractSocket abstractSocket;
  private final Pair<SourceStream, TargetStream> streams;

  public RunnableGameLoopMock(HostWithPort server, GameEventQueue eventBus, NVTSupport nvtSupport,
      GameStrategy gameStrategy, ClientCommunicationStrategy clientStrategy, RecordQueue recordQueue,
      TelnetWriter telnetSender, GameEventBus gameEventBus, AbstractSocket abstractSocket,
      Pair<SourceStream, TargetStream> streams) {
    super(server, eventBus, nvtSupport, gameStrategy, clientStrategy, recordQueue);
    this.telnetSender = telnetSender;
    this.gameEventBus = gameEventBus;
    this.abstractSocket = abstractSocket;
    this.streams = streams;
  }

  @Override
  protected TelnetWriter getTelnetSerder(TargetStream targetStream, GameEventThreadDispatcherProducer recordQueue) {
    if (telnetSender != null) {
      return telnetSender;
    }
    return super.getTelnetSerder(targetStream, recordQueue);
  }

  @Override
  protected GameEventBus getGameEventListenerBus(GameStrategy gameStrategy) {
    if (gameEventBus != null) {
      return gameEventBus;
    }
    return super.getGameEventListenerBus(gameStrategy);
  }

  @Override
  protected AbstractSocket getSocket(HostWithPort server, File recordingDirectory) throws IOException {
    if (abstractSocket != null) {
      return abstractSocket;
    }
    if (streams != null) {
      return new AbstractSocketImpl(streams.getLeft(), streams.getRight());
    }
    return super.getSocket(server, recordingDirectory);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private HostWithPort server;
    private GameEventQueue eventBus;
    private NVTSupport nvtSupport;
    private GameStrategy gameStrategy;
    private ClientCommunicationStrategy clientStrategy;
    private RecordQueue recordQueue;
    private TelnetWriter telnetSender;
    private GameEventBus gameEventBus;
    private AbstractSocket abstractSocket;
    private Pair<SourceStream, TargetStream> streams;

    public Builder setServer(HostWithPort server) {
      this.server = server;
      return this;
    }

    public Builder setEventBus(GameEventQueue eventBus) {
      this.eventBus = eventBus;
      return this;
    }

    public Builder setNvtSupport(NVTSupport nvtSupport) {
      this.nvtSupport = nvtSupport;
      return this;
    }

    public Builder setGameStrategy(GameStrategy gameStrategy) {
      this.gameStrategy = gameStrategy;
      return this;
    }

    public Builder setClientStrategy(ClientCommunicationStrategy clientStrategy) {
      this.clientStrategy = clientStrategy;
      return this;
    }

    public Builder setRecordQueue(RecordQueue recordQueue) {
      this.recordQueue = recordQueue;
      return this;
    }

    public Builder setTelnetSender(TelnetWriter telnetSender) {
      this.telnetSender = telnetSender;
      return this;
    }

    public Builder setGameEventBus(GameEventBus gameEventBus) {
      this.gameEventBus = gameEventBus;
      return this;
    }

    public Builder setAbstractSocket(AbstractSocket abstractSocket) {
      this.abstractSocket = abstractSocket;
      return this;
    }

    public Builder setStreams(SourceStream ss, TargetStream ts) {
      this.streams = Pair.of(ss, ts);
      return this;
    }

    public RunnableGameLoop build() {
      return new RunnableGameLoopMock(server, eventBus, nvtSupport, gameStrategy, clientStrategy, recordQueue,
          telnetSender, gameEventBus, abstractSocket, streams);
    }

    public Thread buildThread() {
      return new Thread(build(), RunnableGameLoop.THREAD_NAME);
    }
  }
}
