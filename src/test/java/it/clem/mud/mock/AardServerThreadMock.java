package it.clem.mud.mock;

import it.clem.mud.protocol.AardServerThread;
import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.handlers.NVTSupport;
import it.clem.mud.protocol.data.handlers.TelnetDataHandlerBus;
import it.clem.mud.protocol.loop.AardTelnetContextImpl;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.shared.game.event.queue.ServerEventProducer;
import it.clem.mud.shared.recording.ServerRecordProducer;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;

public class AardServerThreadMock extends AardServerThread {
  private final TelnetReader telnetReceiver;
  private final AardTelnetContext aardTelnetContext;
  private final TelnetDataHandlerBus telnetDataHandlerBus;

  public AardServerThreadMock(ServerRecordProducer recordQueue, ServerEventProducer eventSender,
      SourceStream sourceStream, TelnetWriter sender, NVTSupport nvtSupport, TelnetReader telnetReceiver,
      AardTelnetContext aardTelnetContext, TelnetDataHandlerBus telnetDataHandlerBus,TelnetDataFactory factory) {
    super(recordQueue, eventSender, sourceStream, sender, nvtSupport,factory);
    this.telnetReceiver = telnetReceiver;
    this.aardTelnetContext = aardTelnetContext;
    this.telnetDataHandlerBus = telnetDataHandlerBus;
  }

  @Override
  protected TelnetReader getTelnetReceiver(SourceStream sourceStream, ServerRecordProducer recordQueue,TelnetDataFactory factory) {
    if (telnetReceiver != null) {
      return telnetReceiver;
    }
    return super.getTelnetReceiver(sourceStream, recordQueue,factory);
  }

  @Override
  protected AardTelnetContext getAardTelnetContext(ServerRecordProducer recordQueue, ServerEventProducer eventSender,
      TelnetReader receiver, TelnetWriter sender,TelnetDataFactory factory) {
    if (aardTelnetContext != null) {
      return aardTelnetContext;
    }
    if(factory!= null) {
      return new AardTelnetContextImpl(eventSender, receiver, sender, factory);  
    }
    return super.getAardTelnetContext(recordQueue, eventSender, receiver, sender,factory);
  }

  @Override
  protected TelnetDataHandlerBus getTelnetDataHandlerBus(NVTSupport client) {
    if (telnetDataHandlerBus != null) {
      return telnetDataHandlerBus;
    }
    return super.getTelnetDataHandlerBus(client);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private ServerRecordProducer recordQueue;
    private ServerEventProducer eventSender;
    private SourceStream sourceStream;
    private TelnetWriter sender;
    private NVTSupport nvtSupport;
    private TelnetReader telnetReceiver;
    private AardTelnetContext aardTelnetContext;
    private TelnetDataHandlerBus telnetDataHandlerBus;
    private TelnetDataFactory factory;
    
    public Builder setTelnetDataFactory(TelnetDataFactory factory) {
      this.factory = factory;
      return this;
    }

    public Builder setServerRecordProducer(ServerRecordProducer recordQueue) {
      this.recordQueue = recordQueue;
      return this;
    }

    public Builder setServerEventProducer(ServerEventProducer eventSender) {
      this.eventSender = eventSender;
      return this;
    }

    public Builder setSourceStream(SourceStream sourceStream) {
      this.sourceStream = sourceStream;
      return this;
    }

    public Builder setTelnetSender(TelnetWriter sender) {
      this.sender = sender;
      return this;
    }

    public Builder setNvtSupport(NVTSupport nvtSupport) {
      this.nvtSupport = nvtSupport;
      return this;
    }

    public Builder setTelnetReceiver(TelnetReader telnetReceiver) {
      this.telnetReceiver = telnetReceiver;
      return this;
    }

    public Builder setAardTelnetContext(AardTelnetContext aardTelnetContext) {
      this.aardTelnetContext = aardTelnetContext;
      return this;
    }

    public Builder setTelnetDataHandlerBus(TelnetDataHandlerBus telnetDataHandlerBus) {
      this.telnetDataHandlerBus = telnetDataHandlerBus;
      return this;
    }

    public AardServerThread build() {
      return new AardServerThreadMock(recordQueue, eventSender, sourceStream, sender, nvtSupport, telnetReceiver,
          aardTelnetContext, telnetDataHandlerBus,factory);
    }
  }
}
