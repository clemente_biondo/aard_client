package it.clem.mud.protocol;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetText;
import it.clem.mud.protocol.data.handlers.TelnetDataHandlerBus;
import it.clem.mud.protocol.data.handlers.impl.NotifyTextFromAardwolf;
import it.clem.mud.protocol.data.handlers.impl.TelnetDataHandlerByTypeBaseImpl;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.protocol.telnet.reader.impl.TelnetReaderImpl;
import it.clem.mud.shared.streams.impl.SourceStreamImpl;
import it.clem.mud.shared.telnet.TelnetDataFactory;

import static org.mockito.Mockito.*;
import static it.clem.mud.TestUtils.*;

public class MapperTagTest {
  @SuppressWarnings("unused")
  private final static Logger log = LoggerFactory.getLogger(MapperTagTest.class);
  private TelnetDataFactory   factory;

  @Before
  public void setUp() {
    factory = new TelnetDataFactory();
  }

  @Test
  public void testCountOnRealFlux() throws IOException {
    try (TelnetReader recv = new TelnetReaderImpl(
        new SourceStreamImpl(getBufferedInputStream(ENTIRE_CONVERSATION_XTERM_DUE)),new TelnetDataFactory())) {
      TelnetData data;
      int mapStartCount = 0;
      int mapEndCount = 0;
      while (!(data = recv.read()).itDenotesConnectionClosedByServer()) {
        if (data.sameOf(factory.getStartCompressionSubneg())) {
          recv.startCompression();
        }
        if (data instanceof TelnetText) {
          TelnetText tt = (TelnetText) data;
          if (tt.isMapStart()) {
            mapStartCount++;
          } else if (tt.isMapEnd()) {
            mapEndCount++;
          }
        }
      }
      assertEquals(76, mapStartCount);
      assertEquals(mapEndCount, mapStartCount);
    }

  }
  
  @Test(timeout = 3000)
  public void testMapContent() throws IOException {
    AardTelnetContext context = mock(AardTelnetContext.class);
    TelnetDataHandlerByTypeBaseImpl<TelnetText> th = new  NotifyTextFromAardwolf();
    TelnetDataFactory f=new TelnetDataFactory();
    th.handle(f.getTelnetText("abc"), context);
    th.handle(f.getTelnetText(TelnetText.MAP_START_TAG), context);
    th.handle(f.getTelnetText("def"), context);
    th.handle(f.getTelnetText("123"), context);
    th.handle(f.getTelnetText(TelnetText.MAP_END_TAG), context);
    th.handle(f.getTelnetText("456"), context);
    InOrder ov = inOrder(context);
    ov.verify(context,times(1)).fireTextSentFromServerEvent(eq("abc"));
    ov.verify(context,times(1)).fireMapTagUpdated(eq("def123"));
    ov.verify(context,times(1)).fireTextSentFromServerEvent(eq("456"));
    verifyNoMoreInteractions(context);
  }
  
  @Test(timeout = 3000)
  public void testEventProduction() throws IOException {
    Thread.currentThread().setName(AardServerThread.THREAD_NAME);
    AardTelnetContext context = mock(AardTelnetContext.class);
    @SuppressWarnings("resource")
    TelnetReader recv = new TelnetReaderImpl(
        new SourceStreamImpl(getBufferedInputStream(ENTIRE_CONVERSATION_XTERM_DUE)),new TelnetDataFactory());
    doAnswer(i -> {
      recv.startCompression();
      return null;
    }).when(context).startCompression();
    TelnetDataHandlerBus client = TelnetDataHandlerBus.getAardClient();
    TelnetData data;

    while (!(data = recv.read()).itDenotesConnectionClosedByServer()) {
      client.handle(data, context);
    }
    
    verify(context,times(76)).fireMapTagUpdated(any());
  }
}
