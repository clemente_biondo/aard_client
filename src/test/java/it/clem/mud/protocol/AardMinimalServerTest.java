package it.clem.mud.protocol;


import java.io.ByteArrayOutputStream;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.mock.AardServerThreadMock;
import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.handlers.TelnetDataHandlerBus;
import it.clem.mud.protocol.telnet.reader.impl.TelnetReaderImpl;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.streams.TargetStream;
import it.clem.mud.shared.streams.impl.TargetStreamImpl;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;
import it.clem.mud.shared.telnet.writer.impl.TelnetWriterImpl;
import it.clem.mud.util.LangUtils;
import it.clem.mud.util.proxy_inspector.TelnetBufferPrinter;
import it.clem.mud.util.proxy_inspector.TelnetBufferPrinter.AnalysisResult;

import static org.mockito.AdditionalAnswers.*;
import static org.mockito.Mockito.*;
import static it.clem.mud.TestUtils.*;
import static it.clem.mud.protocol.TelnetProtocolResources.*;
import static org.junit.Assert.*;

/**
 * 
 * Behavioural test del minimal client telnet. Creo i casi (when con il receiver) e ne verifico l'esito (verify sul sender). Da notare che gli eventi che il
 * server invia non vengono ricevuti da nessuno, cosi' come nessuno invia eventi dal client. In sostanza è un terminale remoto perfettamente funzionante ma in
 * cui l'output va su dev null e nessuno scrive alla tastiera( che non è nemmeno collegata).
 *
 */
// Non funziona nel contesto di più thread e più junit ( colpa di eclipse?)
@RunWith(MockitoJUnitRunner.class)
public class AardMinimalServerTest {
  private final static Logger log = LoggerFactory.getLogger(AardMinimalServerTest.class);

  // Non funziona nel contesto di più thread e più junit ( colpa di eclipse?)
  // @Rule
  // public final TestRule globalTimeout = Timeout.millis(2000);
  //
  // @Mock
  // private volatile GameEventQueue queue;
  // @Mock
  // private volatile SourceStream sourceStream;
  // private volatile ByteArrayOutputStream out;
  // private volatile AardRunnableServer server;
  // @Before
  // public void setUp() {
  // out = new ByteArrayOutputStream(16000);
  // server = AardRunnableServer.forTest(queue, TelnetDataHandlerBus.getMinimalClient(), new TelnetSenderImpl(out), sourceStream);
  // }

  @Test(timeout = 3000)
  public void testClosed() throws Exception {
    AardServerThread server = null;
    try {
      log.debug("inizio testClosed");
      SourceStream sourceStream = mock(SourceStream.class);
      ByteArrayOutputStream bout = new ByteArrayOutputStream(16000);
      TargetStream ts = new TargetStreamImpl(bout);
      GameEventQueue queue = mock(GameEventQueue.class);
      server = AardServerThreadMock.builder()
          .setServerEventProducer(queue)
          .setTelnetDataHandlerBus(TelnetDataHandlerBus.getMinimalClient())
          .setTelnetSender(new TelnetWriterImpl(ts))
          .setTelnetReceiver(new TelnetReaderImpl(sourceStream,new TelnetDataFactory()))
          .setTelnetDataFactory(new TelnetDataFactory()).build();
      // carico il comando di chiusura
      when(sourceStream.read()).thenReturn(SourceStream.STREAM_CLOSED);

      assertFalse("Il game loop non e' terminato", startAndWaitForCompletion(server, 1500));
      ts.flush();
      assertEquals(bout.toByteArray().length, 0);
      verify(queue, times(1)).fireConnectionClosedByServerEvent();
      verifyNoMoreInteractions(queue);
    } catch (Exception ex) {
      log.debug(ex.getMessage(), ex);
      throw ex;
    } finally {
      log.debug(
          server == null ? "aard server ancora non creato!" : (server.getName() + "state:" + server.getState().name()));
      log.debug("this :" + Thread.currentThread().getState().name());
      log.debug("fine testClosed");
    }
  }

  // se faccio sunas junit sulla dir l'interrupt di close thread chiude anche questo, perche' sono in parallelo!
  @Test(timeout = 3000)
  public void testText() throws Exception {
    AardServerThread server = null;
    try {
      log.debug("inizio testText");
      SourceStream sourceStream = mock(SourceStream.class);
      ByteArrayOutputStream bout = new ByteArrayOutputStream(16000);
      TargetStream ts = new TargetStreamImpl(bout);
      GameEventQueue queue = mock(GameEventQueue.class);
      server = AardServerThreadMock.builder()
          .setServerEventProducer(queue)
          .setTelnetDataHandlerBus(TelnetDataHandlerBus.getMinimalClient())
          .setTelnetSender(new TelnetWriterImpl(ts))
          .setTelnetReceiver(new TelnetReaderImpl(sourceStream,new TelnetDataFactory()))
          .setTelnetDataFactory(new TelnetDataFactory()).build();
      //@formatter:off
      when(sourceStream.read())
        .thenReturn(65/* A */)
        .thenReturn(66/* B */)
        .thenReturn(67/* C */)
        .thenReturn(SourceStream.STREAM_CLOSED);
      //@formatter:on
      assertFalse("Il game loop non e' terminato", startAndWaitForCompletion(server, 1500));
      ts.flush();
      assertEquals(bout.toByteArray().length, 0);
      verify(queue, times(1)).fireTextSentFromServerEvent(argThat(s -> s.equals("ABC")));
      verify(queue, times(1)).fireConnectionClosedByServerEvent();
      verifyNoMoreInteractions(queue);
    } catch (Exception ex) {
      log.debug(ex.getMessage(), ex);
      throw ex;
    } finally {
      log.debug(
          server == null ? "aard server ancora non creato!" : (server.getName() + "state:" + server.getState().name()));
      log.debug("this :" + Thread.currentThread().getState().name());
      log.debug("fine testText");
    }
  }

  @Test(timeout = 3000)
  public void testAArdHandshake() throws Exception {
    AardServerThread server = null;
    try {
      log.debug("inizio testAArdHandshake");
      ByteArrayOutputStream bout = new ByteArrayOutputStream(16000);
      TargetStream ts = new TargetStreamImpl(bout);
      GameEventQueue queue = mock(GameEventQueue.class);
      SourceStream sourceStream = getSourceStreamLoadedWithAArdHandshake();
      server = AardServerThreadMock.builder()
          .setServerEventProducer(queue)
          .setTelnetDataHandlerBus(TelnetDataHandlerBus.getMinimalClient())
          .setTelnetSender(new TelnetWriterImpl(ts))
          .setTelnetReceiver(new TelnetReaderImpl(sourceStream,new TelnetDataFactory()))
          .setTelnetDataFactory(new TelnetDataFactory()).build();
      assertFalse("Il game loop non e' terminato", startAndWaitForCompletion(server, 1500));
      ts.flush();
      //@formatter:off
      assertByteArrEquals(bout.toByteArray(), LangUtils.arrOfIntToArrOfByte(
    		  IAC,DONT, CMP2,
    		  IAC,DONT, CMP1,
    		  IAC,DONT, AARD,
    		  IAC,DONT, ATCP,
    		  IAC,DONT, GMCP,
    		  IAC,WONT, AARD,
    		  IAC,WONT, TERMINAL_TYPE,
    		  IAC,WONT, NAWS    		  
    		  ));
      //@formatter:on
      verify(queue, times(1)).fireConnectionClosedByServerEvent();
      verifyNoMoreInteractions(queue);
    } catch (Exception ex) {
      log.debug(ex.getMessage(), ex);
      throw ex;
    } finally {
      log.debug(
          server == null ? "aard server ancora non creato!" : (server.getName() + "state:" + server.getState().name()));
      log.debug("this :" + Thread.currentThread().getState().name());
      log.debug("fine testAArdHandshake");
    }
  }
  
  @Test(timeout = 10000)
  public void testLongConversation() throws Exception {
    AardServerThread server = null;
    try {
      log.debug("inizio testAArdHandshake");
      TelnetBufferPrinter printer= new TelnetBufferPrinter();
      AnalysisResult ar= doWithInputStreamLoadedWithRealConversationUncompressed(printer::analyzeAard);
      GameEventQueue queue = mock(GameEventQueue.class);
      TelnetWriter sender = mock(TelnetWriter.class);

      AardTelnetContext tc = mock(AardTelnetContext.class);
      
      final AtomicInteger textBytesCount=new AtomicInteger(0);       
      final AtomicInteger zeroLenLines=new AtomicInteger(0);       
      final AtomicInteger notLFCRLines=new AtomicInteger(0);
      doAnswer(answerVoid((String text)->{
        textBytesCount.addAndGet(text.length());
        if(text.length()== 0){
          zeroLenLines.incrementAndGet();
        } else if(text.lastIndexOf('\r')!=text.length()-1&&text.lastIndexOf('\n')!=text.length()-1){
          notLFCRLines.incrementAndGet();
        }
      })).when(tc).fireTextSentFromServerEvent(any());//Sintassi per void

      SourceStream sourceStream = getSourceStreamLoadedWithRealConversationUncompressed();
      server = AardServerThreadMock.builder()
        .setServerEventProducer(queue)
        .setTelnetDataHandlerBus(TelnetDataHandlerBus.getMinimalClient())
        .setTelnetSender(sender)
        .setTelnetReceiver(new TelnetReaderImpl(sourceStream,new TelnetDataFactory()))
        .setAardTelnetContext(tc).build();

      assertFalse("Il game loop non e' terminato", startAndWaitForCompletion(server, 5000));
      
      assertEquals(0, zeroLenLines.get());
      assertEquals(2, notLFCRLines.get());
      assertEquals(ar.getTextBytesRead(), textBytesCount.get());
      verify(tc, times(8)).replyWithRefusalOf(any());
      verify(tc, times(2)).replyWithConfirmationOf(any());
      verify(tc, times(ar.count()-8-2)).ignore(any());
      verify(tc, times(1)).fireConnectionClosedByServerEvent();
      verify(tc, times(ar.getTextlines())).fireTextSentFromServerEvent(anyString());
      verifyNoMoreInteractions(queue);
    } catch (Exception ex) {
      log.debug(ex.getMessage(), ex);
      throw ex;
    } finally {
      log.debug(
          server == null ? "aard server ancora non creato!" : (server.getName() + "state:" + server.getState().name()));
      log.debug("this :" + Thread.currentThread().getState().name());
      log.debug("fine testAArdHandshake");
    }
  }
}
