package it.clem.mud.protocol;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;
import static it.clem.mud.protocol.TelnetProtocolResources.*;
import it.clem.mud.protocol.data.TelnetConnectionClosedByServer;
import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetDo;
import it.clem.mud.protocol.data.TelnetDont;
import it.clem.mud.protocol.data.TelnetSubnegotiation;
import it.clem.mud.protocol.data.TelnetWill;
import it.clem.mud.protocol.data.handlers.TelnetDataHandlerBus;
import it.clem.mud.shared.telnet.TelnetDataFactory;

/**
 * 
 * Verifico che il client telnet stateless minimale risponda correttamente a tutte le possibili richiesta.
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class MinimalClientTest {
  
  private TelnetDataHandlerBus client;

  private TelnetDataFactory             factory;

  @After
  public void tearDown() {
    Thread.currentThread().setName("main");
  }
  
  @Before
  public void setUp() {
    Thread.currentThread().setName(AardServerThread.THREAD_NAME);
    client = TelnetDataHandlerBus.getMinimalClient();
    factory = new TelnetDataFactory();
  }

  @Test
  public void testClosed() throws IOException {
    testClosedForClient(client);
  }

  public static void testClosedForClient(TelnetDataHandlerBus client) {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(TelnetConnectionClosedByServer.getInstance(), mockTelnetContext);
    verify(mockTelnetContext, only()).fireConnectionClosedByServerEvent();
  }

  @Test
  public void testText() throws IOException {
    testTextForClient(client);
  }

  public static void testTextForClient(TelnetDataHandlerBus client) {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    InOrder orderVerifier = inOrder(mockTelnetContext);
    TelnetDataFactory f=new TelnetDataFactory();
    client.handle(f.getTelnetText("abc"), mockTelnetContext);
    client.handle(f.getTelnetText("123"), mockTelnetContext);
    orderVerifier.verify(mockTelnetContext, times(1)).fireTextSentFromServerEvent(argThat(text -> text.equals("abc")));
    orderVerifier.verify(mockTelnetContext, times(1)).fireTextSentFromServerEvent(argThat(text -> text.equals("123")));
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testCommandsNotSupported() throws IOException {
    testCommandsNotSupportedForClient(client,factory);
  }

  @Test
  public void testSBNotSupported() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getCommand(SB), mockTelnetContext);
    verify(mockTelnetContext, times(1)).signalAsNotSupported(any());
    verifyNoMoreInteractions(mockTelnetContext);
  }

  public static void testCommandsNotSupportedForClient(TelnetDataHandlerBus client,TelnetDataFactory factory) {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getCommand(AO), mockTelnetContext);
    client.handle(factory.getCommand(SE), mockTelnetContext);
    client.handle(factory.getCommand(NOP), mockTelnetContext);
    client.handle(factory.getCommand(DM), mockTelnetContext);
    client.handle(factory.getCommand(BRK), mockTelnetContext);
    client.handle(factory.getCommand(IP), mockTelnetContext);
    client.handle(factory.getCommand(AO), mockTelnetContext);
    client.handle(factory.getCommand(AYT), mockTelnetContext);
    client.handle(factory.getCommand(EC), mockTelnetContext);
    client.handle(factory.getCommand(EL), mockTelnetContext);
    //client.handle(factory.getCommand(GA), mockTelnetContext);
    verify(mockTelnetContext, times(10)).signalAsNotSupported(any());
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testEcho() throws IOException {
    testEchoForClient(client,factory);
  }

  public static void testEchoForClient(TelnetDataHandlerBus client,TelnetDataFactory factory) {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getWill(ECHO), mockTelnetContext);
    //client.handle(factory.getDo(ECHO), mockTelnetContext);
    verify(mockTelnetContext, times(1)).replyWithConfirmationOf(eq(factory.getWill(ECHO))); 
    //verify(mockTelnetContext, times(1)).replyWithConfirmationOf(eq(factory.getDo(ECHO)));
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testDontWont() throws IOException {
    testDontWontForClient(client, factory);  
  }
  
  public static void testDontWontForClient(TelnetDataHandlerBus client,TelnetDataFactory factory) throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getWont(CMP2), mockTelnetContext);
    client.handle(factory.getWont(TERMINAL_TYPE), mockTelnetContext);
    client.handle(factory.getWont(NAWS), mockTelnetContext);
    client.handle(factory.getDont(CMP2), mockTelnetContext);
    client.handle(factory.getDont(CMP1), mockTelnetContext);
    client.handle(factory.getDont(AARD), mockTelnetContext);
    client.handle(factory.getDont(GMCP), mockTelnetContext);
    client.handle(factory.getDont(ATCP), mockTelnetContext);
    client.handle(factory.getWont(123), mockTelnetContext);
    client.handle(factory.getDont(124), mockTelnetContext);
    verify(mockTelnetContext, times(10)).replyWithConfirmationOf(any());
    verify(mockTelnetContext, times(1)).replyWithConfirmationOf(argThat(e -> e instanceof TelnetDont && ((TelnetDont) e).getOption() == 124));
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testOtherWill() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getWill(CMP2), mockTelnetContext);
    client.handle(factory.getWill(CMP1), mockTelnetContext);
    client.handle(factory.getWill(AARD), mockTelnetContext);
    client.handle(factory.getWill(ATCP), mockTelnetContext);
    client.handle(factory.getWill(GMCP), mockTelnetContext);
    client.handle(factory.getWill(125), mockTelnetContext);
    verify(mockTelnetContext, times(6)).replyWithRefusalOf(argThat(e -> e instanceof TelnetWill));
    verify(mockTelnetContext, times(1)).replyWithRefusalOf(argThat(e -> e instanceof TelnetWill && ((TelnetWill) e).getOption() == 125));
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testOtherDo() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getDo(CMP2), mockTelnetContext);
    client.handle(factory.getDo(TERMINAL_TYPE), mockTelnetContext);
    client.handle(factory.getDo(NAWS), mockTelnetContext);
    client.handle(factory.getDo(CMP1), mockTelnetContext);
    client.handle(factory.getDo(AARD), mockTelnetContext);
    client.handle(factory.getDo(ATCP), mockTelnetContext);
    client.handle(factory.getDo(GMCP), mockTelnetContext);
    client.handle(factory.getDo(126), mockTelnetContext);
    verify(mockTelnetContext, times(8)).replyWithRefusalOf(argThat(e -> e instanceof TelnetDo));
    verify(mockTelnetContext, times(1)).replyWithRefusalOf(argThat(e -> {
      System.out.println(e);
      return e instanceof TelnetDo && ((TelnetDo) e).getOption() == 126;
    }));
    verifyNoMoreInteractions(mockTelnetContext);
  }
  
  @Test
  public void testGA() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getGA(), mockTelnetContext);
    verify(mockTelnetContext, times(1)).ignore(any());
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testSubneg() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getStartCompressionSubneg(), mockTelnetContext);
    client.handle(factory.getSendTerminal(), mockTelnetContext);
    client.handle(factory.getSubnegotionation(127, new byte[] { 1, 2, 3 }), mockTelnetContext);
    verify(mockTelnetContext, times(3)).ignore(any(TelnetSubnegotiation.class));
    verifyNoMoreInteractions(mockTelnetContext);
  }
}
