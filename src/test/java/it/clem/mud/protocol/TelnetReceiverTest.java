package it.clem.mud.protocol;

import java.io.BufferedInputStream;
import java.io.IOException;
import org.apache.commons.io.HexDump;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.TestUtils;
import it.clem.mud.protocol.data.TelnetConnectionClosedByServer;
import it.clem.mud.protocol.data.TelnetDataType;
import it.clem.mud.protocol.data.TelnetGA;
import it.clem.mud.protocol.data.TelnetData;
import it.clem.mud.protocol.data.TelnetText;
import it.clem.mud.protocol.telnet.reader.SkipableTelnetReader;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.protocol.telnet.reader.impl.SkipableTelnetReaderImpl;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import junit.framework.AssertionFailedError;

import static org.junit.Assert.*;
import static it.clem.mud.protocol.TelnetProtocolResources.*;
import static it.clem.mud.TestUtils.*;

/**
 * 
 * Verifico che il receiver interpreti correttamente il traffico in entrata.
 *
 */
public class TelnetReceiverTest {
  private final static Logger log =LoggerFactory.getLogger(TelnetReceiverTest.class);
  private TelnetDataFactory factory;

  @Before
  public void setUp() {
    factory = new TelnetDataFactory();
  }
  
  @Test
  public void testGA() throws IOException {
    TelnetDataFactory f=new TelnetDataFactory();

    //abc IAC GA cde 
    TelnetReader recv = getTelnetReceiver(97, 98, 99,100);
    assertEquals(f.getTelnetText("abcd"), recv.read());
    recv = getTelnetReceiver(97, 98,IAC,GA, 99,100);
    assertEquals(f.getTelnetText("ab"), recv.read());
    TelnetData td=recv.read();
    assertTrue(td instanceof TelnetGA);
    assertEquals(TelnetDataType.GA_CMD, ((TelnetGA)td).getType());
    assertEquals(1, ((TelnetGA)td).getSeqId());
    assertEquals(new TelnetGA(0),td);
    assertEquals(f.getTelnetText("cd"), recv.read());
  }
  
  @Test
  public void testCaratteriSpeciali() throws IOException {
    TelnetReader recv = getTelnetReceiverLoadedWithSpecialChars();
    TestUtils.assertByteArrEquals(CARATTERI_SPECIALI ,recv.read().getBytes());
  }
  
  @Test
  public void readChar255() throws IOException {
    TelnetDataFactory f=new TelnetDataFactory();
    TelnetReader recv = getTelnetReceiver(CHAR_255);
    assertEquals(f.getTelnetText("\u00FF"), recv.read());
    assertEquals(TelnetConnectionClosedByServer.getInstance(), recv.read());
    assertEquals(TelnetConnectionClosedByServer.getInstance(), recv.read());
    assertEquals(TelnetConnectionClosedByServer.getInstance(), recv.read());
  }

  @Test
  public void readInitialData() throws IOException {
    TelnetReader recv = getTelnetReceiverLoadedWithAArdHandshake();
    assertEquals(factory.getWill(CMP2), recv.read());
    assertEquals(factory.getWill(CMP1), recv.read());
    assertEquals(factory.getWill(AARD), recv.read());
    assertEquals(factory.getWill(ATCP), recv.read());
    assertEquals(factory.getWill(GMCP), recv.read());
    assertEquals(factory.getDo(AARD), recv.read());
    assertEquals(factory.getDo(TERMINAL_TYPE), recv.read());
    assertEquals(factory.getDo(NAWS), recv.read());
    assertEquals(TelnetConnectionClosedByServer.getInstance(), recv.read());
    assertEquals(TelnetConnectionClosedByServer.getInstance(), recv.read());
    assertEquals(TelnetConnectionClosedByServer.getInstance(), recv.read());
  }

  @Test
  public void readFirstScreenData() throws IOException {
    TelnetDataFactory f=new TelnetDataFactory();
    TelnetReader recv = getTelnetReceiverLoadedWithFirstScreen();
    try(SkipableTelnetReader sr = new SkipableTelnetReaderImpl(recv)){
    assertEquals(factory.getWill(CMP2), recv.read());
    assertEquals(factory.getWill(CMP1), recv.read());
    assertEquals(factory.getWill(AARD), recv.read());
    assertEquals(factory.getWill(ATCP), recv.read());
    assertEquals(factory.getWill(GMCP), recv.read());
    assertEquals(factory.getDo(AARD), recv.read());
    assertEquals(factory.getDo(TERMINAL_TYPE), recv.read());
    assertEquals(factory.getDo(NAWS), recv.read());
    assertEquals(f.getTelnetText("#############################################################################"+LFCR_AARD_EOL),
        recv.read());
    assertTrue(sr.readUntilTextContains("Disconnecting").received instanceof TelnetText);
    }
  }

  @Test
  public void compressione() throws IOException {
    TelnetDataFactory f=new TelnetDataFactory();
    TelnetReader recv = getTelnetReceiver(TelnetProtocolResources.SUBNEG_START_COMPRESSION,ABC_COMPRESSED);
    assertEquals(factory.getStartCompressionSubneg(), recv.read());
    recv.startCompression();
    assertEquals(f.getTelnetText(ABC_UNCOMPRESSED), recv.read());
    assertEquals(TelnetConnectionClosedByServer.getInstance(), recv.read());
  }

  @Test
  public void readEntireConversation() throws IOException {
    int totalRead = 0;
    try{
    BufferedInputStream inCheck = getBufferedInputStream(ENTIRE_CONVERSATION_UNCOMPRESSED_PATH);
    TelnetReader recv = getTelnetReceiverLoadedWithRealConversation();
    TelnetData data;
    
    while (totalRead <= 239952 &&(data = recv.read()) != TelnetConnectionClosedByServer.getInstance()) {
      if(data.sameBytes(SUBNEG_START_COMPRESSION)){
        recv.startCompression();
      }
      byte[] source = data.getBytes();
      byte[] check = new byte[source.length];
      int len=inCheck.read(check);
      if (len < check.length){
        throw new AssertionFailedError("Campione non compresso terminato prematuramente a"+totalRead +(len >0?len:0));
      }
      for (int i=0;i<source.length;i++){
        if(check[i]!=source[i]){
          System.out.println(data);
          System.out.println("\natteso");
          HexDump.dump(check, 0,System.out, 0);
          System.out.println("attuale");
          HexDump.dump(source, 0,System.out, 0);
          
          throw new AssertionError("Differenza a pos "+totalRead +"("+i+") atteso: "+String.format("0x%1$02X <%1$02d>", check[i] &0xFF)+" attuale: "+String.format("0x%1$02X <%1$02d>", source[i] &0xFF) );
        }
        
        totalRead++;
      }
      
    }
    }catch(Exception ex){
      log.info("read:"+totalRead);
      throw ex;
      
    }
  }

}
