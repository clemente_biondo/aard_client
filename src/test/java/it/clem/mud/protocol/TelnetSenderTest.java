package it.clem.mud.protocol;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.TestUtils;
import it.clem.mud.shared.streams.impl.TargetStreamImpl;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;
import it.clem.mud.shared.telnet.writer.impl.TelnetWriterImpl;
import it.clem.mud.util.LangUtils;
import static it.clem.mud.protocol.TelnetProtocolResources.*;

/**
 * 
 * Verifico che il sender serializzi correttamente il traffico in uscita.
 *
 */
public class TelnetSenderTest {
  @SuppressWarnings("unused")
  private final static Logger   log = LoggerFactory.getLogger(TelnetSenderTest.class);
  private ByteArrayOutputStream bout;
  private TelnetWriter          sender;
  private TelnetDataFactory     factory;

  @Before
  public void setUp() {
    bout = new ByteArrayOutputStream();
    sender = new TelnetWriterImpl(new TargetStreamImpl(bout));
    factory = new TelnetDataFactory();
  }

  @Test
  public void testCommands() throws IOException {
    sender.write(factory.getDo(CMP2));
    sender.write(factory.getDont(CMP1));
    sender.write(factory.getDo( AARD));
    sender.write(factory.getDont(ATCP));
    sender.write(factory.getDo( GMCP));
    sender.write(factory.getWill(AARD));
    sender.write(factory.getWill(TERMINAL_TYPE));
    sender.write(factory.getWont(NAWS));
    TestUtils.assertByteArrEquals(LangUtils.arrOfIntToArrOfByte(IAC, DO, CMP2, IAC, DONT, CMP1, IAC, DO, AARD, IAC, DONT, ATCP, IAC, DO, GMCP, IAC, WILL, AARD,
        IAC, WILL, TERMINAL_TYPE, IAC, WONT, NAWS), bout.toByteArray());
  }
}
