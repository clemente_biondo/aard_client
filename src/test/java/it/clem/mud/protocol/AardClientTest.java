package it.clem.mud.protocol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.AdditionalAnswers.answerVoid;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.mock.AardServerThreadMock;
import it.clem.mud.mock.AardTelnetContextMock;
import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.AbleToIgnoreTelnetData;
import it.clem.mud.protocol.data.TelnetDo;
import it.clem.mud.protocol.data.TelnetGMCPData;
import it.clem.mud.protocol.data.TelnetSubnegotiation;
import it.clem.mud.protocol.data.TelnetWill;
import it.clem.mud.protocol.data.handlers.TelnetDataHandlerBus;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.protocol.telnet.reader.impl.TelnetReaderImpl;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.telnet.writer.TelnetWriter;
import it.clem.mud.util.proxy_inspector.TelnetBufferPrinter;
import it.clem.mud.util.proxy_inspector.TelnetBufferPrinter.AnalysisResult;

import static it.clem.mud.TestUtils.*;
import static it.clem.mud.TestUtils.startAndWaitForCompletion;
import static it.clem.mud.protocol.TelnetProtocolResources.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AardClientTest {
  private final static Logger log = LoggerFactory.getLogger(AardClientTest.class);
  
  private TelnetDataHandlerBus client;

  private TelnetDataFactory factory;

  @After
  public void tearDown() {
    Thread.currentThread().setName("main");
  }

  @Before
  public void setUp() {
    Thread.currentThread().setName(AardServerThread.THREAD_NAME);
    client = TelnetDataHandlerBus.getAardClient();
    factory = new TelnetDataFactory();
  }

  @Test
  public void testClosed() throws IOException {
    MinimalClientTest.testClosedForClient(client);
  }

  @Test
  public void testText() throws IOException {
    MinimalClientTest.testTextForClient(client);
  }

  @Test
  public void testCommandsNotSupported() throws IOException {
    MinimalClientTest.testCommandsNotSupportedForClient(client, factory);
  }

  @Test
  public void testEcho() throws IOException {
    MinimalClientTest.testEchoForClient(client, factory);
  }

  @Test
  public void testDontWont() throws IOException {
    MinimalClientTest.testDontWontForClient(client, factory);  
  }
//  IAC WILL CMP2 -> IAC DO CMP2 
//  IAC WILL GMCP -> IAC DO GMCP 
//  IAC DO TERMINAL-TYPE -> IAC WILL TERMINAL-TYPE
//  IAC WILL ECHO -> IAC DO ECHO
  
  @Test
  public void testWillAndDo() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getWill(CMP2), mockTelnetContext);
    client.handle(factory.getWill(GMCP), mockTelnetContext);
    client.handle(factory.getWill(ECHO), mockTelnetContext);
    client.handle(factory.getDo(TERMINAL_TYPE), mockTelnetContext);
    verify(mockTelnetContext, times(1)).replyWithConfirmationOf(argThat(e ->{return  
        e.sameOf(factory.getWill(CMP2));
    }));
    InOrder ov= inOrder(mockTelnetContext);
    ov.verify(mockTelnetContext, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getWill(CMP2))));
    ov.verify(mockTelnetContext, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getWill(GMCP))));
    ov.verify(mockTelnetContext, times(1)).sendGMCPHandshake();
    ov.verify(mockTelnetContext, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getWill(ECHO))));
    ov.verify(mockTelnetContext, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getDo(TERMINAL_TYPE))));
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testOtherWill() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getWill(CMP1), mockTelnetContext);
    client.handle(factory.getWill(ATCP), mockTelnetContext);
    client.handle(factory.getWill(125), mockTelnetContext);
    client.handle(factory.getWill(AARD), mockTelnetContext);
    verify(mockTelnetContext, times(4)).replyWithRefusalOf(argThat(e -> e instanceof TelnetWill));
    verify(mockTelnetContext, times(1)).replyWithRefusalOf(argThat(e -> e instanceof TelnetWill && ((TelnetWill) e).getOption() == 125));
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testOtherDo() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getDo(AARD), mockTelnetContext);
    client.handle(factory.getDo(CMP2), mockTelnetContext);
    client.handle(factory.getDo(NAWS), mockTelnetContext);
    client.handle(factory.getDo(CMP1), mockTelnetContext);
    client.handle(factory.getDo(ATCP), mockTelnetContext);
    client.handle(factory.getDo(GMCP), mockTelnetContext);
    client.handle(factory.getDo(126), mockTelnetContext);
    verify(mockTelnetContext, times(7)).replyWithRefusalOf(argThat(e -> e instanceof TelnetDo));
    verify(mockTelnetContext, times(1)).replyWithRefusalOf(argThat(e -> {
      System.out.println(e);
      return e instanceof TelnetDo && ((TelnetDo) e).getOption() == 126;
    }));
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testOtherSubneg() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    //client.handle(factory.getStartCompressionSubneg(), mockTelnetContext);
    //client.handle(factory.getSendTerminal(), mockTelnetContext);
    client.handle(factory.getSubnegotionation(127, new byte[] { 1, 2, 3 }), mockTelnetContext);
    verify(mockTelnetContext, times(1)).ignore(any(TelnetSubnegotiation.class));
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testCompression() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getStartCompressionSubneg(), mockTelnetContext);
    verify(mockTelnetContext, times(1)).startCompression();
    verifyNoMoreInteractions(mockTelnetContext);
  }

  @Test
  public void testTerminalType() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getSubnegotionation(TERMINAL_TYPE, TERMINAL_TYPE_SEND), mockTelnetContext);
    verify(mockTelnetContext, times(1)).sendTerminalType();
    verifyNoMoreInteractions(mockTelnetContext);
  }
  
  @Test(timeout = 10000)
  public void testLongConversation() throws Exception {
    AardServerThread server = null;
    try {
      log.debug("inizio testAArdHandshake");
      TelnetBufferPrinter printer= new TelnetBufferPrinter();
      AnalysisResult ar= doWithInputStreamLoadedWithRealConversationUncompressed(printer::analyzeAard);
      GameEventQueue queue = mock(GameEventQueue.class);
      TelnetWriter sender = mock(TelnetWriter.class);

      AardTelnetContext tc = mock(AardTelnetContext.class);
      
      final AtomicInteger textBytesCount=new AtomicInteger(0);       
      final AtomicInteger zeroLenLines=new AtomicInteger(0);       
      final AtomicInteger notLFCRLines=new AtomicInteger(0);
      doAnswer(answerVoid((String text)->{
        textBytesCount.addAndGet(text.length());
        if(text.length()== 0){
          zeroLenLines.incrementAndGet();
        } else if(text.lastIndexOf('\r')!=text.length()-1&&text.lastIndexOf('\n')!=text.length()-1){
          notLFCRLines.incrementAndGet();
        }
      })).when(tc).fireTextSentFromServerEvent(any());//Sintassi per void
      SourceStream sourceStream = getSourceStreamLoadedWithRealConversation();
      //when(tc)..startCompression();
      doAnswer((InvocationOnMock i)->{sourceStream.startCompression();return null;}).when(tc).startCompression();

      server = AardServerThreadMock.builder()
          .setServerEventProducer(queue)
          .setTelnetDataHandlerBus(client)
          .setTelnetSender(sender)
          .setTelnetReceiver(new TelnetReaderImpl(sourceStream,new TelnetDataFactory()))
          .setAardTelnetContext(tc).build();
      
      assertFalse("Il game loop non e' terminato", startAndWaitForCompletion(server, 5000));
      
      assertEquals(0, zeroLenLines.get());
      assertEquals(2, notLFCRLines.get());
      assertEquals(ar.getTextBytesRead(), textBytesCount.get());
      verify(tc, times(1)).replyWithRefusalOf(argThat(e -> e.sameOf(factory.getWill(CMP1))));
      verify(tc, times(1)).replyWithRefusalOf(argThat(e -> e.sameOf(factory.getWill(AARD))));
      verify(tc, times(1)).replyWithRefusalOf(argThat(e -> e.sameOf(factory.getWill(ATCP))));
      verify(tc, times(1)).replyWithRefusalOf(argThat(e -> e.sameOf(factory.getDo(AARD))));
      verify(tc, times(1)).replyWithRefusalOf(argThat(e -> e.sameOf(factory.getDo(NAWS))));
      verify(tc, times(5)).replyWithRefusalOf(any());
      
      verify(tc, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getWill(ECHO))));
      verify(tc, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getWill(CMP2))));
      verify(tc, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getWill(GMCP))));
      verify(tc, times(1)).sendGMCPHandshake();
      verify(tc, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getDo(TERMINAL_TYPE))));
      verify(tc, times(1)).replyWithConfirmationOf(argThat(e -> e.sameOf(factory.getWont(ECHO))));
      verify(tc, times(5)).replyWithConfirmationOf(any());
      //non vengono ignorate:
      //1 START_COMPRESSION
      //1 TERMINAL_TYPE_SEND
      //8 tra will e do iniziali
      //1 will echo
      //1 wont echo
      verify(tc, times(ar.count()-8-2-2)).ignore(any());
      verify(tc, times(1)).fireConnectionClosedByServerEvent();
      verify(tc, times(ar.getTextlines())).fireTextSentFromServerEvent(anyString());
      verifyNoMoreInteractions(queue);
    } catch (Exception ex) {
      log.debug(ex.getMessage(), ex);
      throw ex;
    } finally {
      log.debug(
          server == null ? "aard server ancora non creato!" : (server.getName() + "state:" + server.getState().name()));
      log.debug("this :" + Thread.currentThread().getState().name());
      log.debug("fine testAArdHandshake");
    }
  }

  @Test
  public void testOtherDontRepeatYourself1() throws IOException {
    GameEventQueue queue = mock(GameEventQueue.class);
    TelnetWriter sender = mock(TelnetWriter.class);
    TelnetReader receiver= mock(TelnetReader.class);
    AbleToIgnoreTelnetData ignore= mock(AbleToIgnoreTelnetData.class);
    AardTelnetContext telnetContext=new AardTelnetContextMock(queue, receiver, sender, factory,ignore);  
    
    client.handle(factory.getWill(GMCP), telnetContext);
    client.handle(factory.getWill(GMCP), telnetContext);

    InOrder ov = inOrder(queue,sender,receiver,ignore);
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(sender, times(10)).write(argThat(e-> e instanceof TelnetGMCPData));
    ov.verify(ignore, times(1)).ignore(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    verifyNoMoreInteractions(queue,sender,receiver,ignore);
  }

  @Test
  public void testOtherDontRepeatYourself2() throws IOException {
    GameEventQueue queue = mock(GameEventQueue.class);
    TelnetWriter sender = mock(TelnetWriter.class);
    TelnetReader receiver= mock(TelnetReader.class);
    AbleToIgnoreTelnetData ignore= mock(AbleToIgnoreTelnetData.class);
    AardTelnetContext telnetContext=new AardTelnetContextMock(queue, receiver, sender, factory,ignore);  

    client.handle(factory.getWill(GMCP), telnetContext);
    client.handle(factory.getWont(GMCP), telnetContext);
    client.handle(factory.getWill(GMCP), telnetContext);

    InOrder ov = inOrder(queue,sender,receiver,ignore);
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(sender, times(10)).write(argThat(e-> e instanceof TelnetGMCPData));
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDont(GMCP))));
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(sender, times(10)).write(argThat(e-> e instanceof TelnetGMCPData));
    verifyNoMoreInteractions(queue,sender,receiver,ignore);
  }
  
  @Test
  public void testOtherDontRepeatYourself3() throws IOException {
    GameEventQueue queue = mock(GameEventQueue.class);
    TelnetWriter sender = mock(TelnetWriter.class);
    TelnetReader receiver= mock(TelnetReader.class);
    AbleToIgnoreTelnetData ignore= mock(AbleToIgnoreTelnetData.class);
    AardTelnetContext telnetContext=new AardTelnetContextMock(queue, receiver, sender, factory,ignore);  

    client.handle(factory.getWill(GMCP), telnetContext);
    client.handle(factory.getWill(GMCP), telnetContext);
    client.handle(factory.getWill(CMP2), telnetContext);
    InOrder ov = inOrder(queue,sender,receiver,ignore);

    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(sender, times(10)).write(argThat(e-> e instanceof TelnetGMCPData));
    ov.verify(ignore, times(1)).ignore(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDo(CMP2))));
    verifyNoMoreInteractions(queue,sender,receiver,ignore);
  }

  @Test
  public void testOtherDontRepeatYourself4() throws IOException {
    GameEventQueue queue = mock(GameEventQueue.class);
    TelnetWriter sender = mock(TelnetWriter.class);
    TelnetReader receiver= mock(TelnetReader.class);
    AbleToIgnoreTelnetData ignore= mock(AbleToIgnoreTelnetData.class);
    AardTelnetContext telnetContext=new AardTelnetContextMock(queue, receiver, sender, factory,ignore);  

    client.handle(factory.getWill(GMCP), telnetContext);//do 
    client.handle(factory.getDo(GMCP), telnetContext);//wont
    client.handle(factory.getWill(GMCP), telnetContext);//ignora do
    client.handle(factory.getDo(GMCP), telnetContext);//ignora wont
    client.handle(factory.getDont(GMCP), telnetContext);//ignora wont
    client.handle(factory.getDont(GMCP), telnetContext);//ignora wont
    client.handle(factory.getWill(GMCP), telnetContext);//ignora do
    client.handle(factory.getWont(GMCP), telnetContext);//dont
    client.handle(factory.getWill(GMCP), telnetContext);//do
    client.handle(factory.getWill(GMCP), telnetContext);//ignora do
    client.handle(factory.getDo(GMCP), telnetContext);//wont, era gia' in dont. questa cosa è discutibile, ma nel mio scenario non rilevante.

    InOrder ov = inOrder(queue,sender,receiver,ignore);
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(sender, times(10)).write(argThat(e-> e instanceof TelnetGMCPData));
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getWont(GMCP))));
    ov.verify(ignore, times(1)).ignore(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(ignore, times(3)).ignore(argThat(e -> e.sameOf(factory.getWont(GMCP))));
    ov.verify(ignore, times(1)).ignore(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDont(GMCP))));
    ov.verify(sender, times(1)).write(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(sender, times(10)).write(argThat(e-> e instanceof TelnetGMCPData));
    ov.verify(ignore, times(1)).ignore(argThat(e -> e.sameOf(factory.getDo(GMCP))));
    ov.verify(ignore, times(1)).ignore(argThat(e -> e.sameOf(factory.getWont(GMCP))));
    verifyNoMoreInteractions(queue,sender,receiver,ignore);
  }
  
  @Test
  public void testOptionActive() throws IOException {
    GameEventQueue queue = mock(GameEventQueue.class);
    TelnetWriter sender = mock(TelnetWriter.class);
    TelnetReader receiver= mock(TelnetReader.class);
    AardTelnetContext telnetContext=new AardTelnetContextMock(queue, receiver, sender, factory);  

    assertFalse("GMCP per il client doveva essere disabilitato",telnetContext.isClientOptionActive(GMCP)); 
    assertFalse("GMCP per il server doveva essere disabilitato",telnetContext.isServerOptionActive(GMCP)); 
    assertFalse("CMP2 per il client doveva essere disabilitato",telnetContext.isClientOptionActive(CMP2)); 
    assertFalse("CMP2 per il server doveva essere disabilitato",telnetContext.isServerOptionActive(CMP2)); 
    client.handle(factory.getWill(GMCP), telnetContext);//ok 
    assertFalse("GMCP per il client doveva essere disabilitato",telnetContext.isClientOptionActive(GMCP)); 
    assertTrue("GMCP per il server doveva essere attivo",telnetContext.isServerOptionActive(GMCP)); 
    assertFalse("CMP2 per il client doveva essere disabilitato",telnetContext.isClientOptionActive(CMP2)); 
    assertFalse("CMP2 per il server doveva essere disabilitato",telnetContext.isServerOptionActive(CMP2)); 
    client.handle(factory.getWill(GMCP), telnetContext);//ok 
    assertFalse("GMCP per il client doveva essere disabilitato",telnetContext.isClientOptionActive(GMCP)); 
    assertTrue("GMCP per il server doveva essere attivo",telnetContext.isServerOptionActive(GMCP)); 
    assertFalse("CMP2 per il client doveva essere disabilitato",telnetContext.isClientOptionActive(CMP2)); 
    assertFalse("CMP2 per il server doveva essere disabilitato",telnetContext.isServerOptionActive(CMP2)); 
    client.handle(factory.getWont(GMCP), telnetContext);//ok 
    assertFalse("GMCP per il client doveva essere disabilitato",telnetContext.isClientOptionActive(GMCP)); 
    assertFalse("GMCP per il server doveva essere disabilitato",telnetContext.isServerOptionActive(GMCP)); 
    assertFalse("CMP2 per il client doveva essere disabilitato",telnetContext.isClientOptionActive(CMP2)); 
    assertFalse("CMP2 per il server doveva essere disabilitato",telnetContext.isServerOptionActive(CMP2)); 

  }
  
  @Test
  public void testGA() throws IOException {
    AardTelnetContext mockTelnetContext=mock(AardTelnetContext.class);
    client.handle(factory.getGA(), mockTelnetContext);
    verify(mockTelnetContext, times(1)).ignore(any());
    verifyNoMoreInteractions(mockTelnetContext);
  }
  
  
}
//rinominare sender in writer etc