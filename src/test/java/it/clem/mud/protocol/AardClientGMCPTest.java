package it.clem.mud.protocol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.protocol.data.AardTelnetContext;
import it.clem.mud.protocol.data.TelnetGMCPData;
import it.clem.mud.protocol.data.handlers.TelnetDataHandlerBus;
import it.clem.mud.shared.telnet.TelnetDataFactory;

import static it.clem.mud.TestUtils.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static it.clem.mud.protocol.TelnetProtocolResources.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AardClientGMCPTest {
  private final static Logger log = LoggerFactory.getLogger(AardClientGMCPTest.class);
  private TelnetDataHandlerBus client;
  private TelnetDataFactory factory;
  @Mock private AardTelnetContext mockTelnetContext;
  @After
  public void tearDown() {
    Thread.currentThread().setName("main");
  }

  @Before
  public void setUp() {
    Thread.currentThread().setName(AardServerThread.THREAD_NAME);
    client = TelnetDataHandlerBus.getAardClient();
    factory = new TelnetDataFactory();
  }
  
  @Test
  public void testIgnora() {
    client.handle(factory.getTelnetGMCP("123"),mockTelnetContext);
    verify(mockTelnetContext,times(1)).ignore(any());
    verifyNoMoreInteractions(mockTelnetContext);
  }
  
  @Test
  public void testTick() {
    client.handle(factory.getTelnetGMCP(TelnetGMCPData.SERVER_TICK_MESSAGE),mockTelnetContext);
    verify(mockTelnetContext,times(1)).fireGMCPTick();
    verifyNoMoreInteractions(mockTelnetContext);
  }
  
}
