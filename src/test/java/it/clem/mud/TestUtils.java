package it.clem.mud;

import static it.clem.mud.protocol.TelnetProtocolResources.AARD;
import static it.clem.mud.protocol.TelnetProtocolResources.ATCP;
import static it.clem.mud.protocol.TelnetProtocolResources.CMP1;
import static it.clem.mud.protocol.TelnetProtocolResources.CMP2;
import static it.clem.mud.protocol.TelnetProtocolResources.DO;
import static it.clem.mud.protocol.TelnetProtocolResources.GMCP;
import static it.clem.mud.protocol.TelnetProtocolResources.IAC;
import static it.clem.mud.protocol.TelnetProtocolResources.NAWS;
import static it.clem.mud.protocol.TelnetProtocolResources.TERMINAL_TYPE;
import static it.clem.mud.protocol.TelnetProtocolResources.WILL;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Function;

import org.apache.commons.io.HexDump;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.clem.mud.client.swing.MainWindow;
import it.clem.mud.client.swing.OutputPanel;
import it.clem.mud.client.swing.util.ConnectionStatusController;
import it.clem.mud.client.swing.workers.GameEventLoopWorker;
import it.clem.mud.client.swing.workers.messages.WorkerMessageFactoryImpl;
import it.clem.mud.gedt.RunnableGameLoop;
import it.clem.mud.gedt.socket.AbstractSocket;
import it.clem.mud.protocol.data.handlers.NVTSupport;
import it.clem.mud.protocol.telnet.reader.TelnetReader;
import it.clem.mud.protocol.telnet.reader.impl.TelnetReaderImpl;
import it.clem.mud.shared.game.GameStrategy;
import it.clem.mud.shared.game.event.GameEventFactoryImpl;
import it.clem.mud.shared.game.event.queue.GameEventQueue;
import it.clem.mud.shared.game.event.queue.GameEventQueueImpl;
import it.clem.mud.shared.recording.RecordQueue;
import it.clem.mud.shared.streams.SourceStream;
import it.clem.mud.shared.streams.impl.SourceStreamImpl;
import it.clem.mud.shared.telnet.TelnetDataFactory;
import it.clem.mud.shared.view.ClientCommunicationStrategy;
import it.clem.mud.util.HostWithPort;
import it.clem.mud.util.LangUtils;
//regexp gmcp ^([\s\S](?!GMCP|<<<|>>>)[\s\S])*$
public class TestUtils {
  private final static Logger log = LoggerFactory.getLogger(TestUtils.class);

  public final static String INITIAL_DATA_PATH = "data/initialData.bin";
  public final static String ENTIRE_CONVERSATION_PATH = "sess1/in.bin";
  public final static String ENTIRE_CONVERSATION_UNCOMPRESSED_PATH = "sess1/in_uncompressed.bin";
  public final static String ENTIRE_CONVERSATION_XTERM_DUE = "aard_xterm_due/in.bin";

  public final static int[] AARD_HANDSHAKE = new int[] { IAC, WILL, CMP2, IAC, WILL, CMP1, IAC, WILL, AARD, IAC, WILL,
      ATCP, IAC, WILL, GMCP, IAC, DO, AARD, IAC, DO, TERMINAL_TYPE, IAC, DO, NAWS };
  public final static int[] CHAR_255 = new int[] { IAC, IAC };

  public final static String ABC_UNCOMPRESSED = "abcdefgabcdefgabcdefg";
  public final static byte[] ABC_COMPRESSED = LangUtils.arrOfIntToArrOfByte(0x78, 0x9c, 0x4b, 0x4c, 0x4a, 0x4e, 0x49,
      0x4d, 0x4b, 0x4f, 0x44, 0xa1, 0x00, 0x59, 0xfd, 0x08, 0x35);

  public final static byte[] CARATTERI_SPECIALI = LangUtils.arrOfIntToArrOfByte(0x20, 0xe2, 0x94);

  public static TelnetReader getTelnetReceiver(int... is) throws IOException {
    return new TelnetReaderImpl(getSourceStream(is),new TelnetDataFactory());
  }

  public static TelnetReader getTelnetReceiverLoadedWithRealConversationUncompressed() throws IOException {
    return new TelnetReaderImpl(getSourceStreamLoadedWithRealConversationUncompressed(),new TelnetDataFactory());
  }

  public static TelnetReader getTelnetReceiverLoadedWithRealConversation() throws IOException {
    return new TelnetReaderImpl(getSourceStreamLoadedWithRealConversation(),new TelnetDataFactory());
  }

  public static TelnetReader getTelnetReceiverLoadedWithFirstScreen() throws IOException {
    return new TelnetReaderImpl(getSourceStreamLoadedWithFirstScreen(),new TelnetDataFactory());
  }

  public static TelnetReader getTelnetReceiver(String path) throws IOException {
    return new TelnetReaderImpl(getSourceStream(path),new TelnetDataFactory());
  }

  public static BufferedInputStream getBufferedInputStream(String path) throws IOException {
    return new BufferedInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream(path));
  }

  public static TelnetReader getTelnetReceiver(byte[] first, byte[]... others) throws IOException {
    return new TelnetReaderImpl(getSourceStream(first, others),new TelnetDataFactory());
  }

  public static TelnetReader getTelnetReceiverLoadedWithAArdHandshake() throws IOException {
    return new TelnetReaderImpl(getSourceStreamLoadedWithAArdHandshake(),new TelnetDataFactory());
  }

  public static TelnetReader getTelnetReceiverLoadedWithSpecialChars() throws IOException {
    return new TelnetReaderImpl(getSourceStreamLoadedWithSpecialChars(),new TelnetDataFactory());
  }

  public static SourceStream getSourceStreamLoadedWithRealConversation() throws IOException {
    return getSourceStream(ENTIRE_CONVERSATION_PATH);
  }

  public static SourceStream getSourceStreamLoadedWithRealConversationUncompressed() throws IOException {
    return getSourceStream(ENTIRE_CONVERSATION_UNCOMPRESSED_PATH);
  }

  public static InputStream getInputStreamLoadedWithRealConversationUncompressed() throws IOException {
    return getInputStream(ENTIRE_CONVERSATION_UNCOMPRESSED_PATH);
  }

  public static <T> T doWithInputStreamLoadedWithRealConversationUncompressed(Function<InputStream, T> func)
      throws IOException {
    try (InputStream is = getInputStream(ENTIRE_CONVERSATION_UNCOMPRESSED_PATH)) {
      return func.apply(is);
    }
  }

  public static <T> T doWithInputStreamLoadedWithRealConversation(Function<InputStream, T> func) throws IOException {
    try (InputStream is = getInputStream(ENTIRE_CONVERSATION_PATH)) {
      return func.apply(is);
    }
  }

  public static SourceStream getSourceStreamLoadedWithFirstScreen() throws IOException {
    return getSourceStream(INITIAL_DATA_PATH);
  }

  public static SourceStream getSourceStream(String path) throws IOException {
    return new SourceStreamImpl(Thread.currentThread().getContextClassLoader().getResourceAsStream(path));
  }

  public static InputStream getInputStream(String path) throws IOException {
    return Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
  }

  public static SourceStream getSourceStream(byte[] first, byte[]... others) throws IOException {
    return new SourceStreamImpl(new ByteArrayInputStream(LangUtils.concat(first, others)));
  }

  public static SourceStream getSourceStreamLoadedWithAArdHandshake() throws IOException {
    return getSourceStream(AARD_HANDSHAKE);
  }

  public static SourceStream getSourceStreamLoadedWithSpecialChars() throws IOException {
    return getSourceStream(CARATTERI_SPECIALI);
  }

  public static SourceStream getSourceStream(int... is) throws IOException {
    return new SourceStreamImpl(new ByteArrayInputStream(LangUtils.arrOfIntToArrOfByte(is)));
  }

  public static void assertByteArrEquals(byte[] expected, byte[] actual, int offset)
      throws ArrayIndexOutOfBoundsException, IllegalArgumentException, IOException {
    for (int i = 0; i < actual.length; i++) {
      if (expected[i] != actual[i]) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        HexDump.dump(expected, 0, out, 0);
        String msg = "\natteso\n" + out;
        out = new ByteArrayOutputStream();
        HexDump.dump(actual, 0, out, 0);
        System.out.println();
        log.error(msg + "attuale\n" + out);
        throw new AssertionError(String.format(
            "Differenza a offset %1d (0x%1$06X), pos  %2d (0x%2$06X) atteso: 0x%3$02X <%3$02d> attuale: 0x%4$02X <%4$02d>",
            offset, i, expected[i] & 0xFF, actual[i] & 0xFF));
      }
    }
  }

  public static void assertByteArrEquals(byte[] expected, byte[] actual)
      throws ArrayIndexOutOfBoundsException, IllegalArgumentException, IOException {
    assertByteArrEquals(expected, actual, 0);
  }

  public static boolean startAndWaitForCompletion(Thread t) throws InterruptedException {
    return startAndWaitForCompletion(t, 500);
  }

  public static boolean startAndWaitForCompletion(Thread t, long millis) throws InterruptedException {
    t.start();
    t.join(millis);
    return t.isAlive();
  }

  public static MainWindow getMainWindow(final AbstractSocket socket,String title) {
    return new MainWindow(new GameEventQueueImpl(new LinkedBlockingQueue<>(), new GameEventFactoryImpl()),title) {
      
      /**
       * 
       */
      private static final long serialVersionUID = 3263988223305934479L;

      @Override
      protected ConnectionStatusController getConnectionStatusController(GameEventQueue eventQueue,
          OutputPanel outputPane, OutputPanel mapPanel) {
        return new ConnectionStatusController(eventQueue, this, outputPane, mapPanel) {

          @Override
          protected GameEventLoopWorker getGameEventLoopWorker(GameEventQueue eventQueue,
              WorkerMessageFactoryImpl factory) {

            return new GameEventLoopWorker(eventQueue, factory) {

              @Override
              protected RunnableGameLoop geRunnableGameLoop(HostWithPort server, GameEventQueue eventBus,
                  NVTSupport nvtSupport, GameStrategy gameStrategy, ClientCommunicationStrategy clientStrategy,
                  RecordQueue recordQueue) {
                return new RunnableGameLoop(server, eventBus, nvtSupport, gameStrategy, clientStrategy, recordQueue) {

                  @Override
                  protected AbstractSocket getSocket(HostWithPort server, File recordingDirectory) throws IOException {
                    return socket;
                  }

                };
              }

            };
          }

        };
      }

    };
  }
}
