package it.clem.mud.client;

import it.clem.mud.client.swing.util.CommandHistory;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CommandHistoryTest {
  private CommandHistory history;
  
  @Before
  public void setUp() {
    history = new CommandHistory();
  }

  @Test
  public void empty() {
   assertFalse(history.getNext().isPresent());
   assertFalse(history.getPrev().isPresent());
   
   assertFalse(history.getPrev().isPresent());
   assertFalse(history.getPrev().isPresent());
   assertFalse(history.getNext().isPresent());
   assertFalse(history.getNext().isPresent());
   
   history.appendCommand("");
   assertFalse(history.getPrev().isPresent());
   assertFalse(history.getNext().isPresent());
  
   history.appendCommand("a");
   history.appendCommand("b");
   history.appendCommand("c");
   assertFalse(history.getNext().isPresent());
   assertFalse(history.getPrev().isPresent());
  }
  
  @Test
  public void one() {
   
   //inserisco un comando e resetto la pos nella history al fondo
   history.appendCommand("abcd");
   //sono al fondo quindi non trovo niente
   assertFalse(history.getNext().isPresent());
   //lo recupero dalla history e mi sposto sul dato
   assertEquals("abcd",history.getPrev().get());
   //non c'e' più nulla sopra e sotto
   assertFalse(history.getNext().isPresent());
   assertFalse(history.getPrev().isPresent());
   
   //non accetto il comando in quanto non presente e resetto la pos nella history al fondo
   //siccome è l'unico della lista non succede altro
   history.appendCommand("abcd");
   //si ripresenta lo scenario predecente
   assertFalse(history.getNext().isPresent());
   assertEquals("abcd",history.getPrev().get());
   assertFalse(history.getNext().isPresent());
   assertFalse(history.getPrev().isPresent());
   
   //non accetto il comando in quanto troppo corto. Si ripresenta lo scenario predecente
   history.appendCommand("");
   assertEquals("abcd",history.getPrev().get());
   assertFalse(history.getNext().isPresent());
   assertFalse(history.getPrev().isPresent());
   
   //insomma con un solo comando in canna getNext  non torna mai nulla
   
   
  }
  
  @Test
  public void twoSimple() {
    history.appendCommand("abcd");
    history.appendCommand("defg");
    assertFalse(history.getNext().isPresent());
    assertFalse(history.getNext().isPresent());
    assertEquals("defg",history.getPrev().get());
    assertEquals("abcd",history.getPrev().get());
    assertFalse(history.getPrev().isPresent());
    assertFalse(history.getPrev().isPresent());
    assertEquals("defg",history.getNext().get());
    assertEquals("abcd",history.getPrev().get());
    assertEquals("defg",history.getNext().get());
    assertFalse(history.getNext().isPresent());
    assertEquals("abcd",history.getPrev().get());
    assertFalse(history.getPrev().isPresent());
    assertEquals("defg",history.getNext().get());//sono piazzato su defg

    history.appendCommand("");//reset
    assertEquals("defg",history.getPrev().get());
    assertEquals("abcd",history.getPrev().get());//sono piazzato su abcd
    history.appendCommand("");//reset
    assertFalse(history.getNext().isPresent());
    assertEquals("defg",history.getPrev().get());
  }
   
  @Test
  public void threeSimple() {
    history.appendCommand("abcd");
    history.appendCommand("defg");
    history.appendCommand("iii");
    //salgo
    assertEquals("defg",history.getPrev().get());
    history.appendCommand("iiii");
    
    assertEquals("iiii",history.getPrev().get());
    assertEquals("defg",history.getPrev().get());
    assertEquals("abcd",history.getPrev().get());
    assertFalse(history.getPrev().isPresent());
    //scendo
    assertEquals("defg",history.getNext().get());
    assertEquals("iiii",history.getNext().get());
    assertFalse(history.getNext().isPresent());
    //salgo
    assertEquals("defg",history.getPrev().get());
    assertEquals("abcd",history.getPrev().get());
    assertFalse(history.getPrev().isPresent());//sono in cima ma non è importante, pa pos verra' resettata
    history.appendCommand("defg"); //ora l'ordine è  abcd iii defg
    //salgo
    assertEquals("defg",history.getPrev().get());
    assertEquals("iiii",history.getPrev().get());
    assertEquals("abcd",history.getPrev().get());
    assertFalse(history.getPrev().isPresent());
    //scendo
    assertEquals("iiii",history.getNext().get());
    assertEquals("defg",history.getNext().get());
    assertFalse(history.getNext().isPresent());
    //salgo
    assertEquals("iiii",history.getPrev().get());
    assertEquals("abcd",history.getPrev().get());
    assertFalse(history.getPrev().isPresent());//sono in cima
    
  }
}
