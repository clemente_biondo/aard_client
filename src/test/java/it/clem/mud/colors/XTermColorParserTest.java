package it.clem.mud.colors;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.clem.mud.util.colors.ansi.AnsiColor;
import it.clem.mud.util.colors.xterm.XTermColorParser;
import it.clem.mud.util.colors.xterm.XtermColoredText;

public class XTermColorParserTest {
  private XTermColorParser colorParser;

  @Before
  public void setUp() {
    colorParser = new XTermColorParser();
  }

  @Test
  public void empty() {
    assertEquals(0, colorParser.parseText("").size());
    assertEquals(0, colorParser.parseText(null).size());
  }


  @Test
  public void noColor() {
    List<XtermColoredText> res = colorParser.parseText("abc");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("abc",7,-1), res.get(0));
  }

  @Test
  public void whiteColor() {
    List<XtermColoredText> res = colorParser
        .parseText("Prompt set to \033[0;37m[%h/%Hhp %m/%Mmn %v/%Vmv %qqt %Xtnl] >%c\033[0;37m");
    assertEquals(2, res.size());
    assertEquals(XtermColoredText.valueOf("Prompt set to "), res.get(0));
    assertEquals(XtermColoredText.valueOf("[%h/%Hhp %m/%Mmn %v/%Vmv %qqt %Xtnl] >%c"), res.get(1));
  }

  @Test
  public void multiColor() {
    List<XtermColoredText> res = colorParser.parseText(
        "   @b / @B \033[0;34mblue\033[0;37m / \033[1;34mbblue\033[0;37m           @c / @C \033[0;36mcyan\033[0;37m / \033[1;36mbcyan\033[0;37m");
    assertEquals(8, res.size());
    assertEquals(XtermColoredText.valueOf("   @b / @B "), res.get(0));
    assertEquals(XtermColoredText.valueOf("blue", AnsiColor.Blue.ordinal()), res.get(1));
    assertEquals(XtermColoredText.valueOf(" / "), res.get(2));
    assertEquals(XtermColoredText.valueOf("bblue", AnsiColor.BrightBlue.ordinal()), res.get(3));
    assertEquals(XtermColoredText.valueOf("           @c / @C "), res.get(4));
    assertEquals(XtermColoredText.valueOf("cyan", AnsiColor.Cyan.ordinal()), res.get(5));
    assertEquals(XtermColoredText.valueOf(" / "), res.get(6));
    assertEquals(XtermColoredText.valueOf("bcyan", AnsiColor.BrightCyan.ordinal()), res.get(7));
  }

  @Test
  public void reset() {
    List<XtermColoredText> res = colorParser.parseText(
        "\033[0m[AFK] \033[1;33mGlaer \033[1;32mGlorious \033[1;37mAvatar\033[0;37m of \033[1;33mBenevolent\033[0;37m Humility\033[1;33m is here.\033[0;37m");
    assertEquals(8, res.size());
    assertEquals(XtermColoredText.valueOf("[AFK] "), res.get(0));
    assertEquals(XtermColoredText.valueOf("Glaer ",AnsiColor.BrightYellow.ordinal()), res.get(1));
    assertEquals(XtermColoredText.valueOf("Glorious ",AnsiColor.BrightGreen.ordinal()), res.get(2));
    assertEquals(XtermColoredText.valueOf("Avatar",AnsiColor.BrightWhite.ordinal()), res.get(3));
    assertEquals(XtermColoredText.valueOf(" of "), res.get(4));
    assertEquals(XtermColoredText.valueOf("Benevolent",AnsiColor.BrightYellow.ordinal()), res.get(5));
    assertEquals(XtermColoredText.valueOf(" Humility"), res.get(6));
    assertEquals(XtermColoredText.valueOf(" is here.",AnsiColor.BrightYellow.ordinal()), res.get(7));
  }
  
  @Test
  public void multiplesimple1() {
    List<XtermColoredText> res1 = colorParser.parseText("abc\033[0mcde");
    List<XtermColoredText> res2 = colorParser.parseText("ghi");
    assertEquals(2, res1.size());
    assertEquals(XtermColoredText.valueOf("abc"), res1.get(0));
    assertEquals(XtermColoredText.valueOf("cde"), res1.get(1));
    assertEquals(1, res2.size());
    assertEquals(XtermColoredText.valueOf("ghi"), res2.get(0));
  }
  
  @Test
  public void multiplesimple2() {
    List<XtermColoredText> res1 = colorParser.parseText("abc\033[1mcde");
    List<XtermColoredText> res2 = colorParser.parseText("ghi");
    assertEquals(2, res1.size());
    assertEquals(XtermColoredText.valueOf("abc"), res1.get(0));
    assertEquals(XtermColoredText.valueOf("cde",15,-1), res1.get(1));
    assertEquals(1, res2.size());
    assertEquals(XtermColoredText.valueOf("ghi",15,-1), res2.get(0));
  }
  
  @Test
  public void simplest() {
    List<XtermColoredText> res = colorParser.parseText("a");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("a"), res.get(0));
  }

  @Test
  public void simple1() {
    List<XtermColoredText> res = colorParser.parseText("abc\033[0mcde");
    assertEquals(2, res.size());
    assertEquals(XtermColoredText.valueOf("abc"), res.get(0));
    assertEquals(XtermColoredText.valueOf("cde"), res.get(1));
  }

  @Test
  public void simple2() {
    List<XtermColoredText> res = colorParser.parseText("\033[0mcde");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("cde"), res.get(0));
  }

  @Test
  public void simple3() {
    List<XtermColoredText> res = colorParser.parseText("abc\033[0m");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void simple4() {
    List<XtermColoredText> res = colorParser.parseText("\033[0m\033[0m");
    assertEquals(0, res.size());
  }

  @Test
  public void simple5() {
    List<XtermColoredText> res = colorParser.parseText("\033[0mabc\033[0m\033[0m");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void simple6() {
    List<XtermColoredText> res = colorParser.parseText("abc\033[0m");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void simple7() {
    List<XtermColoredText> res = colorParser.parseText("\033[0mabc");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void simple8() {
    List<XtermColoredText> res = colorParser.parseText("abc\033[0mcde\033[0m");
    assertEquals(2, res.size());
    assertEquals(XtermColoredText.valueOf("abc"), res.get(0));
    assertEquals(XtermColoredText.valueOf("cde"), res.get(1));
  }

  @Test
  public void simple9() {
    List<XtermColoredText> res = colorParser.parseText("\033[0mabc\033[0mcde");
    assertEquals(2, res.size());
    assertEquals(XtermColoredText.valueOf("abc"), res.get(0));
    assertEquals(XtermColoredText.valueOf("cde"), res.get(1));
  }
  
  @Test
  public void test256A() {
    List<XtermColoredText> res = colorParser.parseText("\033[38;5;231mColor 231 Test   \033[1;37mSame color in ANSI\033[0;37m");
    assertEquals(2, res.size());
    assertEquals(XtermColoredText.valueOf("Color 231 Test   ",231,-1), res.get(0));
    assertEquals(XtermColoredText.valueOf("Same color in ANSI",15,-1), res.get(1));
  }
  
  @Test
  public void test256B() {
    List<XtermColoredText> res = colorParser.parseText("\033[0;m");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("\033[0;m"), res.get(0));
    res = colorParser.parseText("\033[0m");
    assertEquals(0, res.size());
  }
  
  @Test
  public void test256C() {
    List<XtermColoredText> res = colorParser.parseText("\033[0mabc");
    assertEquals(1, res.size());
    assertEquals(XtermColoredText.valueOf("abc"), res.get(0));
  }
  
  @Test
  public void test256D() {
    List<XtermColoredText> res = colorParser.parseText("\033[1;37m\033[44m \033[33m            Showing : Alphabetical Command List               \033[0;37m");
    assertEquals(2, res.size());
    assertEquals(XtermColoredText.valueOf(" ",15,12), res.get(0));
    assertEquals(XtermColoredText.valueOf("            Showing : Alphabetical Command List               ",11,12), res.get(1));
  }
  
  @Test
  public void test256E() {
    List<XtermColoredText> res = colorParser.parseText("\033[1;32m Account         \033[1;36m Accept          \033[1;32m Activate        \033[1;36m Affected        \033[0;37m");
    assertEquals(4, res.size());
    assertEquals(XtermColoredText.valueOf(" Account         ",10,-1), res.get(0));
    assertEquals(XtermColoredText.valueOf(" Accept          ",14,-1), res.get(1));
    assertEquals(XtermColoredText.valueOf(" Activate        ",10,-1), res.get(2));
    assertEquals(XtermColoredText.valueOf(" Affected        ",14,-1), res.get(3));
            
  }
  
  
  @Test
  public void test256F() {
    List<XtermColoredText> res = colorParser.parseText("(Hidden) (Golden Aura) (White Aura) Nevela \033[38;5;146m-\033[38;5;145m-\033[38;5;144m=\033[38;5;143m[\033[38;5;157mLiv\033[38;5;156me l\033[38;5;155mong\033[38;5;154m an\033[38;5;155md p\033[38;5;156mros\033[38;5;157mper\033[38;5;143m]\033[38;5;144m=\033[38;5;145m-\033[38;5;146m- is here.");
    assertEquals(16, res.size());
    assertEquals(XtermColoredText.valueOf("(Hidden) (Golden Aura) (White Aura) Nevela "), res.get(0));
    assertEquals(XtermColoredText.valueOf("-",146,-1), res.get(1));
    assertEquals(XtermColoredText.valueOf("-",145,-1), res.get(2));
    assertEquals(XtermColoredText.valueOf("=",144,-1), res.get(3));
    assertEquals(XtermColoredText.valueOf("[",143,-1), res.get(4));
    assertEquals(XtermColoredText.valueOf("Liv",157,-1), res.get(5));
    assertEquals(XtermColoredText.valueOf("e l",156,-1), res.get(6));
    assertEquals(XtermColoredText.valueOf("ong",155,-1), res.get(7));
    assertEquals(XtermColoredText.valueOf(" an",154,-1), res.get(8));
    assertEquals(XtermColoredText.valueOf("d p",155,-1), res.get(9));
    assertEquals(XtermColoredText.valueOf("ros",156,-1), res.get(10));
    assertEquals(XtermColoredText.valueOf("per",157,-1), res.get(11));
    assertEquals(XtermColoredText.valueOf("]",143,-1), res.get(12));
    assertEquals(XtermColoredText.valueOf("=",144,-1), res.get(13));
    assertEquals(XtermColoredText.valueOf("-",145,-1), res.get(14));
    assertEquals(XtermColoredText.valueOf("- is here.",146,-1), res.get(15));
  }
  


}