package it.clem.mud.colors;

import org.junit.Before;
import org.junit.Test;

import it.clem.mud.util.colors.xterm.SGRStatus;
import it.clem.mud.util.colors.xterm.XTermTextColor;

import static org.junit.Assert.*;

public class SGRStatusTest {

  private SGRStatus sgrStatus;

  @Before
  public void setUp() {
    sgrStatus = new SGRStatus();
  }

  @Test
  public void testAnsi() {
    assertTrue(sgrStatus.update("0"));
    assertEquals(XTermTextColor.defaultColor(), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("1"));
    assertEquals(XTermTextColor.valueOf(15, -1), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("44"));
    assertEquals(XTermTextColor.valueOf(15, 12), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("30"));
    assertEquals(XTermTextColor.valueOf(8, 12), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("0"));
    assertEquals(XTermTextColor.defaultColor(), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("30"));
    assertEquals(XTermTextColor.valueOf(0, -1), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("44"));
    assertEquals(XTermTextColor.valueOf(0, 4), sgrStatus.getXTermTextColor());
  }

  @Test
  public void testXterm() {
    assertTrue(sgrStatus.update("38;5;190"));
    assertEquals(XTermTextColor.valueOf(190, -1), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("48;5;241"));
    assertEquals(XTermTextColor.valueOf(190, 241), sgrStatus.getXTermTextColor());// discutibile il 7 al posto del 15
    assertTrue(sgrStatus.update("0"));
    assertEquals(XTermTextColor.defaultColor(), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("48;5;190;38;5;241"));
    assertEquals(XTermTextColor.valueOf(241, 190), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("0;38;5;190;48;5;241"));
    assertEquals(XTermTextColor.valueOf(190, 241), sgrStatus.getXTermTextColor());
    assertTrue(sgrStatus.update("0;38;5;190;48;5;241;40"));
    assertEquals(XTermTextColor.valueOf(190, 0), sgrStatus.getXTermTextColor());
  }
  
  @Test
  public void testErr() {
    assertFalse(sgrStatus.update("388;5;190"));
    assertFalse(sgrStatus.update("2"));
    assertTrue(sgrStatus.update("38;5;190"));
    assertEquals(XTermTextColor.valueOf(190, -1), sgrStatus.getXTermTextColor());
    assertFalse(sgrStatus.update("38;4;200"));
    assertEquals(XTermTextColor.valueOf(190, -1), sgrStatus.getXTermTextColor());
    
  }    
}
