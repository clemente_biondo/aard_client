package it.clem.mud.colors;

import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;
import it.clem.mud.util.colors.xterm.XTermColorTokenizer;

@RunWith(MockitoJUnitRunner.class)
public class XTermColorTokenizerMockTest {

  public XTermColorTokenizer tokenizer;
  public @Mock Consumer<String> textConsumer ;
  public @Mock Consumer<String> sgrConsumer ;
  
  @Before
  public void setUp(){
    tokenizer = new XTermColorTokenizer(textConsumer, sgrConsumer);
  }
  
  
  @Test
  public void testNull(){
    tokenizer.tokenize(null);
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
  }

  @Test
  public void testEmpty(){
    tokenizer.tokenize("");
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
  }
  
  @Test
  public void testSimpleSeq1(){
    tokenizer.tokenize("\033[0;36m");
    verify(sgrConsumer,times(1)).accept(eq("0;36"));
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
  }
  @Test
  public void testSimpleSeq2(){
    tokenizer.tokenize("\033[0;36mm");
    InOrder ov=inOrder(sgrConsumer,textConsumer);
    ov.verify(sgrConsumer,times(1)).accept(eq("0;36"));
    ov.verify(textConsumer,times(1)).accept(eq("m"));
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
  }
  @Test
  public void testSimpleSeq3(){
    tokenizer.tokenize("abc\033[0;36m");
    InOrder ov=inOrder(sgrConsumer,textConsumer);
    ov.verify(textConsumer,times(1)).accept(eq("abc"));
    ov.verify(sgrConsumer,times(1)).accept(eq("0;36"));
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
  }
  @Test
  public void testSimpleSeq4(){
    tokenizer.tokenize("abc\033[0;36mm");
    InOrder ov=inOrder(sgrConsumer,textConsumer);
    ov.verify(textConsumer,times(1)).accept(eq("abc"));
    ov.verify(sgrConsumer,times(1)).accept(eq("0;36"));
    ov.verify(textConsumer,times(1)).accept(eq("m"));
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
  }  
  @Test
  public void testComplex1(){
    tokenizer.tokenize("\033[38;5;30mColor  30 Test   \033[0;36mSame color in ANSI\033[0;37m");
    InOrder ov=inOrder(sgrConsumer,textConsumer);
    ov.verify(sgrConsumer,times(1)).accept(eq("38;5;30"));
    ov.verify(textConsumer,times(1)).accept(eq("Color  30 Test   "));
    ov.verify(sgrConsumer,times(1)).accept(eq("0;36"));
    ov.verify(textConsumer,times(1)).accept(eq("Same color in ANSI"));
    ov.verify(sgrConsumer,times(1)).accept(eq("0;37"));
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
  }
  
  @Test
  public void testComplex2(){
    tokenizer.tokenize("bbb\033[1;37m\033[44m \033[33m            Showing : Alphabetical Command List               \033[0;37maaa");
    InOrder ov=inOrder(sgrConsumer,textConsumer);
    ov.verify(textConsumer,times(1)).accept(eq("bbb"));
    ov.verify(sgrConsumer,times(1)).accept(eq("1;37"));
    ov.verify(sgrConsumer,times(1)).accept(eq("44"));
    ov.verify(textConsumer,times(1)).accept(eq(" "));
    ov.verify(sgrConsumer,times(1)).accept(eq("33"));
    ov.verify(textConsumer,times(1)).accept(eq("            Showing : Alphabetical Command List               "));
    ov.verify(sgrConsumer,times(1)).accept(eq("0;37"));
    ov.verify(textConsumer,times(1)).accept(eq("aaa"));
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
  }
  @Test
  public void testErr1(){
    tokenizer.tokenize("bbb\033[1;37\033[44 \033[33            Showing : Alphabetical Command List               \033[0;37maaa\033[0;");
    InOrder ov=inOrder(sgrConsumer,textConsumer);
    ov.verify(textConsumer,times(1)).accept(eq("bbb\033[1;37\033[44 \033[33            Showing : Alphabetical Command List               "));
    ov.verify(sgrConsumer,times(1)).accept(eq("0;37"));
    ov.verify(textConsumer,times(1)).accept(eq("aaa\033[0;"));
    verifyNoMoreInteractions(textConsumer,sgrConsumer);
    
  }
}
