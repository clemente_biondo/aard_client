package it.clem.mud.colors;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.clem.mud.util.colors.ansi.AnsiColor;
import it.clem.mud.util.colors.ansi.AnsiColorParser;
import it.clem.mud.util.colors.ansi.AnsiColoredText;

public class AnsiColorParserTest {
  private AnsiColorParser colorParser;

  @Before
  public void setUp() {
    colorParser = new AnsiColorParser();
  }

  @Test
  public void empty() {
    assertEquals(0, colorParser.parseText("").size());
    assertEquals(0, colorParser.parseText(null).size());
  }

  @Test
  public void sameOf() {
    assertEquals(AnsiColoredText.valueOf("abc"), new AnsiColoredText("abc", AnsiColor.White));
    assertTrue(AnsiColoredText.valueOf("abc").equals("abc", AnsiColor.White));
    assertFalse(AnsiColoredText.valueOf("abc").equals("abc", AnsiColor.Red));
  }

  @Test
  public void noColor() {
    List<AnsiColoredText> res = colorParser.parseText("abc");
    assertEquals(1, res.size());
    assertEquals(AnsiColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void whiteColor() {
    List<AnsiColoredText> res = colorParser
        .parseText("Prompt set to \033[0;37m[%h/%Hhp %m/%Mmn %v/%Vmv %qqt %Xtnl] >%c\033[0;37m");
    assertEquals(2, res.size());
    assertEquals(AnsiColoredText.valueOf("Prompt set to "), res.get(0));
    assertEquals(AnsiColoredText.valueOf("[%h/%Hhp %m/%Mmn %v/%Vmv %qqt %Xtnl] >%c"), res.get(1));
  }

  @Test
  public void multiColor() {
    List<AnsiColoredText> res = colorParser.parseText(
        "   @b / @B \033[0;34mblue\033[0;37m / \033[1;34mbblue\033[0;37m           @c / @C \033[0;36mcyan\033[0;37m / \033[1;36mbcyan\033[0;37m");
    assertEquals(8, res.size());
    assertEquals(AnsiColoredText.valueOf("   @b / @B "), res.get(0));
    assertEquals(AnsiColoredText.valueOf("blue", AnsiColor.Blue), res.get(1));
    assertEquals(AnsiColoredText.valueOf(" / "), res.get(2));
    assertEquals(AnsiColoredText.valueOf("bblue", AnsiColor.BrightBlue), res.get(3));
    assertEquals(AnsiColoredText.valueOf("           @c / @C "), res.get(4));
    assertEquals(AnsiColoredText.valueOf("cyan", AnsiColor.Cyan), res.get(5));
    assertEquals(AnsiColoredText.valueOf(" / "), res.get(6));
    assertEquals(AnsiColoredText.valueOf("bcyan", AnsiColor.BrightCyan), res.get(7));
  }

  @Test
  public void reset() {
    List<AnsiColoredText> res = colorParser.parseText(
        "\033[0m[AFK] \033[1;33mGlaer \033[1;32mGlorious \033[1;37mAvatar\033[0;37m of \033[1;33mBenevolent\033[0;37m Humility\033[1;33m is here.\033[0;37m");
    assertEquals(8, res.size());
    assertEquals(AnsiColoredText.valueOf("[AFK] "), res.get(0));
    assertEquals(AnsiColoredText.valueOf("Glaer ",AnsiColor.BrightYellow), res.get(1));
    assertEquals(AnsiColoredText.valueOf("Glorious ",AnsiColor.BrightGreen), res.get(2));
    assertEquals(AnsiColoredText.valueOf("Avatar",AnsiColor.BrightWhite), res.get(3));
    assertEquals(AnsiColoredText.valueOf(" of "), res.get(4));
    assertEquals(AnsiColoredText.valueOf("Benevolent",AnsiColor.BrightYellow), res.get(5));
    assertEquals(AnsiColoredText.valueOf(" Humility"), res.get(6));
    assertEquals(AnsiColoredText.valueOf(" is here.",AnsiColor.BrightYellow), res.get(7));
  }

  @Test
  public void simple1() {
    List<AnsiColoredText> res = colorParser.parseText("abc\033[0mcde");
    assertEquals(2, res.size());
    assertEquals(AnsiColoredText.valueOf("abc"), res.get(0));
    assertEquals(AnsiColoredText.valueOf("cde"), res.get(1));
  }

  @Test
  public void simple2() {
    List<AnsiColoredText> res = colorParser.parseText("\033[0mcde");
    assertEquals(1, res.size());
    assertEquals(AnsiColoredText.valueOf("cde"), res.get(0));
  }

  @Test
  public void simple3() {
    List<AnsiColoredText> res = colorParser.parseText("abc\033[0m");
    assertEquals(1, res.size());
    assertEquals(AnsiColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void simple4() {
    List<AnsiColoredText> res = colorParser.parseText("\033[0m\033[0m");
    assertEquals(0, res.size());
  }

  @Test
  public void simple5() {
    List<AnsiColoredText> res = colorParser.parseText("\033[0mabc\033[0m\033[0m");
    assertEquals(1, res.size());
    assertEquals(AnsiColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void simple6() {
    List<AnsiColoredText> res = colorParser.parseText("abc\033[0m");
    assertEquals(1, res.size());
    assertEquals(AnsiColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void simple7() {
    List<AnsiColoredText> res = colorParser.parseText("\033[0mabc");
    assertEquals(1, res.size());
    assertEquals(AnsiColoredText.valueOf("abc"), res.get(0));
  }

  @Test
  public void simple8() {
    List<AnsiColoredText> res = colorParser.parseText("abc\033[0mcde\033[0m");
    assertEquals(2, res.size());
    assertEquals(AnsiColoredText.valueOf("abc"), res.get(0));
    assertEquals(AnsiColoredText.valueOf("cde"), res.get(1));
  }

  @Test
  public void simple9() {
    List<AnsiColoredText> res = colorParser.parseText("\033[0mabc\033[0mcde");
    assertEquals(2, res.size());
    assertEquals(AnsiColoredText.valueOf("abc"), res.get(0));
    assertEquals(AnsiColoredText.valueOf("cde"), res.get(1));
  }
}