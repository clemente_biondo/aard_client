package it.clem.mud.colors;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import it.clem.mud.util.colors.xterm.XTermColorTokenizer;

public class XTermColorTokenizerTest {

  public XTermColorTokenizer tokenizer;
  public String text;
  public String sgr;
  
  @Before
  public void setUp(){
    text=null;
    sgr=null;
    tokenizer = new XTermColorTokenizer(s->text=s, s->sgr=s);
  }
  
  @Test
  public void testNull(){
    tokenizer.tokenize(null);
    assertNull(text);
    assertNull(sgr);
  }
  
  @Test
  public void testEmpty(){
    tokenizer.tokenize("");
    assertNull(text);
    assertNull(sgr);
  }
  
  @Test
  public void testText(){
    tokenizer.tokenize("a");
    assertEquals("a",text);
    assertNull(sgr);
    tokenizer.tokenize("ab");
    assertEquals("ab",text);
    assertNull(sgr);
    tokenizer.tokenize("ab c");
    assertEquals("ab c",text);
    assertNull(sgr);
  }

}
