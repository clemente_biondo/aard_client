package it.clem.mud.log;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ContextedRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogSmokeTest {
  private final static Logger log = LoggerFactory.getLogger(LogSmokeTest.class);
	public static void main(String[] args) {
		StringUtils.trimToNull("");
      new LogSmokeTest().fooLog();
      System.out.println(org.slf4j.impl.StaticLoggerBinder.class);
	}
	
	private void fooLog() {
      log.trace("level was {}","trace");
      log.debug("level was {}","debug");
      log.info("level was {}","info");
      try {
    	  Integer.parseInt("W");
      }catch(NumberFormatException nfe) {
    	  log.error("log ex",nfe);
      }
      log.debug("log custom", new ContextedRuntimeException("messaggio").addContextValue("contesto", 123));

	}

}
