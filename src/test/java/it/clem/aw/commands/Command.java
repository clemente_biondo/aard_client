package it.clem.aw.commands;

import javax.annotation.concurrent.Immutable;

@Immutable
public interface Command {
  void execute();
}
