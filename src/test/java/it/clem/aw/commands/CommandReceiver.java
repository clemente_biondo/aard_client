package it.clem.aw.commands;


public interface CommandReceiver {
  Command take() throws InterruptedException;
}
