package it.clem.aw.commands;

import it.clem.aw.commands.impl.CommandBusImpl;

public class CommandBusFactory {
  private final CommandBus bus = new CommandBusImpl();

  public CommandSender getCommandSender() {
    return bus;
  }

  public CommandSender getCommandReceiver() {
    return bus;
  }
}
