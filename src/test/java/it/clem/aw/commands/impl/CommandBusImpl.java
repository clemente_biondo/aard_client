package it.clem.aw.commands.impl;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import it.clem.aw.commands.Command;
import it.clem.aw.commands.CommandBus;

public class CommandBusImpl implements CommandBus{
  private final BlockingQueue<Command> queue = new LinkedBlockingQueue<>();

  @Override
  public Command take() throws InterruptedException {
    return queue.take();
  }
  
}
