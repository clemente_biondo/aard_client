package it.clem.aw.view;

import it.clem.aw.view.console.UIFrontControllerConsole;

public class UserInterfaceFrontControllerFactory {

  public static UserInterfaceFrontController getUserInterfaceFrontControllerFactory(UserInterfaceFrontControllerType type) {
    switch (type) {
    case Console:
      return new UIFrontControllerConsole();
    default:
      throw new IllegalStateException();
    }
  }

}
