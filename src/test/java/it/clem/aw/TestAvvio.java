package it.clem.aw;

import it.clem.aw.commands.CommandBusFactory;
import it.clem.aw.view.UserInterfaceFrontController;
import it.clem.aw.view.UserInterfaceFrontControllerFactory;
import it.clem.aw.view.UserInterfaceFrontControllerType;

public class TestAvvio {
  public void shouldStartTheMinimalApplication() {
    CommandBusFactory commandBusFactory = new CommandBusFactory();
    UserInterfaceFrontController ui = UserInterfaceFrontControllerFactory.getUserInterfaceFrontControllerFactory(UserInterfaceFrontControllerType.Console);
    CommandLoop = 
    ui.start();
  }
}
